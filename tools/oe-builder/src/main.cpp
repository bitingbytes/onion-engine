#include "FileSystem.h"
#include "onionengine.h"
#include <fstream>
#include <iostream>
#include <yaml-cpp/yaml.h>

void
FixPath(const std::string& _path, char* _res);

void
FixPath(const char* _path, char* _res);

void
CreateFolder(const char* _path);

void
CreateProjectFolders();

void
CreateDefaultPreferenceFile();

void
CreateDefaultFiles();

struct Project;

void
ReplaceProperty(std::string&        _src,
                const std::string&  _property,
                const std ::string& _to);

void
GenerateBuilder(char _flag, const Project& _proj);

void
GenerateCMakeLists(const Project& _proj);

void
GenerateHeaderFiles(const Project& _proj);

void
GenerateSourceFiles(const Project& _proj);

const char* directories[] = {
    "bin/",  "build",         "build/debug/",       "build/release/",
    "res/",  "res/maps/",     "res/maps/examples/", "config/",
    "code/", "code/include/", "code/src/"
};

const char* defaultPreferences = "[General]\n"
                                 "sLanguage=\"en-US\"\n\n"
                                 "[Rendering]\n"
                                 "sGraphicAPI=\"OpenGL\"\n"
                                 "iWidth=1280\n"
                                 "iHeight=720\n"
                                 "iMode=0\n"
                                 "bVSync=true\n";

const char* defaultCMakeLists =
  "cmake_minimum_required (VERSION 3.7.0)\n"
  "project(${BUILDER_PROJECT_NAME} C CXX)\n\n"
  "set(CMAKE_SOURCE_DIR code)\n\n"
  "include_directories(${CMAKE_SOURCE_DIR}/include)\n"
  "file(GLOB_RECURSE SOURCE_FILES ${CMAKE_SOURCE_DIR}/src/*.cpp)\n\n"
  "find_package(OnionEngine CONFIG REQUIRED)\n\n"
  "add_definitions(-DOE_RELEASE)\n\n"
  "include_directories(\"${OnionEngine_DIR}/include\" include)\n"
  "link_directories(\"${OnionEngine_DIR}/lib\")\n\n"
  "add_executable(${BUILDER_PROJECT_NAME} SOURCE_FILES)\n\n"
  "set_property(TARGET ${BUILDER_PROJECT_NAME} PROPERTY CXX_STANDARD 11)\n\n"
  "target_link_libraries(${BUILDER_PROJECT_NAME} PRIVATE onionengine)\n\n";

struct Project
{
    char Path[1024];
    char OName[32];
    char Company[32];

    Project()
    {
        memset(Path, NULL, 1024);
        memset(OName, NULL, 32);
        memset(Company, NULL, 32);
    }

    void Print()
    {
        std::cout << "Project" << std::endl;
        std::cout << "Name: " << OName << std::endl;
        std::cout << "Company: " << OName << std::endl;
    }

    void Save()
    {
        YAML::Node projNode;
        YAML::Node projPlatWindows;
        YAML::Node projPlatLinux;
        YAML::Node projPlatMac;

        projPlatWindows["graphicsAPI"] =
          std::vector<const char*>{ "directx11", "opengl" };

        projPlatLinux["graphicsAPI"] = std::vector<const char*>{ "opengl" };

        projPlatMac["graphicsAPI"] =
          std::vector<const char*>{ "metal", "opengl" };

        std::vector<const char*> fscenes = { "res/maps/examples/graybox.owm",
                                             "res/maps/examples/flapbird.osn" };

        projNode["oversion"] = std::to_string(OE_VERSION_MAJOR) + '.' +
                               std::to_string(OE_VERSION_MINOR) + '.' +
                               std::to_string(OE_VERSION_MICRO);
        projNode["pversion"] = "0.1.0";
        projNode["name"]     = OName;
        projNode["company"]  = Company;
        projNode["resRoot"]  = "res/";
        projNode["scenes"]   = fscenes;
        projNode["windows"]  = projPlatWindows;
        projNode["linux"]    = projPlatLinux;
        projNode["macos"]    = projPlatMac;

        char path[4096];
        FixPath("project.yaml", path);

        std::fstream of(path, std::ios::out);

        if (of.is_open()) {
            of << projNode;
            of.close();
        }
    }
};

Project* g_Proj;

int
main(int argc, char const** argv)
{
    if (argc < 2) {
        std::cout << "Invalid argument number" << std::endl;
        exit(1);
    }

    if (strncmp(argv[1], "init", 4) == 0) {
        Project proj;
        g_Proj = &proj;

        /* FILE* projFile = fopen(FixPath("project.yaml"), "r");

        if (projFile) {
            std::cout << "There is a project inside this folder, create a "
                         "project on an empty one."
                      << std::endl;

            fclose(projFile);
            exit(2);
        } */

        char builder = 'c';

        if (argc == 2) {
            std::cout << "Project Name: ";
            std::cin >> proj.OName;

            std::cout << "Company Name: ";
            std::cin >> proj.Company;

            std::cout << "Build Software CMake, Make, Premake: ";
            std::cin >> builder;

            std::cout << std::endl;
        } else if (argc > 2) {
            std::cout << "Initializing Project" << std::endl;

            for (int i = 2; i < argc; i++) {
                std::cout << argv[i] << std::endl;
                if (strcmp(argv[i], "--name-project") == 0) {
                    strncpy(proj.OName, argv[++i], 32);
                } else if (strcmp(argv[i], "--name-company") == 0) {
                    strncpy(proj.Company, argv[++i], 32);
                } else if (strcmp(argv[i], "--root") == 0) {
                    strcpy(proj.Path, argv[++i]);
                }
            }
        }

        proj.Print();
        proj.Save();
        CreateProjectFolders();
        CreateDefaultFiles();
        GenerateBuilder(builder, proj);
        GenerateHeaderFiles(proj);
        GenerateSourceFiles(proj);
    }
    /* code */
    return 0;
}

void
CreateFolder(const char* _path)
{
    std::string path("");
    char        fpath[4096];
    FixPath(_path, fpath);
    path += fpath;

    OnionEngine::OFileSystem::CreateDirectory(path.c_str());
}

void
CreateProjectFolders()
{
    for (int i = 0; i < 11; i++)
        CreateFolder(directories[i]);
}

void
CreateDefaultPreferenceFile()
{
    char fpath[4096];
    FixPath("config/preferences.cfg", fpath);
    std::fstream of(fpath, std::ios::out);

    if (of.is_open()) {
        of << defaultPreferences;
    }

    of.close();
}

void
CreateDefaultFiles()
{
    CreateDefaultPreferenceFile();
}

void
GenerateBuilder(char _flag, const Project& _proj)
{
    if (_flag == 'C' || _flag == 'c') {
        GenerateCMakeLists(_proj);
    }
}

void
GenerateCMakeLists(const Project& _proj)
{
    char fpath[4096];
    FixPath("CMakeLists.txt", fpath);
    std::fstream of(fpath, std::ios::out);

    if (of.is_open()) {
        std::string cmake(defaultCMakeLists);

        ReplaceProperty(cmake, "${BUILDER_PROJECT_NAME}", _proj.OName);

        of << cmake.c_str();
    }

    of.close();
}

void
ReplaceProperty(std::string&        _src,
                const std::string&  _property,
                const std ::string& _to)
{
    size_t start_pos = 0;
    while ((start_pos = _src.find(_property, start_pos)) != std::string::npos) {
        _src.replace(start_pos, _property.length(), _to);
        start_pos += _to.length();
    }
}

void
GenerateHeaderFiles(const Project& _proj)
{
    std::string path = "code/include/";
    path += _proj.OName;
    path += ".h";

    char fpath[4096];
    FixPath(path, fpath);
    std::fstream of(fpath, std::ios::out);

    if (of.is_open()) {
        of << "#pragma once\n\n"
           << "#include \"OCore.h\"\n"
           << "#include \"OGame.h\"\n\n"
           << "using namespace OnionEngine;\n\n";

        of << "class " << _proj.OName << " : public OGameApplication {\n";
        of << "};";
    }

    of.close();
}

void
GenerateSourceFiles(const Project& _proj)
{
    std::string path = "code/src/";
    path += _proj.OName;
    path += ".cpp";

    char fpath[4096];
    FixPath(path, fpath);
    std::fstream of(fpath, std::ios::out);

    if (of.is_open()) {
        of << "#include \"" << _proj.OName << "\";\n\n";
        of << "int32 main() {\n";
        of << "\tColonizeApp app;\n\n";
        of << "\tapp.Initialize();\n";
        of << "\tapp.Shutdown();\n\n";
        of << "\treturn 0;\n";
        of << "}";
    }

    of.close();
}

void
FixPath(const std::string& _path, char* _res)
{
    std::string cwd(g_Proj->Path);

    cwd += '/';
    cwd += _path;

    strcpy(_res, cwd.c_str());
}

void
FixPath(const char* _path, char* _res)
{
    std::string path(_path);
    return FixPath(path, _res);
}
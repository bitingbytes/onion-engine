#include "ModelImporter.h"
#include "OBJImporter.h"

namespace OnionEngine {
bool
ModelImporter::Import(OFile* _src, OFile* _dest, YAML::Node& _node)
{
    ModelImporter settings;

    if (_node["Mesh"].IsDefined())
        settings = ModelImporter(_node);
    else
        settings = ModelImporter::CreateDefaultImporter(_node);

    if (_src->GetExtension() == ".obj")
        return OBJImporter::Import(_src, _dest, settings);

    return false;
}

ModelImporter
ModelImporter::CreateDefaultImporter(YAML::Node& _node)
{
    YAML::Node settings;
    settings["ImportTexCoords"] = true;
    settings["ImportNormals"]   = true;

    _node["Mesh"] = settings;

    ModelImporter temp(_node);

    return temp;
}

} // OnionEngine

#include "ImporterUtils.h"
#include "ImageImporter.h"
#include "ModelImporter.h"

EResourceType
ResourceUtils::GetResourceType(const char* _path)
{
    OString path(_path);
    OString ext(path.substr(path.find_last_of('.')));

    if (ext == ".bmp" || ext == ".tga" || ext == ".png" || ext == ".jpg")
        return RESOURCE_TYPE_IMAGE;
    if (ext == ".obj" || ext == ".fbx")
        return RESOURCE_TYPE_MESH;
    if (ext == ".mp3" || ext == ".wav" || ext == ".ogg")
        return RESOURCE_TYPE_AUDIO;

    return RESOURCE_TYPE_NONE;
}

OString
ResourceUtils::GenerateUID()
{
    OGUID guid = OGUID::Generate();
    return guid.ToString();
}

EResourceResult
ResourceUtils::Import(const char* _path, EResourceType _type)
{
    OString nodePath(_path);
    nodePath += ".omf";

    YAML::Node node;
    OString     buid;

    if (OFileSystem::FileExists(nodePath.c_str())) {
        node = YAML::LoadFile(nodePath.c_str());

        if (node["fversion"]) {
            int32 fversion = node["fversion"].as<int32>();
            if (fversion > OE_FILE_META_VERSION) {
                return RESOURCE_RESULT_FAILED;
            } else if (fversion < OE_FILE_META_VERSION) {
                // Convert to newer version
                return RESOURCE_RESULT_FAILED;
            } else {
                // if(node["lastwrite"])

                if (node["buid"]) {
                    buid = node["buid"].as<OString>();
                    OString buidPath =
                      "./bin/" + buid.substr(0, 2) + "/" + buid + ".obf";
                    if (OFileSystem::FileExists(buidPath.c_str()) &&
                        node["checksum"]) {
                        TScopePtr<OFile> bin = new OFile(buidPath.c_str(), "r");
                        bin->Open();
                        unsigned binChkSum  = bin->GetCheckSum();
                        unsigned metaChkSum = node["checksum"].as<unsigned>();

                        if (binChkSum == metaChkSum)
                            return RESOURCE_RESULT_UPTODATE;
                    }
                }
            }
        }
    } else {
        buid = ResourceUtils::GenerateUID().c_str();

        node["fversion"]  = OE_FILE_META_VERSION;
        node["buid"]      = buid;
        node["lastwrite"] = 0;
        node["checksum"]  = 0;
    }

    const OString path(_path);
    const OString ext(path.substr(path.find_last_of('.')));
    const OString binPath("./bin/" + buid.substr(0, 2) + "/" + buid + ".obf");

    OString dir("./bin/" + buid.substr(0, 2));
    OFileSystem::CreateDirectory(dir.c_str());

    OFile* src  = new OFile(_path, "r");
    OFile* dest = new OFile(binPath.c_str(), "wb");

    bool result = false;

    switch (_type) {
        case EResourceType::RESOURCE_TYPE_NONE:
            result = false;
            break;
        case EResourceType::RESOURCE_TYPE_IMAGE:
            result = ImageImporter::Import(src, dest, node);
            break;
        case EResourceType::RESOURCE_TYPE_MESH:
            result = ModelImporter::Import(src, dest, node);
            break;
        case EResourceType::RESOURCE_TYPE_AUDIO:
            result = false;
            break;
    }

    dest->Close();
    dest->SetMode("rb");
    dest->Open();
    unsigned chksum = dest->GetCheckSum();

    node["lastwrite"] = src->GetLastWriteTime();

    node["checksum"] = chksum;

    dest->Close();
    src->Close();

    if (!result)
        return RESOURCE_RESULT_FAILED;

    std::ofstream fout(nodePath.c_str());
    fout << node;

    fout.flush();
    fout.close();

    node.reset();

    return RESOURCE_RESULT_IMPORTED;
}

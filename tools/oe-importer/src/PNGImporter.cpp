#include "PNGImporter.h"

namespace OnionEngine {

PNGImporter::PNGImporter()
{
    png_ptr        = nullptr;
    info_ptr       = nullptr;
    width          = 0;
    height         = 0;
    bit_depth      = -1;
    color_type     = -1;
    interlace      = -1;
    rowbytes       = -1;
    compression    = -1;
    filter         = -1;
    image_data     = nullptr;
    row_pointers   = nullptr;
    image_channels = -1;
}

PNGImporter::~PNGImporter()
{
    if (png_ptr)
        png_destroy_read_struct(&png_ptr, NULL, NULL);
    if (info_ptr)
        png_destroy_read_struct(NULL, &info_ptr, NULL);

    if (image_data)
        free(image_data);

    width          = 0;
    height         = 0;
    bit_depth      = -1;
    color_type     = -1;
    interlace      = -1;
    rowbytes       = -1;
    compression    = -1;
    filter         = -1;
    image_channels = -1;
}

bool
PNGImporter::IsPNG(OFile* _source)
{
    uint8 png_sig[8];

    _source->Read(&png_sig, 8);

    return png_sig_cmp(png_sig, 0, 8) == 0;
}

PNGImporter*
PNGImporter::Init(OFile* _source)
{
    PNGImporter* temp = new PNGImporter();

    if (!temp->IsPNG(_source)) {
        delete temp;
        return nullptr;
    }

    temp->png_ptr =
      png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!temp->png_ptr) {
        delete temp;
        return nullptr;
    }

    temp->info_ptr = png_create_info_struct(temp->png_ptr);
    if (!temp->info_ptr) {
        delete temp;
        return nullptr;
    }

    if (setjmp(png_jmpbuf(temp->png_ptr))) {
        delete temp;
        return nullptr;
    }

    png_init_io(temp->png_ptr, _source->GetHandler());
    png_set_sig_bytes(temp->png_ptr, 8);
    png_read_info(temp->png_ptr, temp->info_ptr);

    png_get_IHDR(temp->png_ptr,
                 temp->info_ptr,
                 &temp->width,
                 &temp->height,
                 &temp->bit_depth,
                 &temp->color_type,
                 &temp->interlace,
                 &temp->compression,
                 &temp->filter);

    return temp;
}

ETextureColorFormat
PNGImporter::GetColorFormat() const
{
    switch (color_type) {
        case PNG_COLOR_TYPE_PALETTE:
            if (image_channels == 4)
                return ETextureColorFormat::TCF_R8G8B8A8;
            return ETextureColorFormat::TCF_R8G8B8;
        case PNG_COLOR_TYPE_GRAY:
            if (bit_depth == 16)
                return ETextureColorFormat::TCF_R16;
            return ETextureColorFormat::TCF_R8;
        case PNG_COLOR_TYPE_GA:
            if (bit_depth == 16)
                return ETextureColorFormat::TCF_R16G16;
            return ETextureColorFormat::TCF_R8G8;
        case PNG_COLOR_TYPE_RGB:
            if (bit_depth == 16)
                return ETextureColorFormat::TCF_R16G16B16;
            return ETextureColorFormat::TCF_R8G8B8;
        case PNG_COLOR_TYPE_RGBA:
            if (bit_depth == 16)
                return ETextureColorFormat::TCF_R16G16B16A16;
            return ETextureColorFormat::TCF_R8G8B8A8;
    }

    return ETextureColorFormat::TCF_NONE;
}

bool
PNGImporter::GetImage()
{
    if (setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        return false;
    }

    if (color_type == PNG_COLOR_TYPE_PALETTE)
        png_set_expand(png_ptr);
    if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
        png_set_expand(png_ptr);
    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
        png_set_expand(png_ptr);
    if (bit_depth == 16)
        png_set_strip_16(png_ptr);
    if (color_type == PNG_COLOR_TYPE_GRAY ||
        color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
        png_set_gray_to_rgb(png_ptr);

    png_read_update_info(png_ptr, info_ptr);

    rowbytes       = png_get_rowbytes(png_ptr, info_ptr);
    image_channels = (int32)png_get_channels(png_ptr, info_ptr);

    if ((image_data = (uint8*)malloc(rowbytes * height)) == nullptr) {
        return false;
    }

    if ((row_pointers = (png_bytepp)malloc(height * sizeof(png_bytep))) ==
        nullptr)
        return false;

    for (uint32 i = 0; i < height; ++i)
        row_pointers[i] = image_data + i * rowbytes;

    png_read_image(png_ptr, row_pointers);

    free(row_pointers);
    row_pointers = nullptr;

    png_read_end(png_ptr, NULL);

    return true;
}

bool
PNGImporter::Import(OFile* _source, OFile* _destination, ImageImporter& _settings)
{
    _source->SetMode("rb");

    if (!_source->IsOpened())
        if (!_source->Open())
            return false;

    if (!_destination->IsOpened())
        if (!_destination->Open())
            return false;

    PNGImporter* importer = Init(_source);

    if (!importer) {
        delete importer;
        return false;
    }

    if (!importer->GetImage()) {
        delete importer;
        return false;
    }

    uint16 width  = (uint16)importer->width;
    uint16 height = (uint16)importer->height;

    _destination->Write(&width, sizeof(width));
    _destination->Write(&height, sizeof(height));

    uint8 tw = _settings.m_wrap;
    _destination->Write(&tw, sizeof(uint8));

    uint8 tf = importer->GetColorFormat();
    _destination->Write(&tf, sizeof(uint8));

    uint8 tfr = _settings.m_filter;
    _destination->Write(&tfr, sizeof(uint8));

    _destination->Write(importer->image_data,
                        importer->rowbytes * importer->height);

    delete importer;
    importer = nullptr;

    return true;
}
}
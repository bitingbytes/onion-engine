/**
 * @file main.cpp
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-28
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#include "ImporterUtils.h"
#include "LogSubsystem.h"
#include "Types.h"
#include <chrono>

static int64 total_time = 0;

inline bool
Import(const char* path, EResourceType rt)
{
    std::chrono::high_resolution_clock::time_point begin =
      std::chrono::high_resolution_clock::now();

    EResourceResult rr = ResourceUtils::Import(path, rt);

    std::chrono::high_resolution_clock::time_point end =
      std::chrono::high_resolution_clock::now();

    std::cout << "[" << std::this_thread::get_id() << "] ";
    switch (rr) {
        case EResourceResult::RESOURCE_RESULT_FAILED:
            std::cout << "Failed";
            break;
        case EResourceResult::RESOURCE_RESULT_IMPORTED:
            std::cout << "Imported";
            break;
        case EResourceResult::RESOURCE_RESULT_UPTODATE:
            std::cout << "Up-to-date";
            break;
    }

    auto duration =
      std::chrono::duration_cast<std::chrono::milliseconds>(end - begin)
        .count();

    total_time += duration;

    std::cout << " - " << path;

    if (rr == EResourceResult::RESOURCE_RESULT_IMPORTED)
        std::cout << std::setw(2) << " - " << duration << " milliseconds.";

    std::cout << std::endl;

    return true;
}

inline bool
ImportAll(std::vector<OString> _paths)
{

    for (auto path : _paths) {
        EResourceType rt = ResourceUtils::GetResourceType(path.c_str());
        Import(path.c_str(), rt);
    }

    return true;
}

int32
main(int argc, char** argv)
{
    if (argc == 1)
        return 1; // Not enough arguments

    OLogSubsystem::Initialize(ELogLevel::Error);

    const int njobs = argc - 1;

    std::vector<OString> imagePaths;
    std::vector<OString> modelPaths;
    std::vector<OString> audioPaths;

    fprintf(stdout, "Starting importing");

    for (int j = 0; j < njobs; j++) {
        const char*   path = argv[j + 1];
        EResourceType rt   = ResourceUtils::GetResourceType(path);

        switch (rt) {
            case EResourceType::RESOURCE_TYPE_NONE:
                return 0;
            case EResourceType::RESOURCE_TYPE_IMAGE:
                imagePaths.push_back(path);
                break;
            case EResourceType::RESOURCE_TYPE_MESH:
                modelPaths.push_back(path);
                break;
            case EResourceType::RESOURCE_TYPE_AUDIO:
                audioPaths.push_back(path);
                break;
        }
    }

#if 1
    std::thread imageWorker(ImportAll, imagePaths);
    std::thread modelWorker(ImportAll, modelPaths);
    std::thread audioWorker(ImportAll, audioPaths);

    imageWorker.join();
    modelWorker.join();
    audioWorker.join();
#else
    ImportAll(imagePaths);
    ImportAll(modelPaths);
    ImportAll(audioPaths);
#endif

    std::cout << "Total duration: " << total_time << " milliseconds."
              << std::endl;

    OLogSubsystem::Shutdown();

    return 0;
}

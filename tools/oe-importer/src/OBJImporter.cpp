#include "OBJImporter.h"

namespace OnionEngine {
bool
OBJImporter::Import(OFile* _source, OFile* _destination, ModelImporter& _settings)
{
    if (!_source->IsOpened())
        if (!_source->Open())
            return false;

    if (!_destination->IsOpened())
        if (!_destination->Open())
            return false;

    std::vector<Math::OFloat3> vpos;
    std::vector<Math::OFloat2> vtpos;
    std::vector<Math::OFloat3> vnpos;

    std::vector<uint32> vindexes;
    std::vector<uint32> tindexes;
    std::vector<uint32> nindexes;

    while (!_source->IsEoF()) {
        OString line       = _source->GetLine();
        OString firstToken = GetFirstToken(line);

        if (firstToken == TEXT("v")) {
            std::vector<OString> tempPosString;

            Split(Tail(line), tempPosString, TEXT(" "));

            vpos.push_back(Math::OFloat3(std::stof(tempPosString[0]),
                                        std::stof(tempPosString[1]),
                                        std::stof(tempPosString[2])));
        } else if (firstToken == TEXT("vt") && _settings.ImportTexCoords) {
            std::vector<OString> tempTexString;

            Split(Tail(line), tempTexString, TEXT(" "));

            vtpos.push_back(Math::OFloat2(std::stof(tempTexString[0]),
                                         std::stof(tempTexString[1])));
        } else if (firstToken == TEXT("vn") && _settings.ImportNormals) {
            std::vector<OString> tempNorString;

            Split(Tail(line), tempNorString, TEXT(" "));

            vnpos.push_back(Math::OFloat3(std::stof(tempNorString[0]),
                                         std::stof(tempNorString[1]),
                                         std::stof(tempNorString[2])));
        } else if (firstToken == TEXT("f")) {
            std::vector<OString> tempVerString;

            Split(Tail(line), tempVerString, TEXT(" "));
            for (int i = 0; i < 3; i++) {
                int vindex = 0;
                int tindex = 0;
                int nindex = 0;

                sscanf(tempVerString[i].c_str(),
                       "%d/%d/%d",
                       &vindex,
                       &tindex,
                       &nindex);

                vindexes.push_back(vindex);
                tindexes.push_back(tindex);
                nindexes.push_back(nindex);
            }
        }
    }

    uint8 infoLevel = 0U;
    if (vtpos.size() > 0)
        infoLevel = BIT(1);
    if (vnpos.size() > 0)
        infoLevel = BIT(2);

    uint16 vertexCount = (uint16)vpos.size();

    _destination->Write(&infoLevel, sizeof(uint8));
    _destination->Write(&vertexCount, sizeof(uint16));

    for (size_t i = 0; i < vindexes.size(); i++) {
        _destination->Write(&vpos[vindexes[i] - 1], 12);

        if (!vnpos.empty())
            _destination->Write(&vnpos[nindexes[i] - 1], 12);

        if (!vtpos.empty())
            _destination->Write(&vtpos[tindexes[i] - 1], 8);
        _destination->Flush();
    }

    return true;
}
} // OnionEngine

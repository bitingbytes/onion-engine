#include "ImageImporter.h"
#include "PNGImporter.h"

namespace OnionEngine {
bool
ImageImporter::Import(OFile* _src, OFile* _dest, YAML::Node& _node)
{
    ImageImporter settings;

    if (_node["Image"].IsDefined())
        settings = ImageImporter(_node["Image"]);
    else
        settings = ImageImporter::CreateDefaultImporter(_node);

    if (_src->GetExtension() == ".png")
        return PNGImporter::Import(_src, _dest, settings);

    return false;
}

ImageImporter
ImageImporter::CreateDefaultImporter(YAML::Node& _node)
{
    YAML::Node settings;
    settings["wrap"]   = 1; // Vide ETextureWrapMode
    settings["filter"] = 2; // Vide ETextureFilter

    _node["Image"] = settings;

    ImageImporter temp(settings);

    return temp;
}

} // OnionEngine

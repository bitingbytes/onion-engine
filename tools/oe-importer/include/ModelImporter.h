/**
 * @file ModelImporter.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-18
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "FileSystem.h"
#include "OString.h"
#include "ScopePtr.h"
#include <yaml-cpp/yaml.h>

namespace OnionEngine {
class ModelImporter
{
  public:
    ModelImporter() {}

    ModelImporter(const YAML::Node& _node)
    {
        if (_node["Mesh"]["ImportTexCoords"])
            ImportTexCoords = _node["Mesh"]["ImportTexCoords"].as<bool>();
        if (_node["Mesh"]["ImportNormals"])
            ImportNormals = _node["Mesh"]["ImportNormals"].as<bool>();
    }

    static bool Import(OFile* _src, OFile* _dest, YAML::Node& _node);

    static ModelImporter CreateDefaultImporter(YAML::Node& _node);

    bool ImportTexCoords;
    bool ImportNormals;
};
} // OnionEngine

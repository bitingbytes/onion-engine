/**
 * @file OBJImporter.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-18
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "FileSystem.h"
#include "ModelImporter.h"
#include "Vertex.h"

namespace OnionEngine {
class OBJImporter
{
  public:
    static bool Import(OFile*          _source,
                       OFile*          _destination,
                       ModelImporter& _settings);
};
} // OnionEngine

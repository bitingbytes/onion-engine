/**
 * @file ImporterUtils.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-28
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#include "GUID.h"
#include "Name.h"
#include "OString.h"
#include "Types.h"
#include "onionengine.h"
#include <fstream>

using namespace OnionEngine;

enum EResourceResult
{
    RESOURCE_RESULT_FAILED = 0U,
    RESOURCE_RESULT_IMPORTED,
    RESOURCE_RESULT_UPTODATE
};

enum EResourceType
{
    RESOURCE_TYPE_NONE = 0U,
    RESOURCE_TYPE_IMAGE,
    RESOURCE_TYPE_MESH,
    RESOURCE_TYPE_AUDIO
};

class ResourceUtils
{
  public:
    static EResourceType   GetResourceType(const char* _path);
    static EResourceResult Import(const char* _path, EResourceType _type);
    static OString          GenerateUID();
};
/**
 * @file ImageImporter.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-29
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "FileSystem.h"
#include <yaml-cpp/yaml.h>

namespace OnionEngine {
// TODO: Delete
enum ETextureWrapMode : uint8
{
    TW_NONE         = 0,
    TW_WRAP         = 1,
    TW_MIRROR       = 2,
    TW_CLAMP        = 3,
    TW_BORDER_COLOR = 4
};

enum ETextureColorFormat : uint8
{
    TCF_NONE         = 0,
    TCF_R8           = 1,
    TCF_R16          = 2,
    TCF_R32          = 3,
    TCF_R8G8         = 4,
    TCF_R16G16       = 5,
    TCF_R32G32       = 6,
    TCF_R8G8B8       = 7,
    TCF_R16G16B16    = 8,
    TCF_R32G32B32    = 9,
    TCF_R8G8B8A8     = 10,
    TCF_R16G16B16A16 = 11,
    TCF_R32G32B32A32 = 12,
    TCF_BC1          = 13,
    TCF_BC2          = 14,
    TCF_BC3          = 15,
    TCF_BC4          = 16,
    TCF_BC5          = 17,
    TCF_BC6          = 18,
    TCF_BC7          = 19
};

enum ETextureFilter : uint8
{
    TF_NONE        = 0,
    TF_NEAREST     = 1,
    TF_BILINEAR    = 2,
    TF_TRILINEAR   = 3,
    TF_ANISOTROPIC = 4
};

class ImageImporter
{
  public:
    ImageImporter() {}

    ImageImporter(const YAML::Node& _node)
    {
        if (_node["wrap"])
            m_wrap = (ETextureWrapMode)_node["wrap"].as<int>();
        if (_node["filter"])
            m_filter = (ETextureFilter)_node["filter"].as<int>();
    }

    static bool Import(OFile* _src, OFile* _dest, YAML::Node& _node);

    static ImageImporter CreateDefaultImporter(YAML::Node& _node);

    ETextureWrapMode m_wrap;
    ETextureFilter   m_filter;
};
} // OnionEngine
/**
 * @file PNGImporter.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-29
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "FileSystem.h"
#include "ImageImporter.h"
#include <libpng16/png.h>
#include <yaml-cpp/yaml.h>

#define PNG_DEBUG 3

namespace OnionEngine {

class PNGImporter
{
  public:
    static bool Import(OFile*          _source,
                       OFile*          _destination,
                       ImageImporter& _settings);
    PNGImporter();
    ~PNGImporter();

  private:
    bool IsPNG(OFile* _source);

    static PNGImporter* Init(OFile* _source);

    bool GetImage();

    ETextureColorFormat GetColorFormat() const;

    png_structp png_ptr;
    png_infop   info_ptr;
    uint32      width;
    uint32      height;
    int32       bit_depth;
    int32       color_type;
    int32       interlace;
    int32       compression;
    int32       filter;
    int32       rowbytes;
    uint8*      image_data;
    uint8**     row_pointers;

    int32 image_channels;
};
} // OnionEngine

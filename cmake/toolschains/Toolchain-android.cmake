set(CMAKE_SYSTEM_NAME Android)
set(CMAKE_SYSTEM_VERSION 26) # API level
set(CMAKE_ANDROID_ARCH_ABI arm64-v8a)
set(CMAKE_ANDROID_NDK C:/developmentTools/android-sdk-windows/ndk-bundle)
set(ANDROID_TOOLCHAIN=clang)
add_definitions(-Wall -fexceptions)
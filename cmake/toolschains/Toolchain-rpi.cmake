# Define our host system
SET(CMAKE_SYSTEM_NAME Linux)
SET(CMAKE_SYSTEM_VERSION 1)
set(CMAKE_SYSTEM_PROCESSOR armhf)
set(CPACK_DEBIAN_PACKAGE_ARCHITECTURE armhf)
set(IS_ARM TRUE)

# Define the cross compiler locations
SET(CMAKE_C_COMPILER   /home/brian/Develop/rpi-tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-gcc-4.8.3)
SET(CMAKE_CXX_COMPILER /home/brian/Develop/rpi-tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin/arm-linux-gnueabihf-g++)

# Define the sysroot path for the RaspberryPi distribution in our tools folder 
SET(CMAKE_FIND_ROOT_PATH /home/brian/Develop/rpi-tools/rootfs)

INCLUDE_DIRECTORIES(${CMAKE_FIND_ROOT_PATH}/usr/include)
INCLUDE_DIRECTORIES(${CMAKE_FIND_ROOT_PATH}/usr/include/arm-linux-gnueabihf)
INCLUDE_DIRECTORIES(${CMAKE_FIND_ROOT_PATH}/opt/vc/include)
INCLUDE_DIRECTORIES(${CMAKE_FIND_ROOT_PATH}/opt/vc/include/interface/vcos/pthreads)
INCLUDE_DIRECTORIES(${CMAKE_FIND_ROOT_PATH}/opt/vc/include/interface/vmcs_host/linux)

SET(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} --sysroot=${CMAKE_FIND_ROOT_PATH}")
SET(CMAKE_SHARED_LINKER_FLAGS "${CMAKE_SHARED_LINKER_FLAGS} --sysroot=${CMAKE_FIND_ROOT_PATH}")
SET(CMAKE_MODULE_LINKER_FLAGS "${CMAKE_MODULE_LINKER_FLAGS} --sysroot=${CMAKE_FIND_ROOT_PATH}")

# Search for programs only in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)

# Search for libraries and headers in the target directories only
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

add_definitions(-Wall -std=c++11)
include (GenerateExportHeader)

configure_file (
  "${CMAKE_MODULE_PATH}/onionengine.h.in"
  "${PROJECT_BINARY_DIR}/exports/onionengine.h"
)

GENERATE_EXPORT_HEADER(
    onionengine
    BASE_NAME OE
    EXPORT_MACRO_NAME OE_EXPORT
    EXPORT_FILE_NAME ${PROJECT_BINARY_DIR}/exports/onionengine_lib.h
    STATIC_DEFINE OE_BUILT_AS_STATIC
)
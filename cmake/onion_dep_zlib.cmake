find_package(ZLIB QUIET)

if(${ZLIB_FOUND})
    message("zlib found")
	include_directories(${ZLIB_INCLUDE_DIRS})
	add_library(ZLIB_LIB STATIC IMPORTED)
else()
	message("zlib NOT found")

	set(ZLIB_PREFIX zlib)

	set(ZLIB_SOURCE_DIR "${CMAKE_BINARY_DIR}/ext")

	ExternalProject_Add(
		dl-${ZLIB_PREFIX}
		URL ${ZLIB_URI}
		URL_HASH MD5=${ZLIB_HASH}
		SOURCE_DIR ${ZLIB_SOURCE_DIR}/${ZLIB_PREFIX}
		BINARY_DIR      ${CMAKE_BINARY_DIR}/ext/build/${ZLIB_PREFIX}
		INSTALL_DIR     ${CMAKE_BINARY_DIR}/ext/dist
		CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/ext/dist ${DEFAULT_CMAKE_FLAGS} -DCMAKE_POSITION_INDEPENDENT_CODE=ON
	)

	set(ZLIB_INCLUDE_DIRS ${CMAKE_BINARY_DIR}/ext/dist/include)
	set(ZLIB_LIBRARY_DIRS ${CMAKE_BINARY_DIR}/ext/dist/lib)

	if(UNIX)
		set(ZLIB_NAME libz)
	else()
		set(ZLIB_NAME zlib)
	endif()

	add_library(ZLIB_STATIC_LIB STATIC IMPORTED)

if(WINDOWS)
	if(CMAKE_BUILD_TYPE MATCHES "Debug")
		set(ZLIB_STATIC_GENERAL_LIBRARIES           ${CMAKE_BINARY_DIR}/ext/dist/lib/zlibstaticd.lib)
	else()
		set(ZLIB_STATIC_GENERAL_LIBRARIES           ${CMAKE_BINARY_DIR}/ext/dist/lib/zlibstatic.lib)
	endif()
else()
	set(ZLIB_STATIC_GENERAL_LIBRARIES           ${CMAKE_BINARY_DIR}/ext/dist/lib/zlib.lib)
endif()

	set(ZLIB_LIBRARIES ${ZLIB_STATIC_GENERAL_LIBRARIES})
	set_property(TARGET ZLIB_STATIC_LIB PROPERTY IMPORTED_LOCATION ${ZLIB_STATIC_GENERAL_LIBRARIES})
	add_dependencies(ZLIB_STATIC_LIB dl-${ZLIB_PREFIX})
	list(APPEND EXTERNAL_LIBRARIES ${ZLIB_LIBRARIES})
	set_target_properties(ZLIB_STATIC_LIB PROPERTIES FOLDER External)
endif()
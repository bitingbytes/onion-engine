find_package(SDL2 QUIET)

if(${SDL2_FOUND})
    message("SDL2 found")
    include_directories(${SDL2_INCLUDE_DIRS})
    string(STRIP ${SDL2_LIBRARIES} SDL2_LIBRARIES)
	list(APPEND EXTERNAL_LIBRARIES ${SDL2_LIBRARIES})
else()
    message("SDL2 NOT found")

    set(SDL2_PREFIX SDL2)

    set(SDL2_EXTRA_ARGS
        -DSDL_STATIC:BOOL=TRUE
        -DSDL_SHARED:BOOL=FALSE
        -DSDL_VIDEO=OFF
        -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=ON
    )

    if(IS_ARM)
        set(SDL2_EXTRA_ARGS
            ${SDL2_EXTRA_ARGS}
            -DPULSEAUDIO:BOOL=FALSE
            -DVIDEO_OPENGLES:BOOL=ON
            # -DSDL_VIDEO:BOOL=FALSE
            # -DSNDIO:BOOL=FALSE
            # -DINPUT_TSLIB:BOOL=FALSE
        )
    endif()    

    set(SDL2_SOURCE_DIR "${CMAKE_BINARY_DIR}/ext")

    if(WIN32)
        ExternalProject_Add(
            dl-${SDL2_PREFIX}
            URL ${SDL_URI}
            URL_HASH MD5=${SDL_HASH}
            INSTALL_DIR     ${CMAKE_BINARY_DIR}/ext/dist
            CONFIGURE_COMMAND   ""
            BUILD_COMMAND       ""
            INSTALL_COMMAND 
                COMMAND ${CMAKE_COMMAND} -E copy            ${CMAKE_BINARY_DIR}/dl-SDL2-prefix/src/dl-SDL2/lib/x64/SDL2.dll  ${PROJECT_BINARY_DIR}/ext/dist/bin/SDL2.dll
                COMMAND ${CMAKE_COMMAND} -E copy_directory  ${CMAKE_BINARY_DIR}/dl-SDL2-prefix/src/dl-SDL2/lib/x64  ${PROJECT_BINARY_DIR}/ext/dist/lib
                COMMAND ${CMAKE_COMMAND} -E copy_directory  ${CMAKE_BINARY_DIR}/dl-SDL2-prefix/src/dl-SDL2/include          ${PROJECT_BINARY_DIR}/ext/dist/include/SDL2
        )

        ExternalProject_Get_Property(dl-${SDL2_PREFIX} INSTALL_DIR)
        set(SDL2_INCLUDE_DIRS ${INSTALL_DIR}/include)
        set(SDL2_LIBRARIES ${INSTALL_DIR}/lib/SDL2.lib)

        include_directories(${SDL2_INCLUDE_DIRS})
        list(APPEND EXTERNAL_LIBRARIES ${SDL2_LIBRARIES})
    else()
        ExternalProject_Add(
            dl-${SDL2_PREFIX}
            URL ${SDL_URI}
            URL_HASH MD5=${SDL_HASH}
            SOURCE_DIR ${SDL2_SOURCE_DIR}/${SDL2_PREFIX}
            BINARY_DIR      ${CMAKE_BINARY_DIR}/ext/build/${SDL2_PREFIX}
            INSTALL_DIR     ${CMAKE_BINARY_DIR}/ext/dist
            # CONFIGURE_COMMAND <SOURCE_DIR>/configure --prefix=<INSTALL_DIR> --host=${ARCH_TRIPLET} --disable-shared --disable-directx ${SDL_FLAGS} CC=${CMAKE_C_COMPILER} CXX=${CMAKE_CXX_COMPILER}
            CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${PROJECT_BINARY_DIR}/ext/dist ${DEFAULT_CMAKE_FLAGS} ${SDL2_EXTRA_ARGS}
        )

        set(SDL2_INCLUDE_DIRS ${PROJECT_BINARY_DIR}/ext/dist/include)
        set(SDL2_LIBRARY_DIRS ${PROJECT_BINARY_DIR}/ext/dist/lib)
        add_library(SDL2_LIB STATIC IMPORTED)

        set(SDL2_STATIC_GENERAL_LIBRARIES           ${PROJECT_BINARY_DIR}/ext/dist/lib/${CMAKE_STATIC_LIBRARY_PREFIX}${SDL2_PREFIX}${BUILD_TYPE_SUFFIX}${CMAKE_STATIC_LIBRARY_SUFFIX})
        set_property(TARGET SDL2_LIB PROPERTY IMPORTED_LOCATION ${SDL2_STATIC_GENERAL_LIBRARIES})
        add_dependencies(SDL2_LIB dl-${SDL2_PREFIX})
        list(APPEND EXTERNAL_LIBRARIES SDL2_LIB)
        set_target_properties(SDL2_LIB PROPERTIES FOLDER External)
    endif()
endif()

# target_include_directories(onionengine PUBLIC ${sdl2_SOURCE_DIR}/include)
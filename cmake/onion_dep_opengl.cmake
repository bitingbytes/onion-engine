find_package(OpenGL REQUIRED)

if(OPENGL_FOUND)
    set(OpenGL_GL_PREFERENCE "GLVND")
    message(STATUS "opengl found")
    include_directories(${OPENGL_INCLUDE_DIR})
#    list(APPEND EXTERNAL_LIBRARIES ${OPENGL_LIBRARIES})
    message("OpenGL Libs: ${OPENGL_LIBRARIES}")
    message("OpenGL Incl: ${OPENGL_INCLUDE_DIR}")
endif()

find_package(GLEW QUIET)

if(GLEW_FOUND)
    message(STATUS "glew found")    
else()
    set(GLEW_PREFIX glew)

    set(GLEW_EXTRA_ARGS
        -DCMAKE_BUILD_TYPE=Release
        )

    set(GLEW_CPP_SOURCE_DIR "${CMAKE_BINARY_DIR}/ext")

set(INSTALL_CMD "")

if(WINDOWS)
    set(INSTALL_CMD  
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_BINARY_DIR}/dl-glew-prefix/src/dl-glew/bin/Release/x64  ${PROJECT_BINARY_DIR}/ext/dist/bin
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_BINARY_DIR}/dl-glew-prefix/src/dl-glew/lib/Release/x64  ${PROJECT_BINARY_DIR}/ext/dist/lib
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${CMAKE_BINARY_DIR}/dl-glew-prefix/src/dl-glew/include          ${PROJECT_BINARY_DIR}/ext/dist/include)
endif()

    ExternalProject_Add(
        dl-${GLEW_PREFIX}
        URL ${GLEW_URI}
        URL_HASH MD5=${GLEW_HASH}
        INSTALL_DIR     ext/dist
        CONFIGURE_COMMAND   ""
        BUILD_COMMAND       ""
        INSTALL_COMMAND ${INSTALL_CMD}
    )	

    ExternalProject_Get_Property(dl-${GLEW_PREFIX} INSTALL_DIR)
    set(GLEW_INCLUDE_DIRS ${INSTALL_DIR}/include)
    set(GLEW_LIBRARIES ${INSTALL_DIR}/lib/glew32.lib)

    message("GLEW INC: ${GLEW_INCLUDE_DIRS}")
    message("GLEW LIB: ${GLEW_LIBRARIES}")
endif()

set(GLEW_INCLUDE_DIRS ${GLEW_INCLUDE_DIRS} CACHE STRING "")
set(GLEW_LIBRARIES ${GLEW_LIBRARIES} CACHE STRING "")

include_directories(${GLEW_INCLUDE_DIRS})
# list(APPEND EXTERNAL_LIBRARIES ${GLEW_LIBRARIES})    
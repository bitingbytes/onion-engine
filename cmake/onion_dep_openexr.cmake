cmake_minimum_required(VERSION 2.8.2)

set(OPENEXR_EXTRA_ARGS 
    -DBUILD_SHARED_LIBS=OFF 
	-DCMAKE_CXX_STANDARD_LIBRARIES=${OPENEXR_CMAKE_CXX_STANDARD_LIBRARIES}
    -DZLIB_LIBRARY=${CMAKE_CURRENT_BINARY_DIR}/zlib/lib/${ZLIB_LIBRARY}
    -DZLIB_INCLUDE_DIR=${CMAKE_CURRENT_BINARY_DIR}/zlib/include/
    # -DILMBASE_PACKAGE_PREFIX=${CMAKE_CURRENT_BINARY_DIR}/ilmbase/
)

include(ExternalProject)
ExternalProject_Add(openexr-download
                    SOURCE_DIR          ${CMAKE_CURRENT_SOURCE_DIR}/ext/openexr/OpenEXR
                    BINARY_DIR          ${CMAKE_CURRENT_BINARY_DIR}/openexr-build
                    CONFIGURE_COMMAND   ""
                    BUILD_COMMAND       ""
                    INSTALL_COMMAND     ""
                    TEST_COMMAND        ""
                    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/openexr-build
                    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${CMAKE_CURRENT_BINARY_DIR}/openexr ${OPENEXR_EXTRA_ARGS} ${DEFAULT_CMAKE_FLAGS}
                    INSTALL_DIR ${CMAKE_CURRENT_BINARY_DIR}/openexr
)

# add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/ext/openexr)

add_dependencies(openexr-download zlib-download)
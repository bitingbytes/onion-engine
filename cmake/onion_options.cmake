option(BUILD_SHARED                     "Build Onion Engine as shared library" ON)
option(BUILD_SDL	                    "Build SDL 2 library" ON)
option(BUILD_YAML			            "Build Yaml 1.2 library" ON)
option(BUILD_ZLIB			            "Build ZLIB library" ON)
option(BUILD_OPENEXR		            "Build OpenEXR library" OFF)
option(BUILD_TOOLS			            "Build Onion Engine tools" OFF)
option(BUILD_EDITOR                     "Build Onion Engine Qt based Editor" OFF)
option(BUILD_GAME_MODULE	            "Build the Game Modules" OFF)
option(ENABLE_PIC			            "Enable Position Independent Code" ON)
option(OE_AUTHORED_MEMORY_ALLOCATION    "Use Onion Engine custom memory allocations instead of OS ones." ON)
option(OE_ENTITY_COMPONENT_SYSTEM       "Use the Entity Component System for entities" OFF)

if(NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Debug")
endif()

#### Installing Properties ####

set(OE_INSTALL_DIR "onionengine-${ONIONENGINE_VERSION_MAJOR}.${ONIONENGINE_VERSION_MINOR}")
set(CMAKE_INSTALL_PREFIX ${CMAKE_INSTALL_PREFIX}/${OE_INSTALL_DIR})

if(NOT CMAKE_INSTALL_EXEC_PREFIX)
    set(CMAKE_INSTALL_EXEC_PREFIX ${CMAKE_INSTALL_PREFIX})
endif()
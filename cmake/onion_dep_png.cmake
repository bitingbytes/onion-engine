find_package(PNG QUIET)
include(onion_dep_zlib)

if(${PNG_FOUND})
    message("png found")
	include_directories(${PNG_INCLUDE_DIRS})
	list(APPEND EXTERNAL_LIBRARIES ${PNG_LIBRARIES})
else()
	set(PNG_PREFIX png)

	set(PNG_EXTRA_ARGS
		-DPNG_SHARED=OFF
		-DPNG_STATIC=ON
		-DPNG_TESTS=OFF
		-DPNG_DEBUG=ON
		-DZLIB_LIBRARY=${ZLIB_LIBRARIES}
		-DZLIB_INCLUDE_DIR=${ZLIB_INCLUDE_DIRS}
	)

message("PNG Extras: ${PNG_EXTRA_ARGS}")

	set(PNG_CPP_SOURCE_DIR "${CMAKE_BINARY_DIR}/ext")

	ExternalProject_Add(
		dl-${PNG_PREFIX}
		URL ${PNG_URI}
		URL_HASH MD5=${PNG_HASH}
		SOURCE_DIR ${PNG_CPP_SOURCE_DIR}/png
		BINARY_DIR      ${CMAKE_BINARY_DIR}/ext/build/png
		INSTALL_DIR     ${CMAKE_BINARY_DIR}/ext/dist
		CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/ext/dist ${DEFAULT_CMAKE_FLAGS} ${PNG_EXTRA_ARGS}
	)	

	add_dependencies(dl-png dl-zlib)

	set(PNG_CPP_INCLUDE_DIRS ${CMAKE_BINARY_DIR}/ext/dist/include)
	set(PNG_CPP_LIBRARY_DIRS ${CMAKE_BINARY_DIR}/ext/dist/lib)
	add_library(PNG_CPP_LIB STATIC IMPORTED)

	if(WIN32)
		set(PNG_CPP_STATIC_DEBUG_LIBRARIES     ${CMAKE_BINARY_DIR}/ext/dist/lib/libpng16_staticd.lib)
		set_property(TARGET PNG_CPP_LIB PROPERTY IMPORTED_LOCATION_DEBUG ${PNG_CPP_STATIC_DEBUG_LIBRARIES})
	else()
		set(PNG_CPP_STATIC_GENERAL_LIBRARIES           ${CMAKE_BINARY_DIR}/ext/dist/lib/libpng.a)
		set_property(TARGET PNG_CPP_LIB PROPERTY IMPORTED_LOCATION ${PNG_CPP_STATIC_GENERAL_LIBRARIES})
	endif()
	add_dependencies(PNG_CPP_LIB dl-${PNG_PREFIX})
	list(APPEND EXTERNAL_LIBRARIES PNG_CPP_LIB)
	set_target_properties(PNG_CPP_LIB PROPERTIES FOLDER External)
endif()
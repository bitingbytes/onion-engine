#include "Color.h"
#include "Matrix3x3.h"
#include "Matrix4x4.h"
#include "Ray.h"
#include "Sphere.h"
#include "Vector2D.h"
#include "Vector3D.h"
#include "Vector4D.h"

#include <gtest/gtest.h>

using namespace OnionEngine;

template struct Math::TVector3D<float>;
template struct Math::TMatrix3x3<float>;
template struct Math::TMatrix4x4<float>;
template struct Math::TColor<float, 1U>;

TEST(OVector3D, Initialize_Empty)
{
  Math::OFloat3 U;

  EXPECT_FLOAT_EQ(0, U.X);
  EXPECT_FLOAT_EQ(0, U.Y);
  EXPECT_FLOAT_EQ(0, U.Z);
}

TEST(OVector3D, Initialize_Scalar)
{
  Math::OFloat3 U(2.9f);

  EXPECT_FLOAT_EQ(2.9f, U.X);
  EXPECT_FLOAT_EQ(2.9f, U.Y);
  EXPECT_FLOAT_EQ(2.9f, U.Z);
}

TEST(OVector3D, Copy)
{
  Math::OFloat3 U(2.9f, 0.7f, 19.93f);
  Math::OFloat3 V(U);

  EXPECT_FLOAT_EQ(V.X, U.X);
  EXPECT_FLOAT_EQ(V.Y, U.Y);
  EXPECT_FLOAT_EQ(V.Z, U.Z);
}

TEST(OVector3D, Addiction)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 V(4, 5, 6);
  Math::OFloat3 W = U + V;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(7, W.Y);
  EXPECT_FLOAT_EQ(9, W.Z);
}

TEST(OVector3D, Addiction_Scalar)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 W = U + 5;

  EXPECT_FLOAT_EQ(6, W.X);
  EXPECT_FLOAT_EQ(7, W.Y);
  EXPECT_FLOAT_EQ(8, W.Z);
}

TEST(OVector3D, Addiction_Scalar_Forward)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 W = 5.0f + U;

  EXPECT_FLOAT_EQ(6, W.X);
  EXPECT_FLOAT_EQ(7, W.Y);
  EXPECT_FLOAT_EQ(8, W.Z);
}

TEST(OVector3D, Addiction_EQ)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 V(4, 5, 6);
  U += V;

  EXPECT_FLOAT_EQ(5, U.X);
  EXPECT_FLOAT_EQ(7, U.Y);
  EXPECT_FLOAT_EQ(9, U.Z);
}

TEST(OVector3D, Addiction_Scalar_EQ)
{
  Math::OFloat3 U(1, 2, 3);
  U += 5;

  EXPECT_FLOAT_EQ(6, U.X);
  EXPECT_FLOAT_EQ(7, U.Y);
  EXPECT_FLOAT_EQ(8, U.Z);
}

TEST(OVector3D, Subtraction)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 V(4, 5, 6);
  Math::OFloat3 W = U - V;

  EXPECT_FLOAT_EQ(-3, W.X);
  EXPECT_FLOAT_EQ(-3, W.Y);
  EXPECT_FLOAT_EQ(-3, W.Z);
}

TEST(OVector3D, Subtraction_Scalar)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 W = U - 5;

  EXPECT_FLOAT_EQ(-4, W.X);
  EXPECT_FLOAT_EQ(-3, W.Y);
  EXPECT_FLOAT_EQ(-2, W.Z);
}

TEST(OVector3D, Subtraction_Forward)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 W = 5.0f - U;

  EXPECT_FLOAT_EQ(4, W.X);
  EXPECT_FLOAT_EQ(3, W.Y);
  EXPECT_FLOAT_EQ(2, W.Z);
}

TEST(OVector3D, Subtraction_EQ)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 V(4, 5, 6);
  U -= V;

  EXPECT_FLOAT_EQ(-3, U.X);
  EXPECT_FLOAT_EQ(-3, U.Y);
  EXPECT_FLOAT_EQ(-3, U.Z);
}

TEST(OVector3D, Subtraction_Scalar_EQ)
{
  Math::OFloat3 U(1, 2, 3);
  U -= 5;

  EXPECT_FLOAT_EQ(-4, U.X);
  EXPECT_FLOAT_EQ(-3, U.Y);
  EXPECT_FLOAT_EQ(-2, U.Z);
}

TEST(OVector3D, Multiplication)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 V(4, 5, 6);
  Math::OFloat3 W = U * V;

  EXPECT_FLOAT_EQ(4, W.X);
  EXPECT_FLOAT_EQ(10, W.Y);
  EXPECT_FLOAT_EQ(18, W.Z);
}

TEST(OVector3D, Multiplication_Scalar)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 W = U * 5;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(10, W.Y);
  EXPECT_FLOAT_EQ(15, W.Z);
}

TEST(OVector3D, Multiplication_Forward)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 W = 5.0f * U;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(10, W.Y);
  EXPECT_FLOAT_EQ(15, W.Z);
}

TEST(OVector3D, Multiplication_EQ)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 V(4, 5, 6);
  U *= V;

  EXPECT_FLOAT_EQ(4, U.X);
  EXPECT_FLOAT_EQ(10, U.Y);
  EXPECT_FLOAT_EQ(18, U.Z);
}

TEST(OVector3D, Multiplication_Scalar_EQ)
{
  Math::OFloat3 U(1, 2, 3);
  U *= 5;

  EXPECT_FLOAT_EQ(5, U.X);
  EXPECT_FLOAT_EQ(10, U.Y);
  EXPECT_FLOAT_EQ(15, U.Z);
}

TEST(OVector3D, Division)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 V(4, 5, 6);
  Math::OFloat3 W = U / V;

  EXPECT_FLOAT_EQ(0.25f, W.X);
  EXPECT_FLOAT_EQ(0.4f, W.Y);
  EXPECT_FLOAT_EQ(0.5f, W.Z);
}

TEST(OVector3D, Division_Scalar)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 W = U / 5;

  EXPECT_FLOAT_EQ(1.0f / 5.0f, W.X);
  EXPECT_FLOAT_EQ(2.0f / 5.0f, W.Y);
  EXPECT_FLOAT_EQ(3.0f / 5.0f, W.Z);
}

TEST(OVector3D, Division_Scalar_Forward)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 W = 5.0f / U;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(2.5f, W.Y);
  EXPECT_FLOAT_EQ(5.0f / 3.0f, W.Z);
}

TEST(OVector3D, Division_EQ)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 V(4, 5, 6);
  U /= V;

  bool nan = U.IsNaN();

  EXPECT_TRUE(nan == false);
  EXPECT_FLOAT_EQ(0.25f, U.X);
  EXPECT_FLOAT_EQ(0.4f, U.Y);
  EXPECT_FLOAT_EQ(0.5f, U.Z);
}

TEST(OVector3D, Division_Scalar_EQ)
{
  Math::OFloat3 U(1, 2, 3);
  U /= 5;

  EXPECT_FLOAT_EQ(1.0f / 5.0f, U.X);
  EXPECT_FLOAT_EQ(2.0f / 5.0f, U.Y);
  EXPECT_FLOAT_EQ(3.0f / 5.0f, U.Z);
}

TEST(OVector3D, InvertSign)
{
  Math::OFloat3 U(1, 2, 3);
  U = -U;

  EXPECT_FLOAT_EQ(-1, U.X);
  EXPECT_FLOAT_EQ(-2, U.Y);
  EXPECT_FLOAT_EQ(-3, U.Z);
}

TEST(OVector3D, ArrayAccess)
{
  Math::OFloat3 U(1, 2, 3);

  float x = U[0];
  float y = U[1];
  float z = U[2];

  EXPECT_FLOAT_EQ(1, x);
  EXPECT_FLOAT_EQ(2, y);
  EXPECT_FLOAT_EQ(3, z);

  EXPECT_FLOAT_EQ(1, U[0]);
  EXPECT_FLOAT_EQ(2, U[1]);
  EXPECT_FLOAT_EQ(3, U[2]);
}

TEST(OVector3D, Dot)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 V(4, 5, 6);
  float R = Math::Dot(U, V);

  EXPECT_FLOAT_EQ(32, R);
}

TEST(OVector3D, Cross)
{
  Math::OFloat3 U(1, 2, 3);
  Math::OFloat3 V(4, 5, 6);
  Math::OFloat3 W = Math::Cross(U, V);

  EXPECT_FLOAT_EQ(-3, W.X);
  EXPECT_FLOAT_EQ(6, W.Y);
  EXPECT_FLOAT_EQ(-3, W.Z);
}

TEST(OVector3D, LengthSquared)
{
  Math::OFloat3 U(1, 2, 3);

  float length = Math::LengthSquared(U);

  EXPECT_FLOAT_EQ(14, length);
}

TEST(OVector3D, Length)
{
  Math::OFloat3 U(1, 2, 3);

  float length = Math::Length(U);

  EXPECT_FLOAT_EQ(3.7416575f, length);
}
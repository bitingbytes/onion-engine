#include "Vector4D.h"

#include <gtest/gtest.h>

using namespace OnionEngine;

template struct Math::TVector4D<float>;

TEST(OVector4D, Initialize_Empty)
{
  Math::OFloat4 U;

  EXPECT_FLOAT_EQ(0, U.X);
  EXPECT_FLOAT_EQ(0, U.Y);
  EXPECT_FLOAT_EQ(0, U.Z);
  EXPECT_FLOAT_EQ(1, U.W);
}

TEST(OVector4D, Initialize_Scalar)
{
  Math::OFloat4 U(2.9f);

  EXPECT_FLOAT_EQ(2.9f, U.X);
  EXPECT_FLOAT_EQ(2.9f, U.Y);
  EXPECT_FLOAT_EQ(2.9f, U.Z);
  EXPECT_FLOAT_EQ(1.0f, U.W);
}

TEST(OVector4D, Copy)
{
  Math::OFloat4 U(2.9f, 0.7f, 19.93f, 19.83f);
  Math::OFloat4 V(U);

  EXPECT_FLOAT_EQ(V.X, U.X);
  EXPECT_FLOAT_EQ(V.Y, U.Y);
  EXPECT_FLOAT_EQ(V.Z, U.Z);
  EXPECT_FLOAT_EQ(V.W, U.W);
}

TEST(OVector4D, Addiction)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 V(4, 5, 6);
  Math::OFloat4 W = U + V;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(7, W.Y);
  EXPECT_FLOAT_EQ(9, W.Z);
}

TEST(OVector4D, Addiction_Scalar)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 W = U + 5;

  EXPECT_FLOAT_EQ(6, W.X);
  EXPECT_FLOAT_EQ(7, W.Y);
  EXPECT_FLOAT_EQ(8, W.Z);
}

TEST(OVector4D, Addiction_Scalar_Forward)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 W = 5.0f + U;

  EXPECT_FLOAT_EQ(6, W.X);
  EXPECT_FLOAT_EQ(7, W.Y);
  EXPECT_FLOAT_EQ(8, W.Z);
}

TEST(OVector4D, Addiction_EQ)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 V(4, 5, 6);
  U += V;

  EXPECT_FLOAT_EQ(5, U.X);
  EXPECT_FLOAT_EQ(7, U.Y);
  EXPECT_FLOAT_EQ(9, U.Z);
}

TEST(OVector4D, Addiction_Scalar_EQ)
{
  Math::OFloat4 U(1, 2, 3);
  U += 5;

  EXPECT_FLOAT_EQ(6, U.X);
  EXPECT_FLOAT_EQ(7, U.Y);
  EXPECT_FLOAT_EQ(8, U.Z);
}

TEST(OVector4D, Subtration)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 V(4, 5, 6);
  Math::OFloat4 W = U - V;

  EXPECT_FLOAT_EQ(-3, W.X);
  EXPECT_FLOAT_EQ(-3, W.Y);
  EXPECT_FLOAT_EQ(-3, W.Z);
}

TEST(OVector4D, Subtration_Scalar)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 W = U - 5;

  EXPECT_FLOAT_EQ(-4, W.X);
  EXPECT_FLOAT_EQ(-3, W.Y);
  EXPECT_FLOAT_EQ(-2, W.Z);
}

TEST(OVector4D, Subtration_Forward)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 W = 5.0f - U;

  EXPECT_FLOAT_EQ(4, W.X);
  EXPECT_FLOAT_EQ(3, W.Y);
  EXPECT_FLOAT_EQ(2, W.Z);
}

TEST(OVector4D, Subtration_EQ)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 V(4, 5, 6);
  U -= V;

  EXPECT_FLOAT_EQ(-3, U.X);
  EXPECT_FLOAT_EQ(-3, U.Y);
  EXPECT_FLOAT_EQ(-3, U.Z);
}

TEST(OVector4D, Subtration_Scalar_EQ)
{
  Math::OFloat4 U(1, 2, 3);
  U -= 5;

  EXPECT_FLOAT_EQ(-4, U.X);
  EXPECT_FLOAT_EQ(-3, U.Y);
  EXPECT_FLOAT_EQ(-2, U.Z);
}

TEST(OVector4D, Multiplication)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 V(4, 5, 6);
  Math::OFloat4 W = U * V;

  EXPECT_FLOAT_EQ(4, W.X);
  EXPECT_FLOAT_EQ(10, W.Y);
  EXPECT_FLOAT_EQ(18, W.Z);
}

TEST(OVector4D, Multiplication_Scalar)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 W = U * 5;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(10, W.Y);
  EXPECT_FLOAT_EQ(15, W.Z);
}

TEST(OVector4D, Multiplication_Forward)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 W = 5.0f * U;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(10, W.Y);
  EXPECT_FLOAT_EQ(15, W.Z);
}

TEST(OVector4D, Multiplication_EQ)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 V(4, 5, 6);
  U *= V;

  EXPECT_FLOAT_EQ(4, U.X);
  EXPECT_FLOAT_EQ(10, U.Y);
  EXPECT_FLOAT_EQ(18, U.Z);
}

TEST(OVector4D, Multiplication_Scalar_EQ)
{
  Math::OFloat4 U(1, 2, 3);
  U *= 5;

  EXPECT_FLOAT_EQ(5, U.X);
  EXPECT_FLOAT_EQ(10, U.Y);
  EXPECT_FLOAT_EQ(15, U.Z);
}

TEST(OVector4D, Division)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 V(4, 5, 6);
  Math::OFloat4 W = U / V;

  EXPECT_FLOAT_EQ(0.25f, W.X);
  EXPECT_FLOAT_EQ(0.4f, W.Y);
  EXPECT_FLOAT_EQ(0.5f, W.Z);
}

TEST(OVector4D, Division_Scalar)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 W = U / 5;

  EXPECT_FLOAT_EQ(1.0f / 5.0f, W.X);
  EXPECT_FLOAT_EQ(2.0f / 5.0f, W.Y);
  EXPECT_FLOAT_EQ(3.0f / 5.0f, W.Z);
}

TEST(OVector4D, Division_Scalar_Forward)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 W = 5.0f / U;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(2.5f, W.Y);
  EXPECT_FLOAT_EQ(5.0f / 3.0f, W.Z);
}

TEST(OVector4D, Division_EQ)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 V(4, 5, 6);
  U /= V;

  bool nan = U.IsNaN();

  EXPECT_TRUE(nan == false);
  EXPECT_FLOAT_EQ(0.25f, U.X);
  EXPECT_FLOAT_EQ(0.4f, U.Y);
  EXPECT_FLOAT_EQ(0.5f, U.Z);
}

TEST(OVector4D, Division_Scalar_EQ)
{
  Math::OFloat4 U(1, 2, 3);
  U /= 5;

  EXPECT_FLOAT_EQ(1.0f / 5.0f, U.X);
  EXPECT_FLOAT_EQ(2.0f / 5.0f, U.Y);
  EXPECT_FLOAT_EQ(3.0f / 5.0f, U.Z);
}

TEST(OVector4D, InvertSign)
{
  Math::OFloat4 U(1, 2, 3);
  U = -U;

  EXPECT_FLOAT_EQ(-1, U.X);
  EXPECT_FLOAT_EQ(-2, U.Y);
  EXPECT_FLOAT_EQ(-3, U.Z);
}

TEST(OVector4D, ArrayAccess)
{
  Math::OFloat4 U(1, 2, 3);

  float x = U[0];
  float y = U[1];
  float z = U[2];

  EXPECT_FLOAT_EQ(1, x);
  EXPECT_FLOAT_EQ(2, y);
  EXPECT_FLOAT_EQ(3, z);

  EXPECT_FLOAT_EQ(1, U[0]);
  EXPECT_FLOAT_EQ(2, U[1]);
  EXPECT_FLOAT_EQ(3, U[2]);
}

TEST(OVector4D, Dot)
{
  Math::OFloat4 U(1, 2, 3);
  Math::OFloat4 V(4, 5, 6);
  float R = Math::Dot(U, V);

  EXPECT_FLOAT_EQ(33, R);
}

TEST(OVector4D, LengthSquared)
{
  Math::OFloat4 U(1, 2, 3);

  float length = Math::LengthSquared(U);

  EXPECT_FLOAT_EQ(15, length);
}

TEST(OVector4D, Length)
{
  Math::OFloat4 U(1, 2, 3);

  float length = Math::Length(U);

  EXPECT_FLOAT_EQ(3.8729835f, length);
}
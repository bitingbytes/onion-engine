#include "Vector2D.h"

#include <gtest/gtest.h>

using namespace OnionEngine;

template struct Math::TVector2D<float>;

TEST(OVector2D, Initialize_Empty)
{
  Math::OFloat2 U;

  EXPECT_FLOAT_EQ(0, U.X);
  EXPECT_FLOAT_EQ(0, U.Y);
}

TEST(OVector2D, Initialize_Scalar)
{
  Math::OFloat2 U(2.9f);

  EXPECT_FLOAT_EQ(2.9f, U.X);
  EXPECT_FLOAT_EQ(2.9f, U.Y);
}

TEST(OVector2D, Copy)
{
  Math::OFloat2 U(2.9f, 0.7f);
  Math::OFloat2 V(U);

  EXPECT_FLOAT_EQ(V.X, U.X);
  EXPECT_FLOAT_EQ(V.Y, U.Y);
}

TEST(OVector2D, Addiction)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 V(4, 5);
  Math::OFloat2 W = U + V;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(7, W.Y);
}

TEST(OVector2D, Addiction_Scalar)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 W = U + 5;

  EXPECT_FLOAT_EQ(6, W.X);
  EXPECT_FLOAT_EQ(7, W.Y);
}

TEST(OVector2D, Addiction_Scalar_Forward)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 W = 5.0f + U;

  EXPECT_FLOAT_EQ(6, W.X);
  EXPECT_FLOAT_EQ(7, W.Y);
}

TEST(OVector2D, Addiction_EQ)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 V(4, 5);
  U += V;

  EXPECT_FLOAT_EQ(5, U.X);
  EXPECT_FLOAT_EQ(7, U.Y);
}

TEST(OVector2D, Addiction_Scalar_EQ)
{
  Math::OFloat2 U(1, 2);
  U += 5;

  EXPECT_FLOAT_EQ(6, U.X);
  EXPECT_FLOAT_EQ(7, U.Y);
}

TEST(OVector2D, Subtration)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 V(4, 5);
  Math::OFloat2 W = U - V;

  EXPECT_FLOAT_EQ(-3, W.X);
  EXPECT_FLOAT_EQ(-3, W.Y);
}

TEST(OVector2D, Subtration_Scalar)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 W = U - 5;

  EXPECT_FLOAT_EQ(-4, W.X);
  EXPECT_FLOAT_EQ(-3, W.Y);
}

TEST(OVector2D, Subtration_Forward)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 W = 5.0f - U;

  EXPECT_FLOAT_EQ(4, W.X);
  EXPECT_FLOAT_EQ(3, W.Y);
}

TEST(OVector2D, Subtration_EQ)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 V(4, 5);
  U -= V;

  EXPECT_FLOAT_EQ(-3, U.X);
  EXPECT_FLOAT_EQ(-3, U.Y);
}

TEST(OVector2D, Subtration_Scalar_EQ)
{
  Math::OFloat2 U(1, 2);
  U -= 5;

  EXPECT_FLOAT_EQ(-4, U.X);
  EXPECT_FLOAT_EQ(-3, U.Y);
}

TEST(OVector2D, Multiplication)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 V(4, 5);
  Math::OFloat2 W = U * V;

  EXPECT_FLOAT_EQ(4, W.X);
  EXPECT_FLOAT_EQ(10, W.Y);
}

TEST(OVector2D, Multiplication_Scalar)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 W = U * 5;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(10, W.Y);
}

TEST(OVector2D, Multiplication_Forward)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 W = 5.0f * U;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(10, W.Y);
}

TEST(OVector2D, Multiplication_EQ)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 V(4, 5);
  U *= V;

  EXPECT_FLOAT_EQ(4, U.X);
  EXPECT_FLOAT_EQ(10, U.Y);
}

TEST(OVector2D, Multiplication_Scalar_EQ)
{
  Math::OFloat2 U(1, 2);
  U *= 5;

  EXPECT_FLOAT_EQ(5, U.X);
  EXPECT_FLOAT_EQ(10, U.Y);
}

TEST(OVector2D, Division)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 V(4, 5);
  Math::OFloat2 W = U / V;

  EXPECT_FLOAT_EQ(0.25f, W.X);
  EXPECT_FLOAT_EQ(0.4f, W.Y);
}

TEST(OVector2D, Division_Scalar)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 W = U / 5;

  EXPECT_FLOAT_EQ(1.0f / 5.0f, W.X);
  EXPECT_FLOAT_EQ(2.0f / 5.0f, W.Y);
}

TEST(OVector2D, Division_Scalar_Forward)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 W = 5.0f / U;

  EXPECT_FLOAT_EQ(5, W.X);
  EXPECT_FLOAT_EQ(2.5f, W.Y);
}

TEST(OVector2D, Division_EQ)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 V(4, 5);
  U /= V;

  bool nan = U.IsNaN();

  EXPECT_TRUE(nan == false);
  EXPECT_FLOAT_EQ(0.25f, U.X);
  EXPECT_FLOAT_EQ(0.4f, U.Y);
}

TEST(OVector2D, Division_Scalar_EQ)
{
  Math::OFloat2 U(1, 2);
  U /= 5;

  EXPECT_FLOAT_EQ(1.0f / 5.0f, U.X);
  EXPECT_FLOAT_EQ(2.0f / 5.0f, U.Y);
}

TEST(OVector2D, InvertSign)
{
  Math::OFloat2 U(1, 2);
  U = -U;

  EXPECT_FLOAT_EQ(-1, U.X);
  EXPECT_FLOAT_EQ(-2, U.Y);
}

TEST(OVector2D, ArrayAccess)
{
  Math::OFloat2 U(1, 2);

  float x = U[0];
  float y = U[1];

  EXPECT_FLOAT_EQ(1, x);
  EXPECT_FLOAT_EQ(2, y);

  EXPECT_FLOAT_EQ(1, U[0]);
  EXPECT_FLOAT_EQ(2, U[1]);
}

TEST(OVector2D, Dot)
{
  Math::OFloat2 U(1, 2);
  Math::OFloat2 V(4, 5);
  float R = Math::Dot(U, V);

  EXPECT_FLOAT_EQ(14, R);
}

TEST(OVector2D, LengthSquared)
{
  Math::OFloat2 U(1, 2);

  float length = Math::LengthSquared(U);

  EXPECT_FLOAT_EQ(5, length);
}

TEST(OVector2D, Length)
{
  Math::OFloat2 U(1, 2);

  float length = Math::Length(U);

  EXPECT_FLOAT_EQ(2.236068f, length);
}
# Onion Engine

## Build Status and Coverage

[![pipeline status](https://gitlab.com/bitingbytes/onion-engine/badges/develop/pipeline.svg)](https://gitlab.com/bitingbytes/onion-engine/commits/develop)

[![coverage report](https://gitlab.com/bitingbytes/onion-engine/badges/develop/coverage.svg)](https://gitlab.com/bitingbytes/onion-engine/commits/develop)

## Documentation

The in-depth documentation can be found at:

https://bitingbytes.gitlab.io/onion-engine/

## Dependencies

https://github.com/nlohmann/json/

## Installing Windows

```powershell
"C:\Program Files\CMake\bin\cmake.EXE" --build ./build --config Debug --target install -- /m /property:GenerateFullPaths=true
```

/**
 * @file AssetManager.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-06
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "CRC32.h"
#include "FileSystem.h"
#include "OString.h"
#include "Types.h"
#include "onionengine_lib.h"
#include <yaml-cpp/yaml.h>

namespace OnionEngine {
class IAsset
{
  public:
    IAsset() {}

    virtual ~IAsset() {}
};

class AssetManager
{
  private:
    static AssetManager* s_Instance;

  public:
    AssetManager();

    ~AssetManager();

    static OE_EXPORT void Initialize();

    static OE_EXPORT void Shutdown();

    static OE_EXPORT AssetManager& Get() { return *s_Instance; }

    template<typename T>
    inline T* LoadAsset(const char* _path)
    {
        OString path = LoadAssetInternal(_path);
        if (path.empty())
            return nullptr;

        if (!m_loadedAssets.empty()) {
            auto it = m_loadedAssets.find(path);

            if (it != m_loadedAssets.end())
                return (T*)it->second;
        }

        T* asset = T::Load(path.c_str());

        // T* asset = new T(path.c_str());

        m_loadedAssets.emplace(path, asset);

        return asset;
    }

  private:
    OString                              LoadAssetInternal(const char* _path);
    std::unordered_map<OString, IAsset*> m_loadedAssets;
};
} // OnionEngine

/**
 * @file WorldManager.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-23
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "OString.h"
#include "ProjectManager.h"
#include "WorldMap.h"

namespace OnionEngine {
class OE_EXPORT WorldManager
{
    static WorldManager* s_Instance;

  public:
    WorldManager();

    ~WorldManager();

    static void Initialize();

    static void Shutdown();

    static WorldMap* GetCurrentMap();

    static void CreateMap(const OString& _path);

    static void LoadMap(uint32 _index);

    static void LoadMap(const OString& _name);

    static void UnloadCurrentMap();

    static WorldManager& Get() { return *s_Instance; }

  private:
    WorldMap* m_currentMap;

    static bool LoadMapFromFile(OString _path);
};
} // OnionEngine

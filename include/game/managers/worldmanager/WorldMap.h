/**
 * @file WorldMap.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-23
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "ComponentManager.h"
#include "EntityManager.h"
#include "IGameLogic.h"
#include "Types.h"
#include "onionengine_lib.h"

namespace OnionEngine {
class OE_EXPORT WorldMap : public IGameLogic
{
    friend class WorldManager;
    uint8 m_active : 1;

    // Entity and Component Managers
    EntityManager*    m_entityManager;
    ComponentManager* m_componentManager;

  public:
    WorldMap();

    ~WorldMap();

    Entity SpawnEntity();

    void DestroyEntity();

    Entity FindByName(const char* _name) const;

    Entity FindByID(unsigned _id) const;

    inline bool IsActive() const { return m_active; }

    inline void SetActive(bool _active) { m_active = _active; }

  public:
    /** Game Logic Interface Implementation */
    virtual void Start() override;
    virtual void Update() override;
    virtual void Pause() override;
    virtual void Resume() override;
    virtual void Render(Renderer& _renderer) override;
    virtual void Stop() override;
    virtual void OnEnable() override;
    virtual void OnDisable() override;
};
} // OnionEngine

/**
 * @file MemoryManager.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-23
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "StackAllocator.h"
#include "onionengine.h"

namespace OnionEngine {
class OE_EXPORT MemoryManager
{
    static MemoryManager* s_Instance;

  public:
    MemoryManager();

    ~MemoryManager();

    static void Initialize();

    static void Shutdown();

  private:
    OStackAllocator* m_bigStack;
};
} // OnionEngine

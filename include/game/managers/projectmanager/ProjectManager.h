/**
 * @file ProjectManager.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-23
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "OString.h"
#include "Types.h"
#include "onionengine.h"
#include "onionengine_lib.h"
#include <yaml-cpp/yaml.h>

namespace OnionEngine {
class OE_EXPORT ProjectManager
{
    static ProjectManager* s_Instance;

  public:
    ProjectManager();

    ~ProjectManager();

    static void Initialize();

    static void Shutdown();

    static std::vector<OString> GetMapsPath();

  private:
    YAML::Node m_project;
};
} // OnionEngine

/**
 * @file IEntity.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-27
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Types.h"
#include "onionengine_lib.h"

namespace OnionEngine {
class OE_EXPORT Entity
{
    friend class EntityManager;
    unsigned m_id;

  public:
    Entity(unsigned _id);
    ~Entity() = delete;
};
} // OnionEngine

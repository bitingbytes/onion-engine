/**
 * @file EntityManager.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief 
 * @version 0.1
 * @date 2018-12-27
 * 
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 * 
 */

#pragma once

#include "Entity.h"


namespace OnionEngine
{
    class OE_EXPORT EntityManager
    {
        
    };
} // OnionEngine

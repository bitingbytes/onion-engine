/**
 * @file TransformComponent.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-27
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "IComponent.h"
#include "Vector3D.h"
#include "Matrix4x4.h"

namespace OnionEngine {
struct TransformComponent : public IComponent
{
    Math::OVector3D Position;
    Math::OVector3D Rotation;
    Math::OVector3D Scale;

    Math::Matrix4x4 LocalToWorld;
};
} // OnionEngine

/**
 * @file ComponentManager.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-27
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Entity.h"
#include "MemoryManager.h"
#include "Types.h"

namespace OnionEngine {
class OE_EXPORT ComponentManager
{
    std::set<unsigned>         m_components;
    IComponent                 m_next;
    std::map<Entity, unsigned> m_map;
    OStackAllocator*            m_poolAllocator;

  public:
    ComponentManager();

    ~ComponentManager();

    template<typename T, typename... TARGS>
    T AddComponent(const Entity& _entity, TARGS&&... _args)
    {
        void* ptr       = m_poolAllocator->Allocate(sizeof(T));
        T*    Component = new (ptr) T(std::forward<TARGS>(_args)...);

        Component->m_owner = _entity;
        Component->m_id    = RequestComponentID();

        m_map.insert(_entity, Component->m_id);

        return *Component;
    }

    bool Exists(const Component& _component) const;

  private:
    unsigned RequestComponentID() const;
};
} // OnionEngine

/**
 * @file OGameApplication.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-23
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Application.h"
#include "AssetManager.h"
#include "MemoryManager.h"
#include "ProjectManager.h"
#include "WorldManager.h"

namespace OnionEngine {
class OE_EXPORT OGameApplication : public OApplication
{
  public:
    OGameApplication();

    ~OGameApplication();

    void Initialize() override;

    void Shutdown() override;

    // virtual void Start();
    // virtual void Update();
    // virtual void Tick();
    // virtual void Pause();
    // virtual void Resume();
    // virtual void Exit();
    // virtual void OnEvent(/* Event& _event */);

  private:
    void InitializeSubmodules() override;

    void ShutdownSubmodules() override;
};
} // OnionEngine

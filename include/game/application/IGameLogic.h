/**
 * @file IGameLogic.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief Contains the functions needed by the game logic.
 * @version 0.1
 * @date 2018-12-27
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "onionengine_lib.h"

namespace OnionEngine {
class Renderer;

class OE_EXPORT IGameLogic
{
  public:
    virtual void Start()  = 0;
    virtual void Update() = 0;
    virtual void Pause()  = 0;
    virtual void Resume() = 0;
    virtual void Render(Renderer& _renderer);
    virtual void Stop() = 0;

    virtual void OnEnable()  = 0;
    virtual void OnDisable() = 0;
};
} // OnionEngine

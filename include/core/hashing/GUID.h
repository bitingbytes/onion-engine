/**
 * @file GUID.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-12
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "OString.h"
#include "Types.h"
#include "onionengine_lib.h"

namespace OnionEngine {
struct OGUID
{
    union
    {
        unsigned char Data[16];
        unsigned int  DataInt[4];
    };

    static OE_EXPORT OGUID Generate();

    OString ToString() const
    {
        std::stringstream stream0;

        stream0 << std::hex << DataInt[0];
        stream0 << std::hex << DataInt[1];
        stream0 << std::hex << DataInt[2];
        stream0 << std::hex << DataInt[3];

        stream0.flush();

        return OString(stream0.str());
    }
};
} // OnionEngine

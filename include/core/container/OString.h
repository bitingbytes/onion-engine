/**
 * @file OString.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-10-30
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Types.h"
#include <string.h>

namespace OnionEngine {
typedef std::string OString;

inline OString
GetFirstToken(const OString& _line)
{
    if (!_line.empty()) {
        const size_t token_start = _line.find_first_not_of(TEXT(" \t"));
        const size_t token_end = _line.find_first_of(TEXT(" \t"), token_start);
        if (token_start != OString::npos && token_end != OString::npos) {
            return _line.substr(token_start, token_end - token_start);
        } else if (token_start != OString::npos) {
            return _line.substr(token_start);
        }
    }
    return "";
}

inline OString
Tail(const OString& _line)
{
    const size_t token_start = _line.find_first_not_of(" \t");
    const size_t space_start = _line.find_first_of(" \t", token_start);
    const size_t tail_start  = _line.find_first_not_of(" \t", space_start);
    const size_t tail_end    = _line.find_first_not_of(" \t");
    if (tail_start != OString::npos && tail_end != OString::npos) {
        return _line.substr(tail_start, tail_end - tail_start + 1);
    } else if (tail_start != OString::npos) {
        return _line.substr(tail_start);
    }
    return "";
}

inline void
Split(const OString& _line, std::vector<OString>& _outSplitted, OString _token)
{
    _outSplitted.clear();

    OString temp;

    for (size_t i = 0; i < _line.length(); i++) {
        OString test = _line.substr(i, _token.length());

        if (test == _token) {
            if (!temp.empty()) {
                _outSplitted.push_back(temp);
                temp.clear();
                i += (int)_token.length() - 1;
            } else {
                _outSplitted.push_back("");
            }
        } else if (i + _token.length() >= _line.length()) {
            // temp += _line.substr(i, _token.length());
            _outSplitted.push_back(temp);
            break;
        } else {
            temp += _line[i];
        }
    }
}
} // OnionEngine

/**
 * @file ScopePtr.h
 * @author Brian Batista
 * @brief
 * @version 0.1
 * @date 2018-10-16
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

namespace OnionEngine {
template<class T>
class TScopePtr
{
  public:
    inline TScopePtr() { m_data = new T(); }

    inline TScopePtr(T* _data)
      : m_data(_data)
    {}

    inline TScopePtr(const TScopePtr& _other)
      : m_data(_other.m_data)
    {}

    inline TScopePtr(T&& _data)
      : m_data(std::move(_data))
    {}

    inline ~TScopePtr() { delete m_data; }

    // Implicit conversions.
    inline operator T*() const { return m_data; }
    inline T& operator*() const { return *m_data; }
    inline T* operator->() const { return m_data; }

  private:
    T* m_data;
};
} // namespace OnionEngine

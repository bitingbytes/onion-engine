/**
 * @file OName.h
 * @author Brian Batista
 * @brief Converts a string to a stringid(hash).
 * @version 0.1
 * @date 2018-10-08
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "CRC32.h"
#include "OString.h"
#include "Types.h"

namespace OnionEngine {
/**
 * @brief Converts a string to an id.
 *
 * Used to identify objects by its name.
 *
 * @note A name can not contain special character like \n\r\b
 */
class OName
{
  public:
    typedef uint32 Type;

    OName()            = default;
    OName(OName&&)      = default;
    OName(const OName&) = default;
    OName& operator=(OName&&) = default;
    OName& operator=(const OName&) = default;

    /**
     * @brief Construct a new OName object.
     *
     * Construct a new OName object based on a string.
     *
     * @param _value OString to be converted.
     */
    inline OName(const tchar* _value)
      : m_string(_value)
      , m_id(OCRC32::Get(_value))
    {}

    /**
     * @brief Destroy the OName object
     *
     */
    inline ~OName() { m_id = 0; }

    /**
     * @brief Compare two Names
     *
     * @return true If the ids are equal.
     * @return false If the ids are diferent.
     */
    inline friend bool operator==(const OName& _lhs, const OName& _rhs)
    {
        return _lhs.m_id == _rhs.m_id;
    }

    /**
     * @brief Compare two Names
     *
     * @return true If the ids are diferent.
     * @return false If the ids are equal.
     */
    inline friend bool operator!=(const OName& _lhs, const OName& _rhs)
    {
        return _lhs.m_id != _rhs.m_id;
    }

    inline Type GetID() const { return m_id; }

    inline const char* ToString() const { return m_string.c_str(); }

  private:
    OString m_string;
    Type m_id;
};
} // namespace OnionEngine

/**
 * @file RenderCommand.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Types.h"
#include "Mesh.h"
#include "Matrix4x4.h"

namespace OnionEngine {
namespace Graphics {
enum class ERendererCommand : uint8
{
    RC_DRAW_MESH
};

struct RendererCommand
{
    RendererCommand(ERendererCommand _type, void* _data)
      : Type(_type)
      , Data(_data)
    {}

    ERendererCommand Type;
    void*            Data;
};
} // Graphics
} // OnionEngine

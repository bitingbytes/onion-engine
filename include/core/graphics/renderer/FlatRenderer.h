/**
 * @file FlatRenderer.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-14
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "BatchRenderer.h"
#include "IRenderer.h"
#include "IndexBuffer.h"
#include "Material.h"
#include "Mesh.h"
#include "Sprite.h"
#include "Vertex.h"

namespace OnionEngine {
class CameraComponent;
}

namespace OnionEngine {
namespace Graphics {
class FlatRenderer : public IRenderer
{
  public:
    FlatRenderer(CameraComponent* _owner);

    ~FlatRenderer();

    void Begin() override;
    void BeginScene() override;
    void SubmitCommand(RendererCommand&&) override;
    void EndScene() override;
    void End() override;
    void Present() override;

  private:
    void CommandRender();

    void BatchRender();

  private:
    CameraComponent* m_owner;
    BatchRenderer*   m_batcher;
    uint8            m_isBatching : 1;
};
} // Graphics
} // OnionEngine

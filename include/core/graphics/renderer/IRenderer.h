/**
 * @file IRenderer.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Color.h"
#include "RendererCommand.h"

namespace OnionEngine {
namespace Graphics {
class IRenderer
{
  public:
    virtual ~IRenderer() {}

    virtual void Begin()                          = 0;
    virtual void BeginScene()                     = 0;
    virtual void SubmitCommand(RendererCommand&&) = 0;
    virtual void EndScene()                       = 0;
    virtual void End()                            = 0;
    virtual void Present()                        = 0;

  protected:
    std::queue<RendererCommand> m_commandQueue;
};
} // Graphics
} // OnionEngine

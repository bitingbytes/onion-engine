/**
 * @file BatchRenderer.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-17
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Mesh.h"
#include "Vertex.h"

namespace OnionEngine {
namespace Graphics {
class BatchRenderer
{
  public:
    BatchRenderer();

    ~BatchRenderer();

    void SubmitMesh(Mesh* _mesh);

    void Present();

    void Clean();

  private:
    VertexArray* m_varray;
    IndexBuffer* m_ibuffer;
};
} // Graphics
} // OnionEngine

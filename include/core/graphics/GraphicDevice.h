/**
 * @file OGraphicDevice.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-27
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Rect.h"
#include "Types.h"
#include "Window.h"
#include "onionengine_lib.h"

namespace OnionEngine {
namespace Graphics {
class OGraphicDevice
{
  public:
    virtual ~OGraphicDevice();
    virtual bool Create(OWindow* _window);

    virtual void Clear();

    virtual void Present();

    virtual void SetViewport(const Math::ORect&);

  protected:
    OGraphicDevice();

    OWindow* m_window;
};
} // Graphics
} // OnionEngine

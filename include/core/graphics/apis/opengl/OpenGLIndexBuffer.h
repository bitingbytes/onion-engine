/**
 * @file OpenGLIndexBuffer.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-10
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "IndexBuffer.h"
#include <GL/glew.h>
#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

namespace OnionEngine {
namespace Graphics {
class OpenGLIndexBuffer : public IndexBuffer
{
  public:
    OpenGLIndexBuffer(uint16* _data, uint32 _size);

    OpenGLIndexBuffer(uint32* _data, uint32 _size);

    ~OpenGLIndexBuffer();

    void Attach() override;

    void Render() override;

    void Detach() override;

  private:
    GLuint m_ibo;
};
} // Graphics
} // OnionEngine
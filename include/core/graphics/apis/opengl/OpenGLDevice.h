/**
 * @file OpenGLDevice.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-27
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "GraphicDevice.h"

#if defined(__APPLE__)
  #if OE_PLATFORM_MACOS
    #include <OpenGL/gl.h>
    #include <OpenGL/glu.h>
    #include <GL/glew.h>
  #elif OE_PLATFORM_IOS
    #include <OpenGLES/ES2/gl.h>
    #include <OpenGLES/ES2/glext.h>
#else
  #include <GL/glew.h>
  #include <GL/gl.h>
  #include <GL/glu.h>
#endif

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>

namespace OnionEngine {
namespace Graphics {
class OpenGLDevice : public OGraphicDevice
{
  public:
    OpenGLDevice();

    ~OpenGLDevice();

    bool Create(OWindow* _window) override;

    void Clear() override;

    void Present() override;

    void SetViewport(const Math::ORect&) override;

    static OE_EXPORT GLuint GetProgramID();

  private:
    bool InitGL();

  private:
    GLuint        gProgramID = 0;
    SDL_GLContext gContext;
};
} // Graphics
} // OnionEngine

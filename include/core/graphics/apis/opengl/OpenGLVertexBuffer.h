/**
 * @file OpenGLVertexBuffer.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-03
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "VertexBuffer.h"
#include <GL/glew.h>
#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

namespace OnionEngine {
namespace Graphics {
class OpenGLVertexBuffer : public VertexBuffer
{
  public:
    OpenGLVertexBuffer();

    ~OpenGLVertexBuffer();

    void Attach() override;

    void Render() override;

    void Detach() override;

    void Allocate(size_t _size) override;

    void SetData(const void* _data, size_t _size) override;

    void ReleasePointer() override;

    void SetLayout() override;

  private:
    uint32      m_size;
    GLuint      m_VBO;

  protected:
    void* GetPointerInternal() override;
};
} // Graphics
} // OnionEngine

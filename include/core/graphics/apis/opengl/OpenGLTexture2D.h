/**
 * @file OpenGLTexture.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-27
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "IResource.h"
#include "OpenGLDevice.h"
#include "Texture2D.h"
#include <GL/glew.h>

namespace OnionEngine {
namespace Graphics {
class OpenGLTexture2D : public Texture2D
{
  public:
    OpenGLTexture2D(const char* _path);

    void Attach() override;

    void Render() override;

    void Detach() override;

  private:
    GLuint m_tex;
};
} // Graphics
} // OnionEngine
/**
 * @file OpenGLShader.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-11
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Shader.h"

#include <GL/glew.h>
#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

namespace OnionEngine {
namespace Graphics {
class OpenGLShader : public Shader
{
  public:
    OpenGLShader(const char* _path);

    ~OpenGLShader();

    void Attach() override;

    void Render() override;

    void Detach() override;

    void SetMatrix4x4(const char*            _property,
                      const Math::Matrix4x4& _matrix) override;

    void SetColor(const char* _property, const Math::OColor _color) override;

    void SetColor(const char* _property, const Math::OColor8 _color) override;

  private:
    GLuint m_shaderProgram;
    GLuint m_vertexShader;
    GLuint m_fragmentShader;
};
} // Graphics
} // OnionEngine

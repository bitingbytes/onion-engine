/**
 * @file OpenGLVertexArray.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-11
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "VertexArray.h"

#include "VertexBuffer.h"
#include <GL/glew.h>
#if defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

namespace OnionEngine {
namespace Graphics {
class OpenGLVertexArray : public VertexArray
{
  private:
    GLuint                     m_VAO;
    std::vector<VertexBuffer*> m_buffers;

  public:
    inline VertexBuffer* GetBuffer(uint32 index = 0) override
    {
        return m_buffers[index];
    }

    OpenGLVertexArray();
    ~OpenGLVertexArray();

    void PushBuffer(VertexBuffer* buffer) override;

    void Bind() const override;
    void Unbind() const override;

    void Draw(uint32 count) const override;
};
} // Graphics
} // OnionEngine

/**
 * @file OGraphicSubsystem.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-27
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "GraphicDevice.h"
#include "Name.h"
#include "Types.h"
#include "onionengine_lib.h"

namespace OnionEngine {
namespace Graphics {
enum class EGraphicAPI
{
    GA_NONE = 0U,
#if OE_SUPPORT_OPENGL
    GA_OPENGL,
#endif
#if ONION_SUPPORT_DIRECTX11
    GA_DIRECTX11,
#endif
#if ONION_SUPPORT_DIRECTX12
    GA_DIRECTX12
#endif
};

class OE_EXPORT OGraphicSubsystem
{
    static OGraphicSubsystem* s_Instance;

  public:
    OGraphicSubsystem();
    ~OGraphicSubsystem();

    static void Initialize();

    static void Shutdown();

    static void SwitchAPI(const char* _apiString);

    static void SwitchAPI(EGraphicAPI _api);

    static void Clear();

    static void Present();

    static bool CreateDevice(OWindow* _window);

    static EGraphicAPI GetAPI();

    inline static OGraphicSubsystem& Get() { return *s_Instance; }

    inline OGraphicDevice& GetDevice() const { return *m_device; }

  private:
    EGraphicAPI m_currentAPI;

    void SwitchAPIInternal(EGraphicAPI _api);

    void ClearInternal();

    void PresentInternal();

    bool CreateDeviceInternal(OWindow* _window);

  private:
    OGraphicDevice* m_device;
};
} // Graphics
} // OnionEngine

/*
#if OE_SUPPORT_OPENGL
#endif
#if ONION_SUPPORT_DIRECTX11 || ONION_SUPPORT_DIRECTX12
#endif
#if ONION_SUPPORT_DIRECTX12
#endif

switch (OGraphicSubsystem::GetAPI()) {
    case EGraphicAPI::GA_NONE:
        break;

#if OE_SUPPORT_OPENGL
    case EGraphicAPI::GA_OPENGL:
        break;
#endif

#if ONION_SUPPORT_DIRECTX11 || ONION_SUPPORT_DIRECTX12
    case EGraphicAPI::GA_DIRECTX11:
        break;
#endif

#if ONION_SUPPORT_DIRECTX12
    case EGraphicAPI::GA_DIRECTX12:
        break;
#endif
}
*/
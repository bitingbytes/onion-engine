/**
 * @file Texture.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-27
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "AssetManager.h"
#include "GraphicSubsystem.h"
#include "Types.h"
#include "onionengine_lib.h"

namespace OnionEngine {
namespace Graphics {
class OE_EXPORT Texture : public IAsset
{
  protected:
    enum ETextureWrapMode : uint8
    {
        TW_NONE = 0U,
        TW_WRAP,
        TW_MIRROR,
        TW_CLAMP,
        TW_BORDER_COLOR
    };

    enum ETextureColorFormat : uint8
    {
        TCF_NONE = 0U,
        TCF_R8,
        TCF_R16,
        TCF_R32,
        TCF_R8G8,
        TCF_R16G16,
        TCF_R32G32,
        TCF_R8G8B8,
        TCF_R16G16B16,
        TCF_R32G32B32,
        TCF_R8G8B8A8,
        TCF_R16G16B16A16,
        TCF_R32G32B32A32,
        TCF_BC1,
        TCF_BC2,
        TCF_BC3,
        TCF_BC4,
        TCF_BC5,
        TCF_BC6,
        TCF_BC7
    };

    enum ETextureFilter : uint8
    {
        TF_NONE = 0U,
        TF_NEAREST,
        TF_BILINEAR,
        TF_TRILINEAR,
        TF_ANISOTROPIC
    };

  public:
    Texture();
    Texture(const char* _path);
    virtual ~Texture();

  protected:
    uint16              m_width;
    uint16              m_height;
    ETextureWrapMode    m_wrapMode;
    ETextureColorFormat m_format;
    ETextureFilter      m_filter;
    uint8               m_hasMipMaps : 1;
    uint8*              m_data;

  public:
    inline uint16              GetWidth() const { return m_width; }
    inline uint16              GetHeight() const { return m_height; }
    inline ETextureWrapMode    GetWrapMode() const { return m_wrapMode; }
    inline ETextureColorFormat GetFormat() const { return m_format; }
    inline ETextureFilter      GetFilter() const { return m_filter; }
    inline bool                HasMipMaps() const { return m_hasMipMaps; }

    inline void SetWrapMode(ETextureWrapMode _mode) { m_wrapMode = _mode; }
    inline void SetFormat(ETextureColorFormat _format) { m_format = _format; }
    inline void SetFilter(ETextureFilter _filter) { m_filter = _filter; }
    inline void GenerateMipMaps(bool _gen) { m_hasMipMaps = true; }

    inline bool IsPowerOfTwo() const
    {
        uint32 dim = m_width * m_height;
        return (dim != 1) && (dim & (dim - 1));
    }

    virtual void Attach() {}

    virtual void Render() {}

    virtual void Detach() {}
};
} // Graphics
} // OnionEngine

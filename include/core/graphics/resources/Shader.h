/**
 * @file Shader.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-11
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "IResource.h"
#include "Matrix4x4.h"
#include "Color.h"
#include "onionengine_lib.h"

namespace OnionEngine {
namespace Graphics {
enum class ShaderType : uint8
{
    ST_UNKOWN = 0,
    ST_VERTEX,
    ST_PIXEL
};

class OE_EXPORT Shader : public IResource
{
  public:
    static Shader* LoadFromFile(const char* _path);

    virtual void SetMatrix4x4(const char*            _property,
                              const Math::Matrix4x4& _matrix) = 0;

    virtual void SetColor(const char* _property, const Math::OColor _color) = 0;
    
    virtual void SetColor(const char* _property, const Math::OColor8 _color) = 0;

  protected:
    static bool PreProcess(const char* _path, OString** _shaders);
};
} // Graphics
} // OnionEngine

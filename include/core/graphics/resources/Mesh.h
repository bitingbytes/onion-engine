/**
 * @file Mesh.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-16
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "IndexBuffer.h"
#include "Material.h"
#include "VertexArray.h"

namespace OnionEngine {
namespace Graphics {
class Mesh
{
  public:
  friend class BatchRender;
  public:
    Mesh(VertexArray* _array, IndexBuffer* _index, Material* _mat);

    Mesh(VertexArray* _array, IndexBuffer* _index, Texture2D* _tex);

    ~Mesh();

    void Draw(Math::Matrix4x4 _cam);

    inline Material* GetMaterial() { return m_material; }

    inline void SetTransform(const Math::Matrix4x4& _transform)
    {
        m_transform = _transform;
    }

  private:
    VertexArray*    m_varray;
    IndexBuffer*    m_ibuffer;
    Material*       m_material;
    Math::Matrix4x4 m_transform;
};
} // Graphics

} // OnionEngine

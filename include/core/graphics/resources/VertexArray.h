/**
 * @file VertexArray.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-11
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "VertexBuffer.h"
#include "onionengine_lib.h"

namespace OnionEngine {
namespace Graphics {
class OE_EXPORT VertexArray
{
  private:
    std::vector<VertexBuffer*> m_buffers;

  public:
    virtual VertexBuffer* GetBuffer(uint32 index = 0) = 0;

    virtual ~VertexArray() {}

    virtual void PushBuffer(VertexBuffer* buffer) = 0;

    virtual void Bind() const   = 0;
    virtual void Unbind() const = 0;

    virtual void Draw(uint32 count) const = 0;

  public:
    static VertexArray* Create();
};
} // Graphics
} // OnionEngine

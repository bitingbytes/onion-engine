/**
 * @file IndexBuffer.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-10
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "IResource.h"
#include "onionengine_lib.h"

namespace OnionEngine {
namespace Graphics {
class OE_EXPORT IndexBuffer : public IResource
{
  public:
    virtual ~IndexBuffer() {}

    static IndexBuffer* Create(uint16* _data, uint32 _size);

    static IndexBuffer* Create(uint32* _data, uint32 _size);

    inline uint32 GetCount() const { return m_count; }

  protected:
    uint32 m_count;
};
} // Graphics
} // OnionEngine

/**
 * @file Sprite.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Material.h"
#include "Rect.h"

namespace OnionEngine {
namespace Graphics {
class Sprite
{
  public:
    friend class SpriteRenderer;

  public:
    Sprite();

    Sprite(Texture2D* _texture);

    Sprite(Texture2D* _texture, const Math::ORect& _rect);

    ~Sprite();

    inline Texture2D* GetTexture(uint8 _index = 0)
    {
        return m_material->GetTextures()[_index];
    }

    inline Math::ORect GetRect() const { return m_rect; }

    inline Material* GetMaterial() { return m_material; }

  private:
    Material*  m_material;
    Math::ORect m_rect;
};
} // Graphics
} // OnionEngine

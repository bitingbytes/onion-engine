/**
 * @file VertexBuffer.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-03
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "IResource.h"
#include "Matrix4x4.h"
#include "Vector3D.h"
#include "onionengine_lib.h"

namespace OnionEngine {
namespace Graphics {
class OE_EXPORT VertexBuffer : public IResource
{
  public:
    virtual ~VertexBuffer() {}

    virtual void Transform(const Math::Matrix4x4&,
                           const Math::Matrix4x4&,
                           const Math::Matrix4x4&)
    {}

    virtual void Allocate(size_t _size) {}

    virtual void SetData(const void* _data, size_t _size) {}

    static VertexBuffer* Create();

    virtual void ReleasePointer() = 0;

    virtual void SetLayout() = 0;

    template<typename T>
    T* GetPointer()
    {
        return (T*)GetPointerInternal();
    }

  protected:
    virtual void* GetPointerInternal() = 0;
};
} // Graphics
} // OnionEngine
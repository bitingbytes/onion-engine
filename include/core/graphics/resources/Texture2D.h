/**
 * @file Texture2D.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-27
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Texture.h"

namespace OnionEngine {
namespace Graphics {
class OE_EXPORT Texture2D : public Texture
{
  public:
    Texture2D(const char* _path);

    static Texture2D* Load(const char* _path);
};
} // Graphics
} // OnionEngine
/**
 * @file IResource.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-03
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "GraphicSubsystem.h"

namespace OnionEngine {
namespace Graphics {
class OE_EXPORT IResource
{
  public:
    virtual ~IResource() {}

    // Attach resource to GPU's memory.
    virtual void Attach() = 0;

    virtual void Render() = 0;

    // Detach resource from GPU's memory.
    virtual void Detach() = 0;
};
} // Graphics
} // OnionEngine

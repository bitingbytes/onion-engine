/**
 * @file Material.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Color.h"
#include "Shader.h"
#include "Texture2D.h"

namespace OnionEngine {
namespace Graphics {
class Material : public IResource
{
  private:
    friend class MaterialInstance;

  public:
    Material(Shader* _shader);
    Material(const char* _shaderPath);
    ~Material();

    void Attach() override;

    void Render() override;

    void Detach() override;

    void SetTexture2D(Texture2D* _texture);

    void SetMatrix(const char* _property, const Math::Matrix4x4& _matrix);

    inline void SetColor(const Math::OColor& _color) { m_color = _color; }

    inline std::vector<Texture2D*> GetTextures() const { return m_textures; }

  protected:
    Shader*                 m_shader;
    std::vector<Texture2D*> m_textures;
    Math::OColor             m_color;
};

/* class MaterialInstance : public IResource
{
  public:
    MaterialInstance(Material* _material);

    void Attach() override;

    void Render() override;

    void Detach() override;

  private:
    Material* m_sourceMaterial;
}; */
} // Graphics
} // OnionEngine

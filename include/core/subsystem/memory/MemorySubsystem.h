/**
 * @file OMemorySubsystem.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief Subsystem responsable for memory operations.
 * @version 0.1
 * @date 2018-10-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "Types.h"
#include "onionengine.h"
#include "onionengine_lib.h"

namespace OnionEngine {
class OE_EXPORT OMemorySubsystem
{
  private:
    static OMemorySubsystem* s_Instance;

  public:
    OMemorySubsystem();

    ~OMemorySubsystem();

    static void Initialize();

    static void Shutdown();

    inline static OMemorySubsystem& Get() { return *s_Instance; }

    inline size_t AlignAddress(size_t _address, size_t _align)
    {
        const size_t mask = _align - 1;
        OE_ASSERT((_align & mask) == 0);
        return (_address + mask) & ~mask;
    }

    template<typename T>
    inline T* AlignPointer(T* _ptr, size_t _align)
    {
        const size_t addr        = reinterpret_cast<size_t>(_ptr);
        const size_t addrAligned = AlignAddress(addr, _align);
        return reinterpret_cast<T*>(addrAligned);
    }

    void* AllocateAligned(size_t _bytes, size_t _align = 16)
    {
        size_t actualBytes = _bytes + _align;

        uint8* RawMem = (uint8*)malloc(sizeof(uint8) * actualBytes);

        uint8* AlignedMem = AlignPointer(RawMem, _align);
        if (AlignedMem == RawMem)
            AlignedMem += _align;

        std::ptrdiff_t Shift = AlignedMem - RawMem;
        OE_ASSERT(Shift > 0 && Shift <= 256);

        AlignedMem[-1] = static_cast<uint8>(Shift & 0xFF);

        return AlignedMem;
    }

  public:
    void* Allocate(size_t _size);

    void Free(void* _block);
};
} // namespace OnionEngine

/*#pragma warning(disable : 4595)

 inline void*
operator new(size_t _size)
{
    return OnionEngine::OMemorySubsystem::Get().Allocate(_size);
}

inline void*
operator new[](size_t _size)
{
    return OnionEngine::OMemorySubsystem::Get().Allocate(_size);
}

inline void
operator delete(void* _size)
{
    OnionEngine::OMemorySubsystem::Get().Free(_size);
}

inline void
operator delete[](void* _size)
{
    OnionEngine::OMemorySubsystem::Get().Free(_size);
} */
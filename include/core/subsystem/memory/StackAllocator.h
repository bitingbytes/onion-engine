/**
 * @file OStackAllocator.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief Stack Allocator
 * @version 0.1
 * @date 2018-10-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "MemorySubsystem.h"

namespace OnionEngine
{
/**
 * @brief Allocates memory on a stack.
 *
 * Allocates a big chunck of memory and moves a pointer to allocate objects.
 *
 */
class OE_EXPORT OStackAllocator
{
public:
  explicit OStackAllocator(size_t _size);

  template <class T>
  T *Allocate()
  {
    return reinterpret_cast<T *>(InternalAllocate(sizeof(T)));
  }

  void Deallocate(size_t _size);

  void Clear();

  inline size_t GetMarker() const { return m_marker; }

  inline double GetUsage() const { return (double)m_marker / (double)m_top; }

private:
  uint8 *InternalAllocate(size_t _size);

  size_t m_marker;
  size_t m_top;
  uint8 *m_bottom;
};
} // namespace OnionEngine

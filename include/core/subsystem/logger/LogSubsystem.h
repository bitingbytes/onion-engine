/**
 * @file OLogSubsystem.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-01
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "FileSystem.h"
#include "Types.h"
#include "onionengine_lib.h"

namespace OnionEngine {
enum class ELogLevel : uint8
{
    Debug = 0U,
    Info,
    Warning,
    Error,
    Fatal
};

class OE_EXPORT OLogSubsystem
{
  private:
    static OLogSubsystem* s_Instance;

  public:
    OLogSubsystem();

    ~OLogSubsystem();

    static void Initialize(ELogLevel _level = ELogLevel::Debug);

    static void Shutdown();

    inline static OLogSubsystem& Get() { return *s_Instance; }

    const char* LogLevelToString(ELogLevel _LogLevel);

    void LogInternal(const char* _Category,
                     ELogLevel   _LogLevel,
                     const char* _Format,
                     ...);

    static std::string GetLastErrorAsString();

  private:
    void PlatformLogMessage(ELogLevel _Level);

    void PlatformClearLogMessage();

    OFile* m_logFile;

    bool mb_logToFile;

    ELogLevel m_logLevel;
};
} // OnionEngine

#if OE_DEBUG
#define OE_LOG(Level, Msg, ...)                                                \
    OnionEngine::OLogSubsystem::Get().LogInternal(                              \
      "OnionEngine", Level, Msg, ##__VA_ARGS__)
#define OE_FATAL(Msg, ...)                                                     \
    OnionEngine::OLogSubsystem::Get().LogInternal(                              \
      "OnionEngine", ELogLevel::Fatal, Msg, ##__VA_ARGS__)
#define OE_ERROR(Msg, ...)                                                     \
    OnionEngine::OLogSubsystem::Get().LogInternal(                              \
      "OnionEngine", ELogLevel::Error, Msg, ##__VA_ARGS__)
#define OE_WARN(Msg, ...)                                                      \
    OnionEngine::OLogSubsystem::Get().LogInternal(                              \
      "OnionEngine", ELogLevel::Warning, Msg, ##__VA_ARGS__)
#define OE_INFO(Msg, ...)                                                      \
    OnionEngine::OLogSubsystem::Get().LogInternal(                              \
      "OnionEngine", ELogLevel::Info, Msg, ##__VA_ARGS__)
#define OE_VERB(Msg, ...)                                                      \
    OnionEngine::OLogSubsystem::Get().LogInternal(                              \
      "OnionEngine", ELogLevel::Debug, Msg, ##__VA_ARGS__)
#else
#define OE_LOG(Level, Msg, ...)
#define OE_FATAL(Msg, ...)
#define OE_ERROR(Msg, ...)
#define OE_WARN(Msg, ...)
#define OE_INFO(Msg, ...)
#define OE_VERB(Msg, ...)
#endif
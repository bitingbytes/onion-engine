/**
 * @file OConfigSubsystem.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-10-11
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "CFGParser.h"
#include "onionengine_lib.h"

namespace OnionEngine {
class OConfigSubsystem
{
    static OE_EXPORT OConfigSubsystem* s_Instance;

  public:
    OConfigSubsystem();
    ~OConfigSubsystem();

    static OE_EXPORT void Initialize();

    static OE_EXPORT void Shutdown();

    static OE_EXPORT OSectionData GetSection(const OString& _key);

  private:
    OCFG* m_data;
};
} // namespace OnionEngine

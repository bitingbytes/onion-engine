/**
 * @file OJoystick.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-10
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Vector2D.h"

namespace OnionEngine {
struct OJoystick
{
    Math::OVector2D LeftAxis;
    Math::OVector2D RightAxis;
};
} // OnionEngine

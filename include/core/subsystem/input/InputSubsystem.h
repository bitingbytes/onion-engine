/**
 * @file OInputSubsystem.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-10
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Joystick.h"
#include "onionengine.h"
#include "onionengine_lib.h"

namespace OnionEngine {
class OE_EXPORT OInputSubsystem
{
  private:
    static OInputSubsystem* s_Instance;

    OJoystick m_joysticks[OE_MAX_LOCAL_PLAYERS];

  public:
    OInputSubsystem();
    ~OInputSubsystem();

    static void Initialize();

    static void Shutdown();

    inline static OInputSubsystem& Get() { return *s_Instance; }

    void SetJoystick(uint8 _index, const OJoystick& _joystick);

    inline OJoystick GetJoystick(uint8 _index) const
    {
        return m_joysticks[_index];
    }

    inline void SetButtonStatus(bool _status) { m_button_x = _status; }

    inline bool ButtonXPressed() const { return m_button_x; }

  private:
    uint8 m_button_x : 1; // TODO: Delete
};
} // OnionEngine

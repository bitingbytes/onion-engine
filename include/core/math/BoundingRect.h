/**
 * @file Box.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-18
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Matrix4x4.h"
#include "Vector2D.h"

namespace OnionEngine {
namespace Math {
struct OBoundingRect
{
    OVector2D Min;
    OVector2D Max;

    OBoundingRect()
      : Min(OVector2D(-1.0f, -1.0f))
      , Max(OVector2D(1.0f, 1.0f))
    {}

    OBoundingRect(const OVector2D& _min, const OVector2D& _max)
      : Min(_min)
      , Max(_max)
    {}

    bool Overlapped(const OBoundingRect& _bound)
    {
        return Min > _bound.Min && Max < _bound.Max;
    }

    void Transform(const Matrix4x4& _mat)
    {
        OVector4D vmin(Min.X, Min.Y, 0.0f, 1.0f);
        OVector4D vmax(Max.X, Max.Y, 0.0f, 1.0f);

        vmin = vmin * _mat;
        vmax = vmax * _mat;

        Min = OVector2D(vmin.X, vmin.Y);
        Max = OVector2D(vmax.X, vmax.Y);
    }
};
} // Math
} // OnionEngine

/**
 * @file BaseMath.h
 * @author Brian Batista
 * @brief Base math operations including trigonometry.
 * @version 0.1
 * @date 2018-10-03
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "PlatformDetection.h"
#include "Types.h"
#include <cmath>

namespace OnionEngine {
namespace Math {
constexpr double Pi = 3.1415926535897932384626433832795;

inline static float
DegToRad(float _angle)
{
    return ((float)Pi * _angle) / 180.0f;
}

inline static float
RadToDeg(float _angle)
{
    return 180.0f / (float)Pi * _angle;
}

inline float
InverseSqrt(const float& _X)
{
    return 1.0f / std::sqrt(_X);
}

inline float
Max(const float& _A, const float& _B)
{
    return _A > _B ? _A : _B;
}

inline float
Min(const float& _A, const float& _B)
{
    return _A < _B ? _A : _B;
}

inline float
Sqrt(const float& _X)
{
    return std::sqrt(_X);
}

inline float
Saturate(const float& _X)
{
    return Max(0.0, Min(1.0, _X));
}

inline float
Lerp(const float& _A, const float& _B, const float& _W)
{
    return _A + _W * (_B - _A);
}

template<typename T>
inline T
Abs(T _Value)
{
    return std::abs(_Value);
}
} // namespace Math
}
/**(c) 2015 - 2018 Eremita Studio, Inc.All Rights Reserved.
 * @file OVector4D.h
 * @author Brian Batista
 * @brief Representation of a fourth dimension vector.
 * @version 0.1
 * @date 2018-10-03
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "BaseMath.h"
#include "Serializer.h"
#include "Vector3D.h"
#include "onionengine_lib.h"

namespace OnionEngine {
namespace Math {
/**
 * Represents a fourth dimension vector with components {X}, {Y}, {Z} and {W}.
 */
template<typename T>
struct TVector4D
{
    /* X component. */
    T X;

    /* Y component. */
    T Y;

    /* Z component. */
    T Z;

    /* W component */
    T W;

    /* Cartesian's origin vector. The W component is 1 */
    static const TVector4D Zero;

    /* Vector with all components equal to One. [1, 1, 1, 1]. */
    static const TVector4D One;

    /* Vector points to the Right, aligned with X-Axis. [1, 0, 0, 1]. */
    static const TVector4D Right;

    /* Vector points to the Forward, aligned with Y-Axis. [0, 1, 0, 1]. */
    static const TVector4D Forward;

    /* Vector points to the Up, aligned with Z-Axis. [0, 0, 1, 1]. */
    static const TVector4D Up;

    /**
     * Construct a vector at the cartesian origin.
     *
     * The tuple X, Y and Z are equal to Zero. W is equal 1.
     */
    inline TVector4D() noexcept
      : X(0)
      , Y(0)
      , Z(0)
      , W(1)
    {}

    /**
     * Construct a vector given an scalar. All components, but W, are equal the
     * scalar value.
     *
     * @param _Scalar Value of all the vector's components.
     */
    explicit inline TVector4D(const T& _Scalar) noexcept
      : X(_Scalar)
      , Y(_Scalar)
      , Z(_Scalar)
      , W(1)
    {
        OE_ASSERT(!IsNaN());
    }

    explicit inline TVector4D(const TVector2D<T>& _Other) noexcept
      : X(_Other.X)
      , Y(_Other.Y)
      , Z(0)
      , W(1)
    {
        OE_ASSERT(!IsNaN());
    }

    explicit inline TVector4D(const TVector3D<T>& _Other) noexcept
      : X(_Other.X)
      , Y(_Other.Y)
      , Z(_Other.Z)
      , W(1)
    {
        OE_ASSERT(!IsNaN());
    }

    /**
     * Construct a vector given four scalars. The components are equal to the
     * respective scalars.
     *
     * @param _X Value of the component X.
     * @param _Y Value of the component Y.
     * @param _Z Value of the component Z.
     * @param _W Value of the component W.
     */
    inline TVector4D<T>(const T& _X,
                        const T& _Y,
                        const T& _Z,
                        const T& _W = 1) noexcept
      : X(_X)
      , Y(_Y)
      , Z(_Z)
      , W(_W)
    {
        OE_ASSERT(!IsNaN());
    }

    /**
     * Component-wise addition between this and other vector.
     *
     * @param _Other The vector that going to be added to this one.
     * @return The result of the addition.
     */
    inline TVector4D operator+(const TVector4D& _Other) const
    {
        return TVector4D(
          X + _Other.X, Y + _Other.Y, Z + _Other.Z, W + _Other.W);
    }

    /**
     * Component-wise subtraction between this and other vector.
     *
     * @param _Other The vector that going to be subtracted from this one.
     * @return The result of the subtraction.
     */
    inline TVector4D operator-(const TVector4D& _Other) const
    {
        return TVector4D(
          X - _Other.X, Y - _Other.Y, Z - _Other.Z, W - _Other.W);
    }

    /**
     * Component-wise multiplication between this and other vector.
     *
     * @param _Other The vector that going to be multiplied from this one.
     * @return The result of the multiplication.
     */
    inline TVector4D operator*(const TVector4D& _Other) const
    {
        return TVector4D(
          X * _Other.X, Y * _Other.Y, Z * _Other.Z, W * _Other.W);
    }

    /**
     * Component-wise division between this and other vector.
     *
     * @param _Other The vector that going to be divided from this one.
     * @return The result of the division.
     */
    inline TVector4D operator/(const TVector4D& _Other) const
    {
        OE_ASSERT(_Other.X != 0 && _Other.Y != 0 && _Other.Z != 0 &&
                  _Other.W != 0);

        return TVector4D(
          X / _Other.X, Y / _Other.Y, Z / _Other.Z, W / _Other.W);
    }

    /**
     * Increments this vector by other vector's components value.
     *
     * @param _Other The vector that going to increment this one.
     * @return The result of the incrementation.
     */
    inline TVector4D& operator+=(const TVector4D& _Other)
    {
        X += _Other.X;
        Y += _Other.Y;
        Z += _Other.Z;
        W += _Other.W;

        return *this;
    }

    /**
     * Decrements this vector by other vector's components value.
     *
     * @param _Other The vector that going to decrement this one.
     * @return The result of the decrementation.
     */
    inline TVector4D& operator-=(const TVector4D& _Other)
    {
        X -= _Other.X;
        Y -= _Other.Y;
        Z -= _Other.Z;
        W -= _Other.W;

        return *this;
    }

    /**
     * Multiply this vector's components by other's ones.
     *
     * @param _Other The vector that going to be multplied.
     * @return The result of the multiplication.
     */
    inline TVector4D& operator*=(const TVector4D& _Other)
    {
        X *= _Other.X;
        Y *= _Other.Y;
        Z *= _Other.Z;
        W *= _Other.W;

        return *this;
    }

    /**
     * Divide this vector's components by other's ones.
     *
     * @param _Other The vector that going to be divided.
     * @return The result of the division.
     */
    inline TVector4D& operator/=(const TVector4D& _Other)
    {
        OE_ASSERT(_Other.X != 0 && _Other.Y != 0 && _Other.Z != 0 &&
                  _Other.W != 0);

        X /= _Other.X;
        Y /= _Other.Y;
        Z /= _Other.Z;
        W /= _Other.W;

        return *this;
    }

    /**
     * Addition between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be added to this vector.
     * @return The result of the addition.
     */
    inline TVector4D operator+(const T& _Scalar) const
    {
        return TVector4D(X + _Scalar, Y + _Scalar, Z + _Scalar, W + _Scalar);
    }

    /**
     * Subtraction between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be subtracted from this vector.
     * @return The result of the subtraction.
     */
    inline TVector4D operator-(const T& _Scalar) const
    {
        return TVector4D(X - _Scalar, Y - _Scalar, Z - _Scalar, W - _Scalar);
    }

    /**
     * Multiplication between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be multiplied to this vector.
     * @return The result of the multiplication.
     */
    inline TVector4D operator*(const T& _Scalar) const
    {
        return TVector4D(X * _Scalar, Y * _Scalar, Z * _Scalar, W * _Scalar);
    }

    /**
     * Division between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be divided to this vector.
     * @return The result of the division.
     */
    inline TVector4D operator/(const T& _Scalar) const
    {
        OE_ASSERT(_Scalar != 0);

        return TVector4D(X / _Scalar, Y / _Scalar, Z / _Scalar, W / _Scalar);
    }

    inline TVector4D& operator+=(const T& _Scalar)
    {
        X += _Scalar;
        Y += _Scalar;
        Z += _Scalar;
        W += _Scalar;

        return *this;
    }

    inline TVector4D& operator-=(const T& _Scalar)
    {
        X -= _Scalar;
        Y -= _Scalar;
        Z -= _Scalar;
        W -= _Scalar;

        return *this;
    }

    inline TVector4D& operator*=(const T& _Scalar)
    {
        X *= _Scalar;
        Y *= _Scalar;
        Z *= _Scalar;
        W *= _Scalar;

        return *this;
    }

    inline TVector4D& operator/=(const T& _Scalar)
    {
        OE_ASSERT(_Scalar != 0);

        X /= _Scalar;
        Y /= _Scalar;
        Z /= _Scalar;
        W /= _Scalar;

        return *this;
    }

    /**
     * Inverts the signal of this vector components.
     *
     * @return A negative COPY of this vector.
     */
    inline TVector4D operator-() const { return TVector4D(-X, -Y, -Z, -W); }

    /**
     * Get the components by its index.
     * 0 : X
     * 1 : Y
     * 2 : Z
     * 3 : W
     *
     * @param _Index The index of the vector's component.
     * @return The value of the component.
     */
    inline T operator[](int32 _Index) const
    {
        OE_ASSERT(_Index >= 0 && _Index <= 3);
        if (_Index == 0)
            return X;
        if (_Index == 1)
            return Y;
        if (_Index == 2)
            return Z;
        return W;
    }

    inline TVector3D<T> XYZ() const { return TVector3D<T>(X, Y, Z); }

    /**
     * Get the components by its index.
     * 0 : X
     * 1 : Y
     * 2 : Z
     * 3: W
     *
     * @param _Index The index of the vector's component.
     * @return The value of the component.
     */
    inline T& operator[](int32 _Index)
    {
        OE_ASSERT(_Index >= 0 && _Index <= 3);
        if (_Index == 0)
            return X;
        if (_Index == 1)
            return Y;
        if (_Index == 2)
            return Z;
        return W;
    }

    /**
     * Verify if any of the components are NaN.
     *
     * @return True if any of the components is NaN.
     */
    inline bool IsNaN() const
    {
        return std::isnan(X) || std::isnan(Y) || std::isnan(Z) || std::isnan(W);
    }

    friend Serializer& operator<<(Serializer&   _serializer,
                                  TVector4D<T>& _vector)
    {
        return _serializer << _vector.X << _vector.Y << _vector.Z << _vector.W;
    }

    friend Serializer& operator>>(Serializer&   _serializer,
                                  TVector4D<T>& _vector)
    {
        return _serializer >> _vector.X >> _vector.Y >> _vector.Z << _vector.W;
    }
};

template<typename T>
inline TVector4D<T>
operator+(const T& _Scalar, const TVector4D<T>& _Other)
{
    return TVector4D<T>(_Scalar + _Other.X,
                        _Scalar + _Other.Y,
                        _Scalar + _Other.Z,
                        _Scalar + _Other.W);
}

template<typename T>
inline TVector4D<T>
operator-(const T& _Scalar, const TVector4D<T>& _Other)
{
    return TVector4D<T>(_Scalar - _Other.X,
                        _Scalar - _Other.Y,
                        _Scalar - _Other.Z,
                        _Scalar - _Other.W);
}

template<typename T>
inline TVector4D<T> operator*(const T& _Scalar, const TVector4D<T>& _Other)
{
    return TVector4D<T>(_Scalar * _Other.X,
                        _Scalar * _Other.Y,
                        _Scalar * _Other.Z,
                        _Scalar * _Other.W);
}

template<typename T>
inline TVector4D<T>
operator/(const T& _Scalar, const TVector4D<T>& _Other)
{
    return TVector4D<T>(_Scalar / _Other.X,
                        _Scalar / _Other.Y,
                        _Scalar / _Other.Z,
                        _Scalar / _Other.W);
}

/**
 * Calculate absolute value of the vector.
 *
 * @param _U vector to calculate the absolute value.
 * @return A absolute COPY of the vector.
 */
template<typename T>
inline TVector4D<T>
Abs(const TVector4D<T> _U)
{
    return TVector4D<T>(Abs(_U.X), Abs(_U.Y), Abs(_U.Z), Abs(_U.W));
}

/**
 * Calculate dot product between two vectors.
 * If both vectors are unit vectos, returns the cos(angle) between them.
 *
 * @param _U Left vector of the dot product.
 * @param _V Right vector of the dot product.
 * @return The dot product of the two vector. |_U||_V|cos(O).
 */
template<typename T>
inline constexpr T
Dot(const TVector4D<T> _U, const TVector4D<T> _V)
{
    return _U.X * _V.X + _U.Y * _V.Y + _U.Z * _V.Z + _U.W * _V.W;
}

/**
 * Calculate length(magnitude) squared of the vector.
 *
 * @param _U Vector to return the length squared.
 * @return The length squared of the vector.
 */
template<typename T>
inline constexpr T
LengthSquared(const TVector4D<T> _U)
{
    return Dot(_U, _U);
}

/**
 * Calculate length(magnitude) of the vector.
 *
 * @param _U Vector to return the length.
 * @return The length of the vector.
 */
template<typename T>
inline constexpr T
Length(const TVector4D<T> _U)
{
    return Sqrt(Dot(_U, _U));
}

/**
 * Normalize a vector.
 *
 * @param _U Vector to be normalized.
 * @return A normalized COPY of the vector.
 */
template<typename T>
inline constexpr TVector4D<T>
Normalize(const TVector4D<T> _U)
{
    return _U / Length(_U);
}

/**
 * Get the smaller component.
 *
 * @param _U Vector to get the min component.
 * @return A COPY of the smaller component.
 */
template<typename T>
inline T
MinComponent(const TVector4D<T>& _U)
{
    return Min(_U.X, Min(_U.Y, Min(_U.Z, _U.W)));
}

/**
 * Get the biggest component.
 *
 * @param _U Vector to get the max component.
 * @return A COPY of the biggest component.
 */
template<typename T>
inline T
MaxComponent(const TVector4D<T>& _U)
{
    return Max(_U.X, Max(_U.Y, Max(_U.Z, _U.W)));
}

/**
 * Get the component-wise smaller vector.
 *
 * @return A COPY of the smaller of the two vectors.
 */
template<typename T>
inline TVector4D<T>
Min(const TVector4D<T>& _U, const TVector4D<T>& _V)
{
    return TVector4D<T>(
      Min(_U.X, _V.X), Min(_U.Y, _V.Y), Min(_U.Z, _V.Z), Min(_U.W, _V.W));
}

/**
 * Get the component-wise biggest vector.
 *
 * @return A COPY of the biggest of the two vectors.
 */
template<typename T>
inline TVector4D<T>
Max(const TVector4D<T>& _U, const TVector4D<T>& _V)
{
    return TVector4D<T>(
      Max(_U.X, _V.X), Max(_U.Y, _V.Y), Max(_U.Z, _V.Z), Max(_U.W, _V.W));
}

/**
 * Permutes the components values.*
 */
template<typename T>
inline TVector4D<T>
Permute(const TVector4D<T>& _U, int32 _X, int32 _Y, int32 _Z, int32 _W)
{
    return TVector4D<T>(_U[_X], _U[_Y], _U[_Z], _U[_W]);
}

/**
 * Verify if the vector is normalized.
 *
 * @param _U Vector to be verified.
 * @return True if the vector is normalized.
 */
template<typename T>
inline bool
IsNormalized(const TVector4D<T>& _U)
{
    return Length(_U) >= 0.99999;
}

/* Tuple of doubles */
typedef TVector4D<double> /* ALIGN(32) */ ODouble4;

/* Tuple of floats */
typedef TVector4D<float> /* ALIGN(16) */ OFloat4;

/* Vector4D. Exclusive for real-time applications. */
typedef TVector4D<float> /* ALIGN(16) */ OVector4D;

/* Tuple of integers */
typedef TVector4D<int32> /* ALIGN(16) */ OInt4;

template<typename T>
const TVector4D<T> TVector4D<T>::Zero = TVector4D<T>(0);

template<typename T>
const TVector4D<T> TVector4D<T>::One = TVector4D<T>(1);

template<typename T>
const TVector4D<T> TVector4D<T>::Right = TVector4D<T>(1, 0, 0, 0);

template<typename T>
const TVector4D<T> TVector4D<T>::Forward = TVector4D<T>(0, 1, 0, 0);

template<typename T>
const TVector4D<T> TVector4D<T>::Up = TVector4D<T>(0, 0, 1, 0);
} // namespace Math
} // namespace OnionEngine
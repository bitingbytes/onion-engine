/**
 * @file ORect.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-26
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Types.h"

namespace OnionEngine {
namespace Math {
struct ORect
{
    float PosX;
    float PosY;
    float Width;
    float Height;

    inline ORect()
      : PosX(0.0f)
      , PosY(0.0f)
      , Width(0.0f)
      , Height(0.0f)
    {}

    inline ORect(float _posX, float _posY, float _width, float _height)
      : PosX(_posX)
      , PosY(_posY)
      , Width(_width)
      , Height(_height)
    {}
};
} // Math
} // OnionEngine

/**
 * @file ORay.h
 * @author Brian Batista
 * @brief Representation of a ray with Origin and Direction.
 * @version 0.1
 * @date 2018-10-03
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "Vector3D.h"

namespace OnionEngine {
namespace Math {
/**
 * Represents a ray with an Origin and Direction.
 */
struct ORay
{
  /* Ray's origin. */
  OFloat3 Origin;

  /**
   * Ray's direction.
   * It must be normalized.
   */
  OFloat3 Direction;

  /* Construct a ray with origin at cartezian origin and pointing forward. */
  inline ORay() noexcept
    : Origin()
    , Direction(OFloat3::Forward)
  {}

  /**
   * Contruct a ray with a given origin and direction.
   *
   * @param _Origin The ray's origin.
   * @param _Direction The ray's direction. It must be normalized.
   */
  inline ORay(const OFloat3& _Origin, const OFloat3& _Direction) noexcept
    : Origin(_Origin)
    , Direction(_Direction)
  {}

  /**
   * Gives a ray's point at a given time.
   *
   * @param _Time The time containing the wished point.
   * @return A point belonging to the ray at a given time.
   */
  inline OFloat3 operator()(float _Time) const;
};

inline OFloat3
ORay::operator()(float _Time) const
{
  return Origin + Direction * _Time;
}
} // namespace Math
}
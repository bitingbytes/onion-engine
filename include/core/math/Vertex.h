/**
 * @file Vertex.h
 * @author Brian Batista
 * @brief
 * @version 0.1
 * @date 2018-10-16
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Color.h"
#include "Serializer.h"
#include "Vector2D.h"
#include "Vector3D.h"
#include "onionengine_lib.h"

namespace OnionEngine {
namespace Math {

struct OVertexP
{
    OVector3D Position;
};

struct OVertexP1C
{
    OVector3D         Position;
    TColor<float, 1> OColor;
};

// Cache
struct OVertexP1C1T
{
    OVector3D Position;
    OVector3D OColor;
    OVector2D TexCoord;
};

struct OVertexP1T
{
    OVector3D Position;
    OVector2D TexCoord;
};

struct OVertexP1T1N
{
    OVector3D Position;
    OVector3D Normal;
    OVector2D TexCoord;
};

} // namespace Math
} // namespace OnionEngine
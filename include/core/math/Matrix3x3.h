/**(c) 2015 - 2018 Eremita Studio, Inc.All Rights Reserved.
 * @file Matrix3x3.h
 * @author Brian Batista
 * @brief Representation of a 3x3 Matrix.
 * @version 0.1
 * @date 2018-10-03
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "Vector3D.h"

namespace OnionEngine {
namespace Math {
/**
 * Representation of a 3x3 Matrix.
 * The access is row-major [row][column].
 */
template<typename T>
struct TMatrix3x3
{
  TVector3D<T> Row[3];

  /** Contruct a matrix with all elements equals zero. */
  inline TMatrix3x3() noexcept
  {
    Row[0] = TVector3D<T>();
    Row[1] = TVector3D<T>();
    Row[2] = TVector3D<T>();
  }

  inline TMatrix3x3(const T& _Diagonal) noexcept
  {
    Row[0][0] = _Diagonal;
    Row[1][1] = _Diagonal;
    Row[2][2] = _Diagonal;
  }

  inline TMatrix3x3(const TVector3D<T>& _Row0,
                    const TVector3D<T>& _Row1,
                    const TVector3D<T>& _Row2) noexcept
  {
    Row[0] = _Row0;
    Row[1] = _Row1;
    Row[2] = _Row2;
  }

  inline TMatrix3x3(const T& _RowElement0,
                    const T& _RowElement1,
                    const T& _RowElement2,
                    const T& _RowElement3,
                    const T& _RowElement4,
                    const T& _RowElement5,
                    const T& _RowElement6,
                    const T& _RowElement7,
                    const T& _RowElement8) noexcept
  {
    Row[0] = TVector3D<T>(_RowElement0, _RowElement1, _RowElement2);
    Row[1] = TVector3D<T>(_RowElement3, _RowElement4, _RowElement5);
    Row[2] = TVector3D<T>(_RowElement6, _RowElement7, _RowElement8);
  }
};

typedef TMatrix3x3<double> ODouble3x3;

typedef TMatrix3x3<float> OFloat3x3;

typedef TMatrix3x3<int> OInt3x3;

} // namespace Math
}
/**(c) 2015 - 2018 Eremita Studio, Inc.All Rights Reserved.
 * @file OVector2D.h
 * @author Brian Batista
 * @brief Representation of a second dimension vector.
 * @version 0.1
 * @date 2018-10-03
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "BaseMath.h"
#include "Serializer.h"
#include "onionengine_lib.h"

namespace OnionEngine {
namespace Math {
/**
 * Represents a second dimension vector with components {X} and {Y}.
 */
template<typename T>
struct TVector2D
{
    /* X component. */
    T X;

    /* Y component. */
    T Y;

    /* Cartesian's origin vector. */
    static const TVector2D Zero;

    /* Vector with all components equal to One. [1, 1]. */
    static const TVector2D One;

    /* Vector points to the Right, aligned with X-Axis. [1, 0]. */
    static const TVector2D Right;

    /* Vector points to the Up, aligned with Y-Axis. [0, 1]. */
    static const TVector2D Up;

    /**********************************************
     * Construct a vector at the cartesian origin.
     *
     * The components X and Y are equal to Zero.
     *********************************************/
    inline TVector2D() noexcept
      : X(0)
      , Y(0)
    {}

    /**
     * Construct a vector given an scalar. All components are equal to the
     * scalar value.
     *
     * @param _Scalar Value of all the vector's components.
     */
    explicit inline TVector2D(const T& _Scalar) noexcept
      : X(_Scalar)
      , Y(_Scalar)
    {
        OE_ASSERT(!IsNaN());
    }

    /**
     * Construct a vector given three scalars. The components are equal to the
     * respective scalars.
     *
     * @param _X Value of the component X.
     * @param _Y Value of the component Y.
     */
    inline TVector2D(const T& _X, const T& _Y) noexcept
      : X(_X)
      , Y(_Y)
    {
        OE_ASSERT(!IsNaN());
    }

    /**
     * Component-wise addition between this and other vector.
     *
     * @param _Other The vector that going to be added to this one.
     * @return The result of the addition.
     */
    inline TVector2D operator+(const TVector2D& _Other) const
    {
        return TVector2D(X + _Other.X, Y + _Other.Y);
    }

    /**
     * Component-wise subtraction between this and other vector.
     *
     * @param _Other The vector that going to be subtracted from this one.
     * @return The result of the subtraction.
     */
    inline TVector2D operator-(const TVector2D& _Other) const
    {
        return TVector2D(X - _Other.X, Y - _Other.Y);
    }

    /**
     * Component-wise multiplication between this and other vector.
     *
     * @param _Other The vector that going to be multiplied from this one.
     * @return The result of the multiplication.
     */
    inline TVector2D operator*(const TVector2D& _Other) const
    {
        return TVector2D(X * _Other.X, Y * _Other.Y);
    }

    /**
     * Component-wise division between this and other vector.
     *
     * @param _Other The vector that going to be divided from this one.
     * @return The result of the division.
     */
    inline TVector2D operator/(const TVector2D& _Other) const
    {
        OE_ASSERT(_Other.X != 0 && _Other.Y != 0);

        return TVector2D(X / _Other.X, Y / _Other.Y);
    }

    /**
     * Increments this vector by other vector's components value.
     *
     * @param _Other The vector that going to increment this one.
     * @return The result of the incrementation.
     */
    inline TVector2D& operator+=(const TVector2D& _Other)
    {
        X += _Other.X;
        Y += _Other.Y;

        return *this;
    }

    /**
     * Decrements this vector by other vector's components value.
     *
     * @param _Other The vector that going to decrement this one.
     * @return The result of the decrementation.
     */
    inline TVector2D& operator-=(const TVector2D& _Other)
    {
        X -= _Other.X;
        Y -= _Other.Y;

        return *this;
    }

    /**
     * Multiply this vector's components by other's ones.
     *
     * @param _Other The vector that going to be multiplied.
     * @return The result of the multiplication.
     */
    inline TVector2D& operator*=(const TVector2D& _Other)
    {
        X *= _Other.X;
        Y *= _Other.Y;

        return *this;
    }

    /**
     * Divide this vector's components by other's ones.
     *
     * @param _Other The vector that going to be divided.
     * @return The result of the division.
     */
    inline TVector2D& operator/=(const TVector2D& _Other)
    {
        OE_ASSERT(_Other.X != 0 && _Other.Y != 0);

        X /= _Other.X;
        Y /= _Other.Y;

        return *this;
    }

    /**
     * Addition between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be added to this vector.
     * @return The result of the addition.
     */
    inline TVector2D operator+(const T& _Scalar) const
    {
        return TVector2D(X + _Scalar, Y + _Scalar);
    }

    /**
     * Subtraction between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be subtracted from this vector.
     * @return The result of the subtraction.
     */
    inline TVector2D operator-(const T& _Scalar) const
    {
        return TVector2D(X - _Scalar, Y - _Scalar);
    }

    /**
     * Multiplication between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be multiplied to this vector.
     * @return The result of the multiplication.
     */
    inline TVector2D operator*(const T& _Scalar) const
    {
        return TVector2D(X * _Scalar, Y * _Scalar);
    }

    /**
     * Division between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be divided to this vector.
     * @return The result of the division.
     */
    inline TVector2D operator/(const T& _Scalar) const
    {
        if (_Scalar == 0)
            return TVector2D(0, 0);

        return TVector2D(X / _Scalar, Y / _Scalar);
    }

    inline TVector2D& operator+=(const T& _Scalar)
    {
        X += _Scalar;
        Y += _Scalar;

        return *this;
    }

    inline TVector2D& operator-=(const T& _Scalar)
    {
        X -= _Scalar;
        Y -= _Scalar;

        return *this;
    }

    inline TVector2D& operator*=(const T& _Scalar)
    {
        X *= _Scalar;
        Y *= _Scalar;

        return *this;
    }

    inline TVector2D& operator/=(const T& _Scalar)
    {
        OE_ASSERT(_Scalar != 0);

        X /= _Scalar;
        Y /= _Scalar;

        return *this;
    }

    /**
     * Inverts the signal of this vector components.
     *
     * @return A negative COPY of this vector.
     */
    inline TVector2D operator-() const { return TVector2D(-X, -Y); }

    /**
     * Get the components by its index.
     * 0 : X
     * 1 : Y
     *
     * @param _Index The index of the vector's component.
     * @return The value of the component.
     */
    inline T operator[](int32 _Index) const
    {
        OE_ASSERT(_Index >= 0 && _Index <= 1);
        if (_Index == 0)
            return X;
        return Y;
    }

    /**
     * Get the components by its index.
     * 0 : X
     * 1 : Y
     *
     * @param _Index The index of the vector's component.
     * @return The value of the component.
     */
    inline T& operator[](int32 _Index)
    {
        OE_ASSERT(_Index >= 0 && _Index <= 1);
        if (_Index == 0)
            return X;
        return Y;
    }

    inline bool operator==(const TVector2D& _other) const
    {
        return X == _other.X && Y == _other.Y;
    }

    inline bool operator!=(const TVector2D& _other) const
    {
        return X != _other.X && Y != _other.Y;
    }

    inline bool operator>(const TVector2D& _other) const
    {
        return Length(*this) > Length(_other);
    }

    inline bool operator>=(const TVector2D& _other) const
    {
        return Length(*this) >= Length(_other);
    }

    inline bool operator<(const TVector2D& _other) const
    {
        return Length(*this) < Length(_other);
    }

    inline bool operator<=(const TVector2D& _other) const
    {
        return Length(*this) <= Length(_other);
    }

    /**
     * Verify if any of the components are NaN.
     *
     * @return True if any of the components is NaN.
     */
    inline bool IsNaN() const { return std::isnan(X) || std::isnan(Y); }

    friend Serializer& operator<<(Serializer&   _serializer,
                                  TVector2D<T>& _vector)
    {
        return _serializer << _vector.X << _vector.Y;
    }

    friend Serializer& operator>>(Serializer&   _serializer,
                                  TVector2D<T>& _vector)
    {
        return _serializer >> _vector.X >> _vector.Y;
    }
};

template<typename T>
inline TVector2D<T>
operator+(const T& _Scalar, const TVector2D<T>& _Other)
{
    return TVector2D<T>(_Scalar + _Other.X, _Scalar + _Other.Y);
}

template<typename T>
inline TVector2D<T>
operator-(const T& _Scalar, const TVector2D<T>& _Other)
{
    return TVector2D<T>(_Scalar - _Other.X, _Scalar - _Other.Y);
}

template<typename T>
inline TVector2D<T> operator*(const T& _Scalar, const TVector2D<T>& _Other)
{
    return TVector2D<T>(_Scalar * _Other.X, _Scalar * _Other.Y);
}

template<typename T>
inline TVector2D<T>
operator/(const T& _Scalar, const TVector2D<T>& _Other)
{
    return TVector2D<T>(_Scalar / _Other.X, _Scalar / _Other.Y);
}

/**
 * Calculate absolute value of the vector.
 *
 * @param _U vector to calculate the absolute value.
 * @return A absolute COPY of the vector.
 */
template<typename T>
inline TVector2D<T>
Abs(const TVector2D<T> _U)
{
    return TVector2D<T>(Abs(_U.X), Abs(_U.Y));
}

/**
 * Calculate dot product between two vectors.
 * If both vectors are unit vectos, returns the cos(angle) between them.
 *
 * @param _U Left vector of the dot product.
 * @param _V Right vector of the dot product.
 * @return The dot product of the two vector. |_U||_V|cos(O).
 */
template<typename T>
inline constexpr T
Dot(const TVector2D<T> _U, const TVector2D<T> _V)
{
    return _U.X * _V.X + _U.Y * _V.Y;
}

/**
 * Calculate length(magnitude) squared of the vector.
 *
 * @param _U Vector to return the length squared.
 * @return The length squared of the vector.
 */
template<typename T>
inline constexpr T
LengthSquared(const TVector2D<T> _U)
{
    return Dot(_U, _U);
}

/**
 * Calculate length(magnitude) of the vector.
 *
 * @param _U Vector to return the length.
 * @return The length of the vector.
 */
template<typename T>
inline constexpr T
Length(const TVector2D<T> _U)
{
    return Sqrt(Dot(_U, _U));
}

/**
 * Normalize a vector.
 *
 * @param _U Vector to be normalized.
 * @return A normalized COPY of the vector.
 */
template<typename T>
inline constexpr TVector2D<T>
Normalize(const TVector2D<T> _U)
{
    return _U / Length(_U);
}

/**
 * Get the smaller component.
 *
 * @param _U Vector to get the min component.
 * @return A COPY of the smaller component.
 */
template<typename T>
inline T
MinComponent(const TVector2D<T>& _U)
{
    return Min(_U.X, _U.Y);
}

/**
 * Get the biggest component.
 *
 * @param _U Vector to get the max component.
 * @return A COPY of the biggest component.
 */
template<typename T>
inline T
MaxComponent(const TVector2D<T>& _U)
{
    return Max(_U.X, _U.Y);
}

/**
 * Get the component-wise smaller vector.
 *
 * @return A COPY of the smaller of the two vectors.
 */
template<typename T>
inline TVector2D<T>
Min(const TVector2D<T>& _U, const TVector2D<T>& _V)
{
    return TVector2D<T>(Min(_U.X, _V.X), Min(_U.Y, _V.Y));
}

/**
 * Get the component-wise biggest vector.
 *
 * @return A COPY of the biggest of the two vectors.
 */
template<typename T>
inline TVector2D<T>
Max(const TVector2D<T>& _U, const TVector2D<T>& _V)
{
    return TVector2D<T>(Max(_U.X, _V.X), Max(_U.Y, _V.Y));
}

/**
 * Permutes the components values.*
 */
template<typename T>
inline TVector2D<T>
Permute(const TVector2D<T>& _U, int32 _X, int32 _Y)
{
    return TVector2D<T>(_U[_X], _U[_Y]);
}

/**
 * Verify if the vector is normalized.
 *
 * @param _U Vector to be verified.
 * @return True if the vector is normalized.
 */
template<typename T>
inline bool
IsNormalized(const TVector2D<T>& _U)
{
    return Length(_U) >= 0.99999;
}

/* Tuple of doubles */
typedef TVector2D<double> ODouble2;

/* Tuple of floats */
typedef TVector2D<float> OFloat2;

/* Vector2D. Exclusive for real-time applications. */
typedef TVector2D<float> OVector2D;

/* Tuple of integers */
typedef TVector2D<int32> OInt2;

template<typename T>
const TVector2D<T> TVector2D<T>::Zero = TVector2D<T>(0);

template<typename T>
const TVector2D<T> TVector2D<T>::One = TVector2D<T>(1);

template<typename T>
const TVector2D<T> TVector2D<T>::Right = TVector2D<T>(1, 0);

template<typename T>
const TVector2D<T> TVector2D<T>::Up = TVector2D<T>(0, 1);
} // namespace Math
} // namespace OnionEngine
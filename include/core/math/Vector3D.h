/**(c) 2015 - 2018 Eremita Studio, Inc.All Rights Reserved.
 * @file OVector3D.h
 * @author Brian Batista
 * @brief Representation of a third dimension vector.
 * @version 0.1
 * @date 2018-10-03
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "BaseMath.h"
#include "Serializer.h"
#include "Vector2D.h"
#include "onionengine_lib.h"

namespace OnionEngine {
namespace Math {
/**
 * Represents a third dimension vector with components {X}, {Y} and {Z}.
 */
template<typename T>
struct TVector3D
{
    /* X component. */
    T X;

    /* Y component. */
    T Y;

    /* Z component. */
    T Z;

    /* Cartesian's origin vector. */
    static const TVector3D Zero;

    /* Vector with all components equal to One. [1, 1, 1]. */
    static const TVector3D One;

    /* Vector points to the Right, aligned with X-Axis. [1, 0, 0]. */
    static const TVector3D Right;

    /* Vector points to the Forward, aligned with Y-Axis. [0, 1, 0]. */
    static const TVector3D Forward;

    /* Vector points to the Up, aligned with Z-Axis. [0, 0, 1]. */
    static const TVector3D Up;

    /**
     * Construct a vector at the cartesian origin.
     *
     * The tuple X, Y and Z are equal to Zero.
     */
    inline TVector3D() noexcept
      : X(0)
      , Y(0)
      , Z(0)
    {}

    /**
     * Construct a vector given an scalar. All components are equal the scalar
     * value.
     *
     * @param _Scalar Value of all the vector's components.
     */
    explicit inline TVector3D(const T& _Scalar) noexcept
      : X(_Scalar)
      , Y(_Scalar)
      , Z(_Scalar)
    {
        OE_ASSERT(!IsNaN());
    }

    /**
     * Construct a vector given three scalars. The components are equal to the
     * respective scalars.
     *
     * @param _X Value of the component X.
     * @param _Y Value of the component Y.
     * @param _Z Value of the component Z.
     */
    inline TVector3D(const T& _X, const T& _Y, const T& _Z) noexcept
      : X(_X)
      , Y(_Y)
      , Z(_Z)
    {
        OE_ASSERT(!IsNaN());
    }

    /**
     * Component-wise addition between this and other vector.
     *
     * @param _Other The vector that going to be added to this one.
     * @return The result of the addition.
     */
    inline TVector3D operator+(const TVector3D& _Other) const
    {
        return TVector3D(X + _Other.X, Y + _Other.Y, Z + _Other.Z);
    }

    /**
     * Component-wise subtraction between this and other vector.
     *
     * @param _Other The vector that going to be subtracted from this one.
     * @return The result of the subtraction.
     */
    inline TVector3D operator-(const TVector3D& _Other) const
    {
        return TVector3D(X - _Other.X, Y - _Other.Y, Z - _Other.Z);
    }

    /**
     * Component-wise multiplication between this and other vector.
     *
     * @param _Other The vector that going to be multiplied from this one.
     * @return The result of the multiplication.
     */
    inline TVector3D operator*(const TVector3D& _Other) const
    {
        return TVector3D(X * _Other.X, Y * _Other.Y, Z * _Other.Z);
    }

    /**
     * Component-wise division between this and other vector.
     *
     * @param _Other The vector that going to be divided from this one.
     * @return The result of the division.
     */
    inline TVector3D operator/(const TVector3D& _Other) const
    {
        OE_ASSERT(_Other.X != 0 && _Other.Y != 0 && _Other.Z != 0);

        return TVector3D(X / _Other.X, Y / _Other.Y, Z / _Other.Z);
    }

    /**
     * Increments this vector by other vector's components value.
     *
     * @param _Other The vector that going to increment this one.
     * @return The result of the incrementation.
     */
    inline TVector3D& operator+=(const TVector3D& _Other)
    {
        X += _Other.X;
        Y += _Other.Y;
        Z += _Other.Z;

        return *this;
    }

    /**
     * Decrements this vector by other vector's components value.
     *
     * @param _Other The vector that going to decrement this one.
     * @return The result of the decrementation.
     */
    inline TVector3D& operator-=(const TVector3D& _Other)
    {
        X -= _Other.X;
        Y -= _Other.Y;
        Z -= _Other.Z;

        return *this;
    }

    /**
     * Multiply this vector's components by other's ones.
     *
     * @param _Other The vector that going to be multplied.
     * @return The result of the multiplication.
     */
    inline TVector3D& operator*=(const TVector3D& _Other)
    {
        X *= _Other.X;
        Y *= _Other.Y;
        Z *= _Other.Z;

        return *this;
    }

    /**
     * Divide this vector's components by other's ones.
     *
     * @param _Other The vector that going to be divided.
     * @return The result of the division.
     */
    inline TVector3D& operator/=(const TVector3D& _Other)
    {
        OE_ASSERT(_Other.X != 0 && _Other.Y != 0 && _Other.Z != 0);

        X /= _Other.X;
        Y /= _Other.Y;
        Z /= _Other.Z;

        return *this;
    }

    /**
     * Addition between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be added to this vector.
     * @return The result of the addition.
     */
    inline TVector3D operator+(const T& _Scalar) const
    {
        return TVector3D(X + _Scalar, Y + _Scalar, Z + _Scalar);
    }

    /**
     * Subtraction between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be subtracted from this vector.
     * @return The result of the subtraction.
     */
    inline TVector3D operator-(const T& _Scalar) const
    {
        return TVector3D(X - _Scalar, Y - _Scalar, Z - _Scalar);
    }

    /**
     * Multiplication between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be multiplied to this vector.
     * @return The result of the multiplication.
     */
    inline TVector3D operator*(const T& _Scalar) const
    {
        return TVector3D(X * _Scalar, Y * _Scalar, Z * _Scalar);
    }

    /**
     * Division between this vector's components and a scalar.
     *
     * @param _Scalar The scalar that going to be divided to this vector.
     * @return The result of the division.
     */
    inline TVector3D operator/(const T& _Scalar) const
    {
        if (_Scalar == 0)
            return TVector3D::Zero;

        return TVector3D(X / _Scalar, Y / _Scalar, Z / _Scalar);
    }

    inline TVector3D& operator+=(const T& _Scalar)
    {
        X += _Scalar;
        Y += _Scalar;
        Z += _Scalar;

        return *this;
    }

    inline TVector3D& operator-=(const T& _Scalar)
    {
        X -= _Scalar;
        Y -= _Scalar;
        Z -= _Scalar;

        return *this;
    }

    inline TVector3D& operator*=(const T& _Scalar)
    {
        X *= _Scalar;
        Y *= _Scalar;
        Z *= _Scalar;

        return *this;
    }

    inline TVector3D& operator/=(const T& _Scalar)
    {
        OE_ASSERT(_Scalar != 0);

        X /= _Scalar;
        Y /= _Scalar;
        Z /= _Scalar;

        return *this;
    }

    /**
     * Inverts the signal of this vector components.
     *
     * @return A negative COPY of this vector.
     */
    inline TVector3D operator-() const { return TVector3D(-X, -Y, -Z); }

    /**
     * Get the components by its index.
     * 0 : X
     * 1 : Y
     * 2 : Z
     *
     * @param _Index The index of the vector's component.
     * @return The value of the component.
     */
    inline T operator[](int32 _Index) const
    {
        OE_ASSERT(_Index >= 0 && _Index <= 2);
        if (_Index == 0)
            return X;
        if (_Index == 1)
            return Y;
        return Z;
    }

    /**
     * Get the components by its index.
     * 0 : X
     * 1 : Y
     * 2 : Z
     *
     * @param _Index The index of the vector's component.
     * @return The value of the component.
     */
    inline T& operator[](int32 _Index)
    {
        OE_ASSERT(_Index >= 0 && _Index <= 2);
        if (_Index == 0)
            return X;
        if (_Index == 1)
            return Y;
        return Z;
    }

    inline TVector2D<T> XY() const { return TVector2D<T>(X, Y); }

    /**
     * Verify if any of the components are NaN.
     *
     * @return True if any of the components is NaN.
     */
    inline bool IsNaN() const
    {
        return std::isnan(X) || std::isnan(Y) || std::isnan(Z);
    }

    friend Serializer& operator<<(Serializer&   _serializer,
                                  TVector3D<T>& _vector)
    {
        return _serializer << _vector.X << _vector.Y << _vector.Z;
    }

    friend Serializer& operator>>(Serializer&   _serializer,
                                  TVector3D<T>& _vector)
    {
        return _serializer >> _vector.X >> _vector.Y >> _vector.Z;
    }
};

template<typename T>
inline TVector3D<T>
operator+(const T& _Scalar, const TVector3D<T>& _Other)
{
    return TVector3D<T>(
      _Scalar + _Other.X, _Scalar + _Other.Y, _Scalar + _Other.Z);
}

template<typename T>
inline TVector3D<T>
operator-(const T& _Scalar, const TVector3D<T>& _Other)
{
    return TVector3D<T>(
      _Scalar - _Other.X, _Scalar - _Other.Y, _Scalar - _Other.Z);
}

template<typename T>
inline TVector3D<T> operator*(const T& _Scalar, const TVector3D<T>& _Other)
{
    return TVector3D<T>(
      _Scalar * _Other.X, _Scalar * _Other.Y, _Scalar * _Other.Z);
}

template<typename T>
inline TVector3D<T>
operator/(const T& _Scalar, const TVector3D<T>& _Other)
{
    return TVector3D<T>(
      _Scalar / _Other.X, _Scalar / _Other.Y, _Scalar / _Other.Z);
}

/**
 * Calculate absolute value of the vector.
 *
 * @param _U vector to calculate the absolute value.
 * @return A absolute COPY of the vector.
 */
template<typename T>
inline TVector3D<T>
Abs(const TVector3D<T> _U)
{
    return TVector3D<T>(Abs(_U.X), Abs(_U.Y), Abs(_U.Z));
}

/**
 * Calculate dot product between two vectors.
 * If both vectors are unit vectos, returns the cos(angle) between them.
 *
 * @param _U Left vector of the dot product.
 * @param _V Right vector of the dot product.
 * @return The dot product of the two vector. |_U||_V|cos(O).
 */
template<typename T>
inline constexpr T
Dot(const TVector3D<T> _U, const TVector3D<T> _V)
{
    return _U.X * _V.X + _U.Y * _V.Y + _U.Z * _V.Z;
}

/**
 * Calculate cross product between two vectors.
 * If both vectors are unit vectos, returns the sin(angle) between them.
 *
 * @param _U Left vector of the cross product.
 * @param _V Right vector of the cross product.
 * @return A vector orthogonal of the two vectors. |_U||_V|sin(O).
 */
template<typename T>
inline TVector3D<T>
Cross(const TVector3D<T> _U, const TVector3D<T> _V)
{
    return TVector3D<T>((_U.Y * _V.Z) - (_U.Z * _V.Y),
                        (_U.Z * _V.X) - (_U.X * _V.Z),
                        (_U.X * _V.Y) - (_U.Y * _V.X));
}

/**
 * Calculate length(magnitude) squared of the vector.
 *
 * @param _U Vector to return the length squared.
 * @return The length squared of the vector.
 */
template<typename T>
inline constexpr T
LengthSquared(const TVector3D<T> _U)
{
    return Dot(_U, _U);
}

/**
 * Calculate length(magnitude) of the vector.
 *
 * @param _U Vector to return the length.
 * @return The length of the vector.
 */
template<typename T>
inline constexpr T
Length(const TVector3D<T> _U)
{
    return Sqrt(Dot(_U, _U));
}

/**
 * Normalize a vector.
 *
 * @param _U Vector to be normalized.
 * @return A normalized COPY of the vector.
 */
template<typename T>
inline constexpr TVector3D<T>
Normalize(const TVector3D<T> _U)
{
    return _U / Length(_U);
}

/**
 * Get the smaller component.
 *
 * @param _U Vector to get the min component.
 * @return A COPY of the smaller component.
 */
template<typename T>
inline T
MinComponent(const TVector3D<T>& _U)
{
    return Min(_U.X, Min(_U.Y, _U.Z));
}

/**
 * Get the biggest component.
 *
 * @param _U Vector to get the max component.
 * @return A COPY of the biggest component.
 */
template<typename T>
inline T
MaxComponent(const TVector3D<T>& _U)
{
    return Max(_U.X, Max(_U.Y, _U.Z));
}

/**
 * Get the component-wise smaller vector.
 *
 * @return A COPY of the smaller of the two vectors.
 */
template<typename T>
inline TVector3D<T>
Min(const TVector3D<T>& _U, const TVector3D<T>& _V)
{
    return TVector3D<T>(Min(_U.X, _V.X), Min(_U.Y, _V.Y), Min(_U.Z, _V.Z));
}

/**
 * Get the component-wise biggest vector.
 *
 * @return A COPY of the biggest of the two vectors.
 */
template<typename T>
inline TVector3D<T>
Max(const TVector3D<T>& _U, const TVector3D<T>& _V)
{
    return TVector3D<T>(Max(_U.X, _V.X), Max(_U.Y, _V.Y), Max(_U.Z, _V.Z));
}

/**
 * Permutes the components values.*
 */
template<typename T>
inline TVector3D<T>
Permute(const TVector3D<T>& _U, int32 _X, int32 _Y, int32 _Z)
{
    return TVector3D<T>(_U[_X], _U[_Y], _U[_Z]);
}

/**
 * Verify if the vector is normalized.
 *
 * @param _U Vector to be verified.
 * @return True if the vector is normalized.
 */
template<typename T>
inline bool
IsNormalized(const TVector3D<T>& _U)
{
    return Length(_U) >= 0.99999;
}

/**
 * Get a local coordinate system of a vector.
 *
 * @param _U Base vector.
 * @param _V Second axis of the coordinate system.
 * @param _W Third axis of the coordinate system.
 */
template<typename T>
inline void
CoordinateSystem(const TVector3D<T>& _U, TVector3D<T>* _V, TVector3D<T>* _W)
{
    if (Abs(_U.X) > Abs(_U.Y))
        *_V = TVector3D<T>(-_U.Z, 0, _U.X) / Sqrt(_U.X * _U.X + _U.Z * _U.Z);
    else
        *_V = TVector3D<T>(0, _U.Z, -_U.Y) / Sqrt(_U.Y * _U.Y + _U.Z * _U.Z);

    *_W = Cross(_U, *_V);
}

/* Tuple of doubles */
typedef TVector3D<double> ODouble3;

/* Tuple of floats */
typedef TVector3D<float> OFloat3;

/* OVector2D. Exclusive for real-time applications. */
typedef TVector3D<float> OVector3D;

/* Tuple of integers */
typedef TVector3D<int32> OInt3;

template<typename T>
const TVector3D<T> TVector3D<T>::Zero = TVector3D<T>(0);

template<typename T>
const TVector3D<T> TVector3D<T>::One = TVector3D<T>(1);

template<typename T>
const TVector3D<T> TVector3D<T>::Right = TVector3D<T>(1, 0, 0);

template<typename T>
const TVector3D<T> TVector3D<T>::Forward = TVector3D<T>(0, 1, 0);

template<typename T>
const TVector3D<T> TVector3D<T>::Up = TVector3D<T>(0, 0, 1);
} // namespace Math
} // namespace OnionEngine
/**
 * @file OColor.h
 * @author Brian Batista
 * @brief Representation of a RGBA color.
 * @version 0.1
 * @date 2018-10-03
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "BaseMath.h"
#include "Serializer.h"
namespace OnionEngine {
namespace Math {
/**
 * @brief Representation of the RGBA color.
 *
 * @tparam T Type of the components.
 * @tparam T_MAX Max value of the components.
 */
template<class T, uint8 T_MAX>
struct TColor
{
  T R;
  T G;
  T B;
  T A;

  /**
   * @brief Construct a new TColor object.
   *
   */
  inline TColor() noexcept {}

  /**
   * @brief Construct a new TColor object.
   *
   * @param _R Red component.
   * @param _B Blue component.
   * @param _G Green component.
   * @param _A Alpha component.
   */
  inline TColor(T _R, T _B, T _G, T _A = T_MAX) noexcept
    : R(_R)
    , G(_G)
    , B(_B)
    , A(_A)
  {}

  inline TColor operator+(const TColor& _Other) const
  {
    return TColor(R + _Other.R, G + _Other.G, B + _Other.B, A);
  }

  inline TColor operator-(const TColor& _Other) const
  {
    return TColor(R - _Other.R, G - _Other.G, B - _Other.B, A);
  }

  inline TColor operator*(const TColor& _Other) const
  {
    return TColor(R * _Other.R, G * _Other.G, B * _Other.B, A);
  }

  inline TColor operator/(const TColor& _Other) const
  {
    return TColor(R / _Other.R, G / _Other.G, B / _Other.B, A);
  }

  inline TColor& operator+=(const TColor& _Other)
  {
    R += _Other.R;
    G += _Other.G;
    B += _Other.B;

    return *this;
  }

  inline TColor& operator-=(const TColor& _Other)
  {
    R -= _Other.R;
    G -= _Other.G;
    B -= _Other.B;

    return *this;
  }

  inline TColor& operator*=(const TColor& _Other)
  {
    R *= _Other.R;
    G *= _Other.G;
    B *= _Other.B;

    return *this;
  }

  inline TColor& operator/=(const TColor& _Other)
  {
    R /= _Other.R;
    G /= _Other.G;
    B /= _Other.B;

    return *this;
  }

  /** Scalar **/
  inline TColor operator+(const float& _Other) const
  {
    return TColor(R + _Other, G + _Other, B + _Other, A);
  }

  inline TColor operator-(const float& _Other) const
  {
    return TColor(R - _Other, G - _Other, B - _Other, A);
  }

  inline TColor operator*(const float& _Other) const
  {
    return TColor(R * _Other, G * _Other, B * _Other, A);
  }

  inline TColor operator/(const float& _Other) const
  {
    return TColor(R / _Other, G / _Other, B / _Other, A);
  }

  inline TColor& operator+=(const float& _Other)
  {
    R += _Other;
    G += _Other;
    B += _Other;

    return *this;
  }

  inline TColor& operator-=(const float& _Other)
  {
    R -= _Other;
    G -= _Other;
    B -= _Other;

    return *this;
  }

  inline TColor& operator*=(const float& _Other)
  {
    R *= _Other;
    G *= _Other;
    B *= _Other;

    return *this;
  }

  inline TColor& operator/=(const float& _Other)
  {
    R /= _Other;
    G /= _Other;
    B /= _Other;

    return *this;
  }

  friend Serializer& operator<<(Serializer& _serializer,
                                          TColor<T, T_MAX>& _color)
  {
    return _serializer << _color.R << _color.G << _color.B << _color.A;
  }

  friend Serializer& operator>>(Serializer& _serializer,
                                          TColor<T, T_MAX>& _color)
  {
    return _serializer >> _color.R >> _color.G >> _color.B << _color.A;
  }
};

/**
 * @brief 8 bits color at range(0, 255).
 *
 */
typedef TColor<uint8, 255> OColor8;

/**
 * @brief Float color.
 *
 */
typedef TColor<float, 1> OColor;
}
}
/**
 * @file OSphere.h
 * @author Brian Batista
 * @brief Represents a sphere with Center and Radius.
 * @version 0.1
 * @date 2018-10-03
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "Vector3D.h"

namespace OnionEngine {
namespace Math {
struct OSphere
{
  /** Radius */
  float Radius;

  /** Center */
  OVector3D Center;

  /**
   * @brief Construct a new Sphere object.
   *
   */
  inline OSphere() noexcept;

  /**
   * @brief Construct a new Sphere object
   *
   * @param _Radius The radius.
   * @param _Center The center.
   */
  inline OSphere(const float& _Radius,
                const OVector3D _Center = OVector3D::Zero) noexcept;

  /**
   * @brief Get the Surface Area of the Sphere.
   *
   * @return float The surface area.
   */
  inline float GetArea() const;

  /**
   * @brief Get the Volume of the Sphere.
   *
   * $$3/4 * \pi * r ^ 3$$
   *
   * @return float
   */
  inline float GetVolume() const;

  /**
   * @brief Verify if a sphere intersects _Other
   *
   * @param _Other The other sphere.
   * @return true If the spheres intersects eachother.
   * @return false If the spheres do not intersects eachother.
   */
  inline bool Intersection(const OSphere& _Other) const;

  /**
   * @brief Verify if a ray intersects a sphere.
   *
   * @param _Ray The ray.
   * @param _Depth The distance from the ray to the point of intersection.
   * @return true If the ray intersects the sphere.
   * @return false If the ray do not intersects the sphere.
   */
  inline bool Intersection(const ORay& _Ray, float& _Depth) const;
};

inline OSphere::OSphere() noexcept
  : Radius(0.0f)
  , Center(OVector3D::Zero)
{}

inline OSphere::OSphere(const float& _Radius, const OVector3D _Center) noexcept
  : Radius(_Radius)
  , Center(_Center)
{}

inline float
OSphere::GetArea() const
{
  return 0;
}

inline float
OSphere::GetVolume() const
{
  return 0.75f * Math::Pi * Radius * Radius * Radius;
}

inline bool
OSphere::Intersection(const ORay& _Ray, float& _Depth) const
{
  OVector3D op = Center - _Ray.Origin;
  float eps = 1e-4f;
  float b = Dot(op, _Ray.Direction);
  float det = b * b - Dot(op, op) + Radius * Radius;
  if (det < 0)
    return false;
  else
    det = Math::Sqrt(det);

  if ((_Depth = b - det) > eps)
    return true;
  else if ((_Depth = b + det) > eps)
    return true;
  else
    return false;
}

inline bool
OSphere::Intersection(const OSphere& _Other) const
{
  const float Distance = Length(_Other.Center - Center);
  const float RadiusSum = Radius + _Other.Radius;

  return Distance < RadiusSum;
}
} // namespace Math
}
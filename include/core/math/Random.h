/**
 * @file Random.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-29
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include <functional>
#include <random>

namespace OnionEngine {
namespace Math {
namespace Random {
static thread_local std::random_device dev;
static thread_local std::mt19937       engine(dev());

inline int
RandInt()
{
    return engine();
}

inline unsigned int
RandUInt()
{
    return engine();
}
}
} // Math
} // OnionEngine

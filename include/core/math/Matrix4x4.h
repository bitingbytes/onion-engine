// <Eremita Copyrights>

#pragma once

#include "Vector3D.h"
#include "Vector4D.h"

namespace OnionEngine {
namespace Math {
/*
 *   Matrix
 *   Row Major
 *   z-axis
 *     ^
 *     |
 *     |  y-axis
 *     |  /
 *     | /
 *     |/
 *     +--------------->   x-axis
 */
struct Matrix4x4
{
    union
    {
        float    E[4][4];
        float    D[16];
        OVector4D Row[4];
    };

    inline Matrix4x4();

    inline Matrix4x4(float InDi);

    inline Matrix4x4& operator=(const Matrix4x4& InOther);

    inline Matrix4x4(const OVector4D& InRow0,
                     const OVector4D& InRow1,
                     const OVector4D& InRow2,
                     const OVector4D& InRow3);

    inline Matrix4x4(const OVector3D& InRow0,
                     const OVector3D& InRow1,
                     const OVector3D& InRow2,
                     const OVector3D& InRow3 = OVector3D::One);

    inline Matrix4x4 operator*(const Matrix4x4& Other) const;

    inline void operator*=(const Matrix4x4& Other);

    inline Matrix4x4 operator*(float InScalar) const;

    inline Matrix4x4 operator+(const Matrix4x4& Other) const;

    inline Matrix4x4& operator+=(const Matrix4x4& Other);

    inline Matrix4x4 operator+(float InScalar) const;

    inline Matrix4x4 Transposed() const;

    inline static Matrix4x4 Translate(const OVector3D& InTranslation)
    {
        Matrix4x4 Temp = Matrix4x4(1.0f);
        Temp.E[3][0]   = InTranslation.X;
        Temp.E[3][1]   = InTranslation.Y;
        Temp.E[3][2]   = InTranslation.Z;

        return Temp;
    }

    inline static Matrix4x4 Scale(const OVector3D& InScale)
    {
        Matrix4x4 Temp = Matrix4x4(1.0f);
        Temp.E[0][0]   = InScale.X;
        Temp.E[1][1]   = InScale.Y;
        Temp.E[2][2]   = InScale.Z;

        return Temp;
    }

    inline static Matrix4x4 Rotate(const OVector3D& _rotation)
    {
        return Rotate(_rotation.Y, _rotation.X, _rotation.Z);
    }

    inline static Matrix4x4 Rotate(float InYaw, float InPitch, float InRoll)
    {
        return RotateX(InPitch) * RotateY(InYaw) * RotateZ(InRoll);
    }

    inline static Matrix4x4 RotateX(float InAngle)
    {
        float CosAngle = std::cos(InAngle);
        float SinAngle = std::sin(InAngle);

        return Matrix4x4(OVector4D(1.0f, 0.0f, 0.0f, 0.0f),
                         OVector4D(0.0f, CosAngle, -SinAngle, 0.0f),
                         OVector4D(0.0f, SinAngle, CosAngle, 0.0f),
                         OVector4D(0.0f, 0.0f, 0.0f, 1.0f));
    }

    inline static Matrix4x4 RotateY(float InAngle)
    {
        float CosAngle = std::cos(InAngle);
        float SinAngle = std::sin(InAngle);

        return Matrix4x4(OVector4D(CosAngle, 0.0f, SinAngle, 0.0f),
                         OVector4D(0.0f, 1.0f, 0.0f, 0.0f),
                         OVector4D(-SinAngle, 0.0f, CosAngle, 0.0f),
                         OVector4D(0.0f, 0.0f, 0.0f, 1.0f));
    }

    inline static Matrix4x4 RotateZ(float InAngle)
    {
        float CosAngle = std::cos(InAngle);
        float SinAngle = std::sin(InAngle);

        return Matrix4x4(OVector4D(CosAngle, -SinAngle, 0.0f, 0.0f),
                         OVector4D(SinAngle, CosAngle, 0.0f, 0.0f),
                         OVector4D(0.0f, 0.0f, 1.0f, 0.0f),
                         OVector4D(0.0f, 0.0f, 0.0f, 1.0f));
    }

    inline static Matrix4x4 TRS(const Math::OVector3D& _position,
                                const Math::OVector3D& _rotation,
                                const Math::OVector3D& _scale)
    {
        return Translate(_position) * Rotate(_rotation) * Scale(_scale);
    }

    inline static Matrix4x4 SRT(const Math::OVector3D& _position,
                                const Math::OVector3D& _rotation,
                                const Math::OVector3D& _scale)
    {
        return Scale(_scale) * Rotate(_rotation) * Translate(_position);
    }

    inline static Matrix4x4 RS(const Math::OVector3D& _rotation,
                               const Math::OVector3D& _scale)
    {
        return Rotate(_rotation) * Scale(_scale);
    }

    inline static Matrix4x4 SR(const Math::OVector3D& _rotation,
                               const Math::OVector3D& _scale)
    {
        return Scale(_scale) * Rotate(_rotation);
    }

    inline static Matrix4x4 Perspective(float InNear,
                                        float InFar,
                                        float InAspect,
                                        float InFov)
    {

        // FPlane(1.0f / FMath::Tan(HalfFOV), 0.0f, 0.0f, 0.0f),
        // FPlane(0.0f, Width / FMath::Tan(HalfFOV) / Height, 0.0f, 0.0f),
        // FPlane(0.0f, 0.0f, ((MinZ == MaxZ) ? (1.0f - Z_PRECISION) : MaxZ /
        // (MaxZ - MinZ)), 1.0f), FPlane(0.0f, 0.0f, -MinZ * ((MinZ == MaxZ) ?
        // (1.0f - Z_PRECISION) : MaxZ / (MaxZ - MinZ)), 0.0f)
        float HalfFov = Math::DegToRad(InFov) * 0.5f;
        return Matrix4x4(
          OVector4D(1.0f / std::tan(HalfFov), 0.0f, 0.0f, 0.0),
          OVector4D(0.0f, 0.0f, InFar / (InFar - InNear), 1.0f),
          OVector4D(0.0f, InAspect / std::tan(HalfFov), 0.0f, 0.0f),
          OVector4D(0.0f, 0.0f, -InNear * InFar / (InFar - InNear), 0.0f));
    }

    inline static Matrix4x4 Orthographic(
      float Right, float Left, float Top, float Bottom, float Near, float Far)
    {
        return Matrix4x4(
          OVector4D(2.0f / (Right - Left),
                   0.0f,
                   0.0f,
                   -(Right + Left) / (Right - Left)),
          OVector4D(0.0f,
                   2.0f / (Top - Bottom),
                   0.0f,
                   -(Top + Bottom) / (Top - Bottom)),
          OVector4D(0.0f, 0.0f, -1.0f / (Far - Near), -Near / (Far - Near)),
          OVector4D(0.0f, 0.0f, 0.0f, 1.0f));
    }

    inline static Matrix4x4 Orthographic(float Width,
                                         float Height,
                                         float Near,
                                         float Far)
    {
        return Matrix4x4(
          OVector4D(1.0f / Width, 0.0f, 0.0f, 0.0f),
          OVector4D(0.0f, 1.0f / Height, 0.0f, 0.0f),
          OVector4D(0.0f, 0.0f, -2.0f / (Far - Near), -(Far + Near) / (Far - Near)),
          OVector4D(0.0f, 0.0f, 0.0f, 1.0f));
    }

    inline static Matrix4x4 LookAt(const OVector3D& InEyePosition,
                                   const OVector3D& InFocusPoint,
                                   const OVector3D& InUpDirection = OVector3D::Up)
    {
        const OVector3D ForwardVector =
          Normalize(InFocusPoint - InEyePosition); // Y
        const OVector3D RightVector =
          Normalize(Cross(ForwardVector, InUpDirection));            // X
        const OVector3D UpVector = Cross(RightVector, ForwardVector); // Z

        Matrix4x4 M(1.0f);

        for (uint32 RowIndex = 0; RowIndex < 3; RowIndex++) {
            M.E[RowIndex][0] = (&RightVector.X)[RowIndex];   // X
            M.E[RowIndex][1] = (&UpVector.X)[RowIndex];      // Z
            M.E[RowIndex][2] = (&ForwardVector.X)[RowIndex]; // Y
            M.E[RowIndex][3] = 0.0f;
        }
        M.E[3][0] = -Dot(InEyePosition, RightVector);   // X
        M.E[3][1] = -Dot(InEyePosition, UpVector);      // Z
        M.E[3][2] = -Dot(InEyePosition, ForwardVector); // Y
        M.E[3][3] = 1.0f;

        return M;
    }

    /*
    inline static Matrix4x4 LookAt(const Vector3D& InEyePosition, const
    Vector3D& InFocusPoint, const Vector3D& InUpDirection = Vector3D::Up)
    {
            Vector3D Forward = (InFocusPoint - InEyePosition).Normalized();
            Vector3D Right = Vector3D::Cross(Forward,
    InUpDirection.Normalized()); Vector3D Up = Vector3D::Cross(Right,
    Forward).Normalized();

            Matrix4x4 Temp(1.0f);

            Temp.E[0][0] = Right.X;
            Temp.E[0][1] = Right.Y;
            Temp.E[0][2] = Right.Z;

            Temp.E[1][0] = Up.X;
            Temp.E[1][1] = Up.Y;
            Temp.E[1][2] = Up.Z;

            Temp.E[2][0] = Forward.X;
            Temp.E[2][1] = Forward.Y;
            Temp.E[2][2] = Forward.Z;

            return Temp * Translate(-InEyePosition);
    }
    */
    const char* ToString() const
    {
        std::string Temp;

        for (uint32 j = 0; j < 4; ++j) {
            Temp += "|";
            for (uint32 i = 0; i < 4; ++i) {
                Temp += std::to_string(E[i][j]);
                if (i != 3)
                    Temp += "\t";
            }
            Temp += "|\n";
        }

        return Temp.c_str();
    }
};

inline Matrix4x4::Matrix4x4()
{
    for (int32 i = 0; i < 16; i++)
        D[i] = 0.0f;
}

inline Matrix4x4::Matrix4x4(float InDi)
{
    E[0][0] = InDi;
    E[0][1] = 0.0f;
    E[0][2] = 0.0f;
    E[0][3] = 0.0f;
    E[1][0] = 0.0f;
    E[1][1] = InDi;
    E[1][2] = 0.0f;
    E[1][3] = 0.0f;
    E[2][0] = 0.0f;
    E[2][1] = 0.0f;
    E[2][2] = InDi;
    E[2][3] = 0.0f;
    E[3][0] = 0.0f;
    E[3][1] = 0.0f;
    E[3][2] = 0.0f;
    E[3][3] = InDi;
}

inline Matrix4x4&
Matrix4x4::operator=(const Matrix4x4& InOther)
{
    Row[0] = InOther.Row[0];
    Row[1] = InOther.Row[1];
    Row[2] = InOther.Row[2];
    Row[3] = InOther.Row[3];

    return *this;
}

inline Matrix4x4::Matrix4x4(const OVector4D& InRow0,
                            const OVector4D& InRow1,
                            const OVector4D& InRow2,
                            const OVector4D& InRow3)
{
    E[0][0] = InRow0.X;
    E[0][1] = InRow0.Y;
    E[0][2] = InRow0.Z;
    E[0][3] = InRow0.W;
    E[1][0] = InRow1.X;
    E[1][1] = InRow1.Y;
    E[1][2] = InRow1.Z;
    E[1][3] = InRow1.W;
    E[2][0] = InRow2.X;
    E[2][1] = InRow2.Y;
    E[2][2] = InRow2.Z;
    E[2][3] = InRow2.W;
    E[3][0] = InRow3.X;
    E[3][1] = InRow3.Y;
    E[3][2] = InRow3.Z;
    E[3][3] = InRow3.W;
}

inline Matrix4x4::Matrix4x4(const OVector3D& InRow0,
                            const OVector3D& InRow1,
                            const OVector3D& InRow2,
                            const OVector3D& InRow3)
{
    E[0][0] = InRow0.X;
    E[0][1] = InRow0.Y;
    E[0][2] = InRow0.Z;
    E[0][3] = 1.0f;
    E[1][0] = InRow1.X;
    E[1][1] = InRow1.Y;
    E[1][2] = InRow1.Z;
    E[1][3] = 1.0f;
    E[2][0] = InRow2.X;
    E[2][1] = InRow2.Y;
    E[2][2] = InRow2.Z;
    E[2][3] = 1.0f;
    E[3][0] = InRow3.X;
    E[3][1] = InRow3.Y;
    E[3][2] = InRow3.Z;
    E[3][3] = 1.0f;
}

inline static void
MatrixMultiplicator(Matrix4x4*       OutDestination,
                    const Matrix4x4* InSource,
                    const Matrix4x4* InOther)
{
    Matrix4x4 Temp = Matrix4x4(0.0f);

    OVector4D Row0 = OVector4D(InSource->E[0][0],
                             InSource->E[0][1],
                             InSource->E[0][2],
                             InSource->E[0][3]);
    OVector4D Row1 = OVector4D(InSource->E[1][0],
                             InSource->E[1][1],
                             InSource->E[1][2],
                             InSource->E[1][3]);
    OVector4D Row2 = OVector4D(InSource->E[2][0],
                             InSource->E[2][1],
                             InSource->E[2][2],
                             InSource->E[2][3]);
    OVector4D Row3 = OVector4D(InSource->E[3][0],
                             InSource->E[3][1],
                             InSource->E[3][2],
                             InSource->E[3][3]);

    OVector4D Col0 = OVector4D(
      InOther->E[0][0], InOther->E[1][0], InOther->E[2][0], InOther->E[3][0]);
    OVector4D Col1 = OVector4D(
      InOther->E[0][1], InOther->E[1][1], InOther->E[2][1], InOther->E[3][1]);
    OVector4D Col2 = OVector4D(
      InOther->E[0][2], InOther->E[1][2], InOther->E[2][2], InOther->E[3][2]);
    OVector4D Col3 = OVector4D(
      InOther->E[0][3], InOther->E[1][3], InOther->E[2][3], InOther->E[3][3]);

    Temp.E[0][0] = Dot(Row0, Col0);
    Temp.E[0][1] = Dot(Row0, Col1);
    Temp.E[0][2] = Dot(Row0, Col2);
    Temp.E[0][3] = Dot(Row0, Col3);
    Temp.E[1][0] = Dot(Row1, Col0);
    Temp.E[1][1] = Dot(Row1, Col1);
    Temp.E[1][2] = Dot(Row1, Col2);
    Temp.E[1][3] = Dot(Row1, Col3);
    Temp.E[2][0] = Dot(Row2, Col0);
    Temp.E[2][1] = Dot(Row2, Col1);
    Temp.E[2][2] = Dot(Row2, Col2);
    Temp.E[2][3] = Dot(Row2, Col3);
    Temp.E[3][0] = Dot(Row3, Col0);
    Temp.E[3][1] = Dot(Row3, Col1);
    Temp.E[3][2] = Dot(Row3, Col2);
    Temp.E[3][3] = Dot(Row3, Col3);

    memcpy(OutDestination, &Temp, 16 * sizeof(float));
}

inline Matrix4x4 Matrix4x4::operator*(const Matrix4x4& Other) const
{
    Matrix4x4 Temp = Matrix4x4(0.0f);

    MatrixMultiplicator(&Temp, this, &Other);

    return Temp;
}

inline void
Matrix4x4::operator*=(const Matrix4x4& Other)
{
    MatrixMultiplicator(this, this, &Other);
}

inline Matrix4x4 Matrix4x4::operator*(float InScalar) const
{
    Matrix4x4 Temp = *this;
    for (uint32 i = 0; i < 4; ++i) {
        for (uint32 j = 0; j < 4; ++j) {
            Temp.E[i][j] *= InScalar;
        }
    }

    return Temp;
}

inline Matrix4x4
Matrix4x4::operator+(const Matrix4x4& Other) const
{
    Matrix4x4 Temp = *this;
    for (uint32 i = 0; i < 4; ++i) {
        for (uint32 j = 0; j < 4; ++j) {
            Temp.E[i][j] += Other.E[i][j];
        }
    }

    return Temp;
}

inline Matrix4x4&
Matrix4x4::operator+=(const Matrix4x4& Other)
{
    for (uint32 i = 0; i < 4; ++i) {
        for (uint32 j = 0; j < 4; ++j) {
            this->E[i][j] += Other.E[i][j];
        }
    }

    return *this;
}

inline Matrix4x4
Matrix4x4::operator+(float InScalar) const
{
    Matrix4x4 Temp = *this;
    for (uint32 i = 0; i < 4; ++i) {
        for (uint32 j = 0; j < 4; ++j) {
            Temp.E[i][j] += InScalar;
        }
    }

    return Temp;
}

inline OVector4D operator*(const OVector4D& InVector, const Matrix4x4& InMatrix)
{
    OVector4D Col0 = OVector4D(
      InMatrix.E[0][0], InMatrix.E[1][0], InMatrix.E[2][0], InMatrix.E[3][0]);
    OVector4D Col1 = OVector4D(
      InMatrix.E[0][1], InMatrix.E[1][1], InMatrix.E[2][1], InMatrix.E[3][1]);
    OVector4D Col2 = OVector4D(
      InMatrix.E[0][2], InMatrix.E[1][2], InMatrix.E[2][2], InMatrix.E[3][2]);
    OVector4D Col3 = OVector4D(
      InMatrix.E[0][3], InMatrix.E[1][3], InMatrix.E[2][3], InMatrix.E[3][3]);

    float TempX = Dot(InVector, Col0);
    float TempY = Dot(InVector, Col1);
    float TempZ = Dot(InVector, Col2);
    float TempW = Dot(InVector, Col3);

    return OVector4D(TempX, TempY, TempZ, TempW);
}

inline Matrix4x4
Matrix4x4::Transposed() const
{
    Matrix4x4 Temp;

    for (uint32 j = 0; j < 4; j++) {
        for (uint32 i = 0; i < 4; ++i) {
            Temp.E[i][j] = E[j][i];
        }
    }

    return Temp;
}

} // namespace Math
} // namespace OnionEngine

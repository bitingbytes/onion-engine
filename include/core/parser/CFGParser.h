/**
 * @file CFGParser.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-11-06
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "FileSystem.h"
#include "OString.h"
#include "ScopePtr.h"
#include "onionengine_lib.h"

namespace OnionEngine {
struct OSectionData
{
    std::unordered_map<OString, int32> m_ints;
    std::unordered_map<OString, float> m_floats;
    std::unordered_map<OString, bool> m_bools;
    std::unordered_map<OString, OString> m_strings;

    inline int32 GetInt(const OString& _key) const { return Find(_key, 0); }
    inline float GetFloat(const OString& _key) const { return Find(_key, 0.0f); }
    inline float GetBool(const OString& _key) const { return Find(_key, false); }
    inline OString GetString(const OString& _key) const { return Find(_key, ""); }

  private:
    inline int32 Find(const OString& _key, int32) const
    {
        return m_ints.at(_key);
    }

    inline float Find(const OString& _key, float) const
    {
        return m_floats.at(_key);
    }

    inline bool Find(const OString& _key, bool) const
    {
        return m_bools.at(_key);
    }

    inline OString Find(const OString& _key, const char*) const
    {
        return m_strings.at(_key);
    }
};

class OCFG
{
  public:
    static OE_EXPORT OCFG* Parse(const char* _path);

    inline OSectionData Get(const OString& _key) const
    {
        return m_sections.at(_key);
    }

  private:
    std::unordered_map<OString, OSectionData> m_sections;

    static bool IsInteger(const OString& _line);
    static bool IsFloat(const OString& _line);
    static bool IsBool(const OString& _line);
    static bool IsString(const OString& _line);
};
} // OnionEngine

/**
 * @file WindowsWindow.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-22
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "IWindow.h"

namespace OnionEngine {
class WindowsApplication;
}

namespace OnionEngine {
class OE_EXPORT WindowsWindow : public IWindow
{
  public:
    ~WindowsWindow();

    static WindowsWindow* MakeWindow();

    void Initialize(class WindowsApplication* _ownerApplication,
                    const SWindowSettings&    _settings,
                    HINSTANCE                 _instance);

    // IWindow Implementation
    void  Show() override;
    void  Hide() override;
    void  Maximize() override;
    void  Minimize() override;
    bool  IsMaximized() const override;
    bool  IsMinimized() const override;
    void  Close() override;
    void  SetMode(EWindowMode _mode) override;
    void  Move(uint32 _x, uint32 _y) override;
    void* GetHandle() const override;

    inline HWND GetHWND() const { return m_handle; }

  private:
    WindowsWindow();

    HWND                m_handle;
    WindowsApplication* m_ownerApplication;
    EWindowMode         m_windowMode;
};

typedef WindowsWindow OWindow;
} // OnionEngine

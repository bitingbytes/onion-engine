/**
 * @file WindowsApplication.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-23
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "IApplication.h"
#include "WindowsWindow.h"

namespace OnionEngine {
class OE_EXPORT WindowsApplication : public IApplication
{
  public:
    WindowsApplication();

    ~WindowsApplication();

    IWindow* MakeWindow() override;

    virtual void Initialize() override;

    virtual void Shutdown() override;

    virtual void InitializeWindow(IWindow&               _window,
                                  const SWindowSettings& _settings) override;

    virtual void PumpMessage() override;

    IWindow* GetCurrentWindow() const override;

  protected:
    virtual void InitializeSubmodules() override;

    virtual void ShutdownSubmodules() override;

  private:
    HINSTANCE m_hInstance;

    WindowsWindow* m_window;
};

typedef WindowsApplication OApplication;
} // OnionEngine

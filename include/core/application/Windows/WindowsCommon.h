/**
 * @file WindowsCommon.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief Common methods to Windows OS.
 * @version 0.1
 * @date 2018-10-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#define OE_ASSERT(n)                                                           \
    if (!(n)) {                                                                \
        std::cout << "*************************" << std::endl;                 \
        std::cout << "    ASSERTION FAILED!    " << std::endl;                 \
        std::cout << "*************************" << std::endl;                 \
        std::cout << __FILE__ << ": " << __LINE__ << std::endl;                \
        std::cout << "Condition: " << #n << std::endl;                         \
        __debugbreak();                                                        \
    }

#include <Windows.h>

#ifdef TEXT
#undef TEXT
#endif

#if OE_UNICODE
#define TEXT(n) L##n
#else
#define TEXT(n) u8##n
#endif

#define OE_PLATFORM_NAME TEXT("Windows")

#define MAX_PATH_SIZE MAX_PATH
/**
 * @file LinuxCommon.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief Common methods to Linux OS.
 * @version 0.1
 * @date 2018-10-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#define OE_ASSERT(n)                                                           \
    if (!(n)) {                                                                \
        std::cout << "*************************" << std::endl;                 \
        std::cout << "    ASSERTION FAILED!    " << std::endl;                 \
        std::cout << "*************************" << std::endl;                 \
        std::cout << __FILE__ << ": " << __LINE__ << std::endl;                \
        std::cout << "Condition: " << #n << std::endl;                         \
    }

#if OE_UNICODE
#define TEXT(n) L##n
#else
#define TEXT(n) u8##n
#endif

#include <cstring>

#define MAX_PATH_SIZE PATH_MAX

#define OE_PLATFORM_NAME TEXT("MacOS")
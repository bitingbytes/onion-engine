/**
 * @file MacOSWindow.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-23
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "IWindow.h"
#include <SDL2/SDL.h>

namespace OnionEngine {
class OE_EXPORT MacOSWindow : public IWindow
{
  public:
    ~MacOSWindow();

    static MacOSWindow* MakeWindow();

    void Initialize(const SWindowSettings& _settings);

    void  Show() override;
    void  Hide() override;
    void  Maximize() override;
    void  Minimize() override;
    bool  IsMaximized() const override;
    bool  IsMinimized() const override;
    void  Close() override;
    void  SetMode(EWindowMode _mode) override;
    void  Move(uint32 _x, uint32 _y) override;
    void* GetHandle() const override;

    inline SDL_Window* GetSDLWindow() const { return m_handle; }

  private:
    MacOSWindow();

  private:
    SDL_Window* m_handle;
};

typedef MacOSWindow OWindow;
} // namespace OnionEngine
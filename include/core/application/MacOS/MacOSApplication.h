/**
 * @file MacOSApplication.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-28
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "IApplication.h"
#include "MacOSWindow.h"

namespace OnionEngine {
class OE_EXPORT MacOSApplication : public IApplication
{
  public:
    MacOSApplication();

    ~MacOSApplication();

    IWindow* MakeWindow() override;

    virtual void Initialize() override;

    virtual void Shutdown() override;

    virtual void InitializeWindow(IWindow&               _window,
                                  const SWindowSettings& _settings) override;

    virtual void PumpMessage() override;

    IWindow* GetCurrentWindow() const override;

  protected:
    virtual void InitializeSubmodules() override;

    virtual void ShutdownSubmodules() override;

  private:
    void SDLEventHandler();

    MacOSWindow* m_window;

};

typedef MacOSApplication OApplication;
} // OnionEngine

/**
 * @file PlatformDetection.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief Platform Detection layer.
 * @version 0.1
 * @date 2018-10-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#ifndef OE_PLATFORM_WINDOWS
#define OE_PLATFORM_WINDOWS 0
#endif
#ifndef OE_PLATFORM_LINUX
#define OE_PLATFORM_LINUX 0
#endif
#ifndef OE_PLATFORM_MACOS
#define OE_PLATFORM_MACOS 0
#endif
#ifndef OE_PLATFORM_PS4
#define OE_PLATFORM_PS4 0
#endif
#ifndef OE_PLATFORM_XBOXONE
#define OE_PLATFORM_XBOXONE 0
#endif
#ifndef OE_PLATFORM_SWITCH
#define OE_PLATFORM_SWITCH 0
#endif
#ifndef OE_PLATFORM_EMBEDDED
#define OE_PLATFORM_EMBEDDED 0
#endif
#ifndef OE_PLATFORM_ANDROID
#define OE_PLATFORM_ANDROID 0
#endif
#ifndef OE_PLATFORM_IOS
#define OE_PLATFORM_IOS 0
#endif

// Platform Commons
#if OE_PLATFORM_WINDOWS || _WIN32 || __MINGW32__
#include "WindowsCommon.h"
#elif OE_PLATFORM_LINUX || __linux__
#include "LinuxCommon.h"
#elif OE_PLATFORM_MACOS
#include "MacOSCommon.h"
#elif OE_PLATFORM_PS4
#include "Ps4Common.h"
#elif OE_PLATFORM_XBOXONE
#include "XboxCommon.h"
#elif OE_PLATFORM_SWITCH
#include "SwitchCommon.h"
#elif OE_PLATFORM_EMBEDDED
#include "EmbeddedCommon.h"
#elif OE_PLATFORM_ANDROID
#include "AndroidCommon.h"
#elif OE_PLATFORM_IOS
#include "IOSCommon.h"
#else
#error Invalid Compiler
#endif

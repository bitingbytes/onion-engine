/**
 * @file Types.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief Base types.
 * @version 0.1
 * @date 2018-10-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "PlatformDetection.h"
#include "SystemLibrary.h"

namespace OnionEngine {
typedef unsigned char      uint8;  // Unsigned integer with 8 bits.
typedef unsigned short int uint16; // Unsigned integer with 16 bits.
typedef unsigned int       uint32; // Unsigned integer with 32 bits.
typedef unsigned long long uint64; // Unsigned integer with 64 bits.

typedef char      int8;  // Signed integer with 8 bits.
typedef short int int16; // Signed integer with 16 bits.
typedef int       int32; // Signed integer with 32 bits.
typedef long long int64; // Signed integer with 64 bits.

typedef wchar_t wchar;

#if OE_UNICODE
typedef wchar tchar;
#else
typedef char tchar;
#endif
}

#define BIT(x) 1 << x
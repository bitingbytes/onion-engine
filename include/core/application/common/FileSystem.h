/**
 * @file OFileSystem.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief File System.
 * @version 0.1
 * @date 2018-10-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "Serializer.h"
#include "onionengine_lib.h"

namespace OnionEngine {
/**
 * @brief Subsystem responsable for file operations.
 *
 */
class OE_EXPORT OFileSystem
{
  public:
    /**
     * @brief Get the directory of the executable program.
     *
     * @param path The path to the executable directory.
     */
    static void GetExecutableDirectory(tchar* path);

    /**
     * @brief Get the Current Work Path.
     *
     * @param _path The current path.
     */
    inline static void GetCurrentPath(tchar* _path)
    {
        InternalGetCurrentPath(_path);
    }

    /**
     * @brief Create a Directory at a _path.
     *
     * @param _path The path of the new directory.
     * @return true If the directory was created sucessfully.
     * @return false If an error occurred.
     */
    static bool CreateDirectory(const tchar* _path, bool _recursively = false);

    /**
     * @brief Change the working directory.
     *
     * Similar to Linux cd.
     *
     * @param _path The path of the new work directory.
     * @return true If the directory was changed sucessfully.
     * @return false If an error occurred.
     */
    static bool ChangeDirectory(const tchar* _path);

    static bool IsDirectory(const tchar* _path);

    static OString AddExtension(const tchar* _path, const tchar* _extension);

    static OString PathAppend(const tchar* _pathA, const tchar* _pathB);

    static OString GetFileName(const tchar* _path);

    static bool FileExists(const tchar* _path);

    static std::vector<OString> GetFileList(const tchar* _path);

  private:
    static void InternalGetCurrentPath(tchar* _path);

    static void FixPath(tchar* _path);
};

/**
 * @brief Represents a File object.
 *
 */
class OE_EXPORT OFile : public Serializer
{
  public:
    /**
     * @brief Construct a new File object.
     *
     * @param _path The path of the file.
     * @param _mode The openning mode. (r - reading, w - writting, w+ - read and
     * write).
     */
    OFile(const tchar* _path, const tchar* _mode);

    /**
     * @brief Destroy the File object
     *
     */
    virtual ~OFile();

    /**
     * @brief Verify if the file is valid.
     *
     * @return true If the file is valid.
     * @return false If the file is invalid.
     */
    inline bool IsValid() const { return m_Handler != nullptr; }

    /**
     * @brief Get the Size of the File object in bytes.
     *
     * @return size_t Size in bytes.
     */
    inline size_t GetSize() const { return m_Size; }

    inline OString GetPath() const { return OString(m_Path); }

    inline size_t GetCreationTime() const { return m_CreationTime; }

    inline size_t GetLastAccessTime() const { return m_LastAccessTime; }

    inline size_t GetLastWriteTime() const { return m_LastWriteTime; }

    inline void SetMode(const char* _mode) { m_Mode = _mode; }

    unsigned GetCheckSum();

    /**
     * @brief Opens the file.
     *
     * @return true If the File was openned successfully.
     * @return false If the File was not openned successfully.
     */
    bool Open();

    size_t Read(void* _buffer, size_t _bytes);

    size_t Write(void* _buffer, size_t _bytes);

    OString GetLine();

    bool Flush();

    bool Close();

    bool Seek(size_t _offset, int32 _mode);

    size_t Tell();

    inline bool IsEoF() const { return m_Offset >= m_Size; }

    inline bool IsDirty() const;

    inline FILE* GetHandler() { return m_Handler; }

    inline bool IsOpened() const { return m_Opened; }

    OString GetExtension() const;

  private:
    bool InternalOpen();

    size_t InternalRead(void* _buffer, size_t _bytes);

    size_t InternalWrite(void* _buffer, size_t _bytes);

    bool InternalFlush();

    bool InternalClose();

    bool InternalSeek(size_t _offset, int32 _mode);

    size_t InternalTell() const;

  private:
    const tchar* m_Path;
    const tchar* m_Mode;
    size_t       m_Size;
    size_t       m_Offset;
    size_t       m_CreationTime;
    size_t       m_LastAccessTime;
    size_t       m_LastWriteTime;
    uint8        m_Opened : 1;

    FILE* m_Handler;

    void InternalGetStat();
};
} // namespace OnionEngine

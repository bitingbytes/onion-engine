/**
 * @file IApplication.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-23
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "ConfigSubsystem.h"
#include "IWindow.h"
#include "InputSubsystem.h"
#include "LogSubsystem.h"
#include "MemorySubsystem.h"
#include "onionengine.h"
#include "onionengine_lib.h"

namespace OnionEngine {
class OE_EXPORT IApplication
{
    static IApplication* s_Instance;

  public:
    virtual ~IApplication();

    virtual IWindow* MakeWindow() { return new IWindow(); }

    virtual void Initialize();

    virtual void Shutdown();

    virtual void InitializeWindow(IWindow&               _window,
                                  const SWindowSettings& _settings);

    virtual void PumpMessage();

    virtual IWindow* GetCurrentWindow() const;

    inline static IApplication& Get() { return *s_Instance; }

  protected:
    IApplication();

    virtual void InitializeSubmodules();

    virtual void ShutdownSubmodules();

    SWindowSettings GetWindowFromConfig();
};
} // OnionEngine

/**
 * @file Time.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2018-12-17
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "Types.h"
#include "onionengine_lib.h"

namespace OnionEngine {
struct Members;

class OE_EXPORT Time
{
    static Time* s_Instance;

  private:
    uint8    m_reserved[32];
    Members* m_members;
    float    m_snap0;
    float    m_snap1;

  public:
    friend class OApplication;

    Time();

    static void Initialize();

    static void Shutdown();

    static void Reset();

    static float Elapsed();

    static float ElapsedMillis();
};

struct Timestep
{
  private:
    float m_Timestep;
    float m_LastTime;

  public:
    inline Timestep(float initialTime)
      : m_Timestep(0.0f)
      , m_LastTime(initialTime)
    {}

    inline void Update(float currentTime)
    {
        m_Timestep = currentTime - m_LastTime;
        m_LastTime = currentTime;
    }

    inline float GetMillis() const { return m_Timestep; }
    inline float GetSeconds() const { return m_Timestep * 0.001f; }
};
} // OnionEngine

#include "PlatformDetection.h"

#if OE_PLATFORM_WINDOWS
#include "WindowsApplication.h"
#elif OE_PLATFORM_LINUX
#include "LinuxApplication.h"
#elif OE_PLATFORM_MACOS
#include "MacOSApplication.h"
#else
#error "Unsupported Platform"
#endif
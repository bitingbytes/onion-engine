/**
 * @file OWindow.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief Representation of a OWindow.
 * @version 0.1
 * @date 2018-10-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include "LogSubsystem.h"
#include "onionengine_lib.h"

namespace OnionEngine {
/**
 * @enum EWindowMode
 *
 * @brief OWindow exhibition modes.
 *
 * Define what is the OWindow exhibition mode.
 */
enum class EWindowMode : uint8
{
    /** Exhibits OWindow on Windowned mode. */
    WM_WINDOWNED = 0U,

    /** Exhibits OWindow on Fullscreen Borderless mode, this is not true
       fullscreen. */
    WM_FULLSCREEN_BORDERLESS,

    /** Exhibits OWindow on true Fullscreen mode. */
    WM_FULLSCREEN
};

/**
 * @brief OWindow settings.
 *
 * OWindow's general settings.
 *
 */
struct SWindowSettings
{
    /** The OWindow title. */
    OString Title;

    /** The OWindow width in pixels. */
    uint32 Width = 640;

    /** The OWindow height in pixels. */
    uint32 Height = 320;

    /** The OWindow position component X in pixels. */
    uint32 PositionX = 0;

    /** The OWindow position component Y in pixels. */
    uint32 PositionY = 0;

    /** The OWindow exhibition mode. */
    EWindowMode Mode = EWindowMode::WM_WINDOWNED;
};

/**
 * @brief Class that represents a OWindow. Which is a representation of your view
 * area on the screen.
 *
 * Representation of a view area on the screen where your application is going
 * to render its images and UIs.
 *
 */
class OE_EXPORT IWindow
{
  public:
    /**
     * @brief Construct a new OWindow but do not show it.
     *
     * @note Prepare a OWindow instance but do not create a OWindow on the
     * operational system. After instanteate a OWindow, use the Create() method.
     *
     * @param _title OWindow's title.
     * @param _settings OWindow's settings
     */
    IWindow();

    /**
     * @brief Destroy the OWindow instance.
     *
     * Closes and destroy the OWindow instance.
     */
    virtual ~IWindow();

    /**
     * @brief Show the window on the screen.
     *
     * @return true When the window is shown correctly.
     * @return false When an error occurred on the showing process.
     */
    virtual void Show();

    /**
     * @brief Hide the window from the screen.
     *
     * @return true When the window is hidden correctly.
     * @return false When an error occurred on the hidding process.
     */
    virtual void Hide();

    /**
     * @brief Maximizes a window.
     *
     * @return true When the window is maximized correctly.
     * @return false When an error occurred on the maximizing process.
     */
    virtual void Maximize();

    /**
     * @brief Minimizes a window.
     *
     * @return true When the window is minimized correctly.
     * @return false When an error occurred on the minimizing process.
     */
    virtual void Minimize();

    virtual bool IsMaximized() const;

    virtual bool IsMinimized() const;

    /**
     * @brief Closes a window.
     *
     * @note This method only closes the window, it does not destroy it.
     *
     * @return true When the window is closed correctly.
     * @return false When the window is not closed correctly.
     */
    virtual void Close();

    /**
     * @brief Set the Mode of the OWindow
     *
     * @param _mode The OWindow's mode. @see EWindowMode.
     * @return true When the mode is setted correctly.
     * @return false Whe the mode is not setted correctly.
     */
    virtual void SetMode(EWindowMode _mode);

    virtual void Move(uint32 _x, uint32 _y);

  public:
    /**
     * @brief Verifies if the OWindow is valid.
     *
     * @return true When the OWindow is valid.
     * @return false When the OWindow is invalid.
     */

    virtual bool IsValid() const { return GetHandle() != nullptr; }

    /**
     * @brief Get the OWindow's settings.
     *
     * @see SWindowSettings
     *
     * @return The OWindow's settings.
     */
    inline SWindowSettings& GetSettings() { return m_settings; }

    /**
     * @brief Get the pointer that holds the OWindow's handle.
     *
     * @return Pointer to OWindow's handle.
     */
    virtual void* GetHandle() const;

  protected:
    /** The general settings of a OWindow instance. */
    SWindowSettings m_settings;
};
} // namespace OnionEngine
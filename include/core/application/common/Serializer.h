/**
 * @file Serializer.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief Responsable for serializing.
 * @version 0.1
 * @date 2018-10-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */
#pragma once

#include "Types.h"
#include "OString.h"
#include "onionengine_lib.h"

namespace OnionEngine
{
struct OE_EXPORT Serializer
{
public:
  virtual size_t Write(void *_buffer, size_t _bytes) = 0;

  virtual size_t Read(void *_buffer, size_t _bytes) = 0;

  Serializer &operator<<(uint32 _value);
  Serializer &operator>>(uint32 &_value);

  Serializer &operator<<(int32 _value);
  Serializer &operator>>(int32 &_value);

  Serializer &operator<<(uint8 *_value);

  Serializer &operator<<(float _value);
  Serializer &operator>>(float &_value);

  Serializer &operator<<(double _value);
  Serializer &operator>>(double &_value);

  /*   Serializer& operator<<(const OString& _value);
      Serializer& operator>>(OString& _value); */
};
} // namespace OnionEngine
/**
 * @file SystemLibrary.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief Common Libraries
 * @version 0.1
 * @date 2018-10-09
 *
 * @copyright Copyright (c) 2015-2018 Eremita Studio, Inc. All Rights Reserved.
 *
 */

#pragma once

#include <algorithm>
#include <atomic>
#include <chrono>
#include <cmath>
#include <cstdarg>
#include <cstdio>
#include <functional>
#include <iomanip>
#include <iostream>
#include <locale>
#include <map>
#include <memory>
#include <mutex>
#include <queue>
#include <set>
#include <sstream>
#include <stack>
#include <thread>
#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <vector>
#include "MemoryManager.h"
#include "ConfigSubsystem.h"

namespace OnionEngine {
MemoryManager* MemoryManager::s_Instance = nullptr;

MemoryManager::MemoryManager()
  : m_bigStack(nullptr)
{}

MemoryManager::~MemoryManager()
{
    m_bigStack->Clear();
}

void
MemoryManager::Initialize()
{
    if (s_Instance == nullptr) {
        s_Instance         = new MemoryManager();
        SectionData config = ConfigSubsystem::GetSection("Memory");
        s_Instance->m_bigStack =
          new StackAllocator(config.GetInt("iMaxStackSize") * 1024 * 1024);
    }
}

void
MemoryManager::Shutdown()
{
    if (s_Instance) {
        delete s_Instance;
    }
}
} // OnionEngine

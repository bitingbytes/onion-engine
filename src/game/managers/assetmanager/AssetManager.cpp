#include "AssetManager.h"
#include "LogSubsystem.h"

namespace OnionEngine {
AssetManager* AssetManager::s_Instance = nullptr;

AssetManager::AssetManager()
{
    /* Does nothing */
}

AssetManager::~AssetManager()
{
    /* Does nothing */
}

void
AssetManager::Initialize()
{
    if (s_Instance == nullptr) {
        s_Instance                 = new AssetManager();
        s_Instance->m_loadedAssets = { { "", nullptr } };
    }
}

void
AssetManager::Shutdown()
{
    if (s_Instance) {
        delete s_Instance;
        s_Instance = nullptr;
    }
}

String
AssetManager::LoadAssetInternal(const char* _path)
{
    String path(_path);
    String metaPath(path + ".omf");

    if (!FileSystem::FileExists(metaPath.c_str())) {
        OE_ERROR(
          "File %s does not have a meta file, verify if it was imported.",
          _path);

        return nullptr;
    }

    YAML::Node meta = YAML::LoadFile(metaPath.c_str());

    if (!meta["buid"]) {
        OE_ERROR("Invalid meta file: %s", metaPath.c_str());
        return nullptr;
    }

    String buid     = meta["buid"].as<String>();
    String buidPath = "./bin/" + buid.substr(0, 2) + "/" + buid + ".obf";

    if (!FileSystem::FileExists(buidPath.c_str())) {
        OE_ERROR("File %s binaries does not exists, verify if it was imported.",
                 _path);

        return nullptr;
    }

    return buidPath;
}

} // OnionEngine

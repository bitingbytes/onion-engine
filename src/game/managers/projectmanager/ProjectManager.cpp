#include "ProjectManager.h"
#include "LogSubsystem.h"

namespace OnionEngine {
ProjectManager* ProjectManager::s_Instance = nullptr;

ProjectManager::ProjectManager() {}

ProjectManager::~ProjectManager() {}

void
ProjectManager::Initialize()
{
    if (s_Instance == nullptr) {
        s_Instance            = new ProjectManager();
        s_Instance->m_project = YAML::LoadFile("project.yaml");
        if (s_Instance->m_project.IsNull())
            OE_ERROR("Could not find the project.yaml file.");
    }
}

void
ProjectManager::Shutdown()
{
    if (s_Instance)
        delete s_Instance;
}

std::vector<String>
ProjectManager::GetMapsPath()
{
    return s_Instance->m_project["maps"].as<std::vector<String>>();
}
} // OnionEngine

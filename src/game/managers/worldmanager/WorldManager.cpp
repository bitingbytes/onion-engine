#include "WorldManager.h"
#include "LogSubsystem.h"

namespace OnionEngine {
WorldManager* WorldManager::s_Instance = nullptr;

WorldManager::WorldManager() {}

WorldManager::~WorldManager() {}

void
WorldManager::Initialize()
{
    if (s_Instance == nullptr) {
        s_Instance = new WorldManager();
    }
}

void
WorldManager::Shutdown()
{
    if (s_Instance)
        delete s_Instance;
}

WorldMap*
WorldManager::GetCurrentMap()
{
    return s_Instance->m_currentMap;
}

void
WorldManager::CreateMap(const String& _path)
{
}

void
WorldManager::LoadMap(uint32 _index)
{
    std::vector<String> paths = ProjectManager::GetMapsPath();

    if (_index + 1 > paths.size()) {
        OE_ERROR(
          "Could not access map with index %d, your project have %d maps.",
          _index,
          paths.size());

        return;
    }

    if (!LoadMapFromFile(paths[_index])) {
        OE_ERROR("Could not load map %s", paths[_index].c_str());
        return;
    }
}

void
WorldManager::LoadMap(const String& _name)
{}

void
WorldManager::UnloadCurrentMap()
{}

bool
WorldManager::LoadMapFromFile(String _path)
{
    return false;
}
} // OnionEngine
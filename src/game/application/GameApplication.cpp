#include "GameApplication.h"

namespace OnionEngine {
GameApplication::GameApplication() {}

GameApplication::~GameApplication() {}

void
GameApplication::Initialize()
{
    Application::Initialize();
}

void
GameApplication::Shutdown()
{
    Application::Shutdown();
}

void
GameApplication::InitializeSubmodules()
{
    Application::InitializeSubmodules();
    MemoryManager::Initialize();
    ProjectManager::Initialize();
    AssetManager::Initialize();
    WorldManager::Initialize();
}

void
GameApplication::ShutdownSubmodules()
{
    WorldManager::Shutdown();
    AssetManager::Shutdown();
    ProjectManager::Shutdown();
    MemoryManager::Shutdown();
    Application::ShutdownSubmodules();
}
} // OnionEngine
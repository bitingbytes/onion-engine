#include "ComponentManager.h"

namespace OnionEngine {
ComponentManager::ComponentManager()
{
    m_poolAllocator = MemoryManager::Allocate(4096 * sizeof(Entity));
}

ComponentManager::~ComponentManager()
{
    m_poolAllocator->Clear();
}

unsigned
ComponentManager::RequestComponentID() const
{
    ++m_next.m_id;
    while (Exists(m_next))
        ++m_next.m_id;

    return m_next.m_id;
}

bool
ComponentManager::Exists(const Component& _component) const
{
    return m_components.find(_component) != m_components.end();
}
} // OnionEngine

#include "CFGParser.h"

namespace OnionEngine {
OCFG*
OCFG::Parse(const char* _path)
{
    TScopePtr<OFile> file = new OFile(_path, "r");

    if (!file->Open())
        return nullptr;

    OCFG* result = nullptr;

    OString sectionKey;

    while (!file->IsEoF()) {
        OString line = file->GetLine();
        OString firstToken = GetFirstToken(line);

        if (firstToken[0] == '[') {
            OString sectionName = line.substr(line.find_first_not_of('['),
                                             line.find_first_of(']') - 1);

            if (!result)
                result = new OCFG();

            sectionKey = std::move(sectionName);

            result->m_sections[sectionKey];
            continue;
        } else if (IsInteger(firstToken)) {
            std::vector<OString> splitted;
            Split(line, splitted, "=");

            OString key = splitted[0].c_str();
            int32 value = std::stoi(splitted[1]);

            result->m_sections[sectionKey].m_ints[key] = value;

        } else if (IsFloat(firstToken)) {
            std::vector<OString> splitted;
            Split(line, splitted, "=");

            OString key = splitted[0].c_str();
            float value = std::stof(splitted[1]);

            result->m_sections[sectionKey].m_floats[key] = value;
        } else if (IsBool(firstToken)) {
            std::vector<OString> splitted;
            Split(line, splitted, "=");

            OString key = splitted[0].c_str();
            bool value = splitted[1] == "true";

            result->m_sections[sectionKey].m_bools[key] = value;
        } else if (IsString(firstToken)) {
            std::vector<OString> splitted;
            Split(line, splitted, "=");

            OString key = splitted[0].c_str();
            OString value = splitted[1];

            size_t f = value.find_first_not_of('\"');

            value = value.substr(value.find_first_not_of('\"'),
                                 value.find_first_of('\"', f) - 1);

            result->m_sections[sectionKey].m_strings[key] = value;
        }
    }

    return result;
}

bool
OCFG::IsInteger(const OString& _line)
{
    return GetFirstToken(_line)[0] == 'i';
}

bool
OCFG::IsFloat(const OString& _line)
{
    return GetFirstToken(_line)[0] == 'f';
}

bool
OCFG::IsBool(const OString& _line)
{
    return GetFirstToken(_line)[0] == 'b';
}

bool
OCFG::IsString(const OString& _line)
{
    return GetFirstToken(_line)[0] == 's';
}
} // OnionEngine

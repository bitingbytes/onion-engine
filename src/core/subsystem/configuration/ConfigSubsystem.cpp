#include "ConfigSubsystem.h"
#include "LogSubsystem.h"

namespace OnionEngine {
OConfigSubsystem* OConfigSubsystem::s_Instance = nullptr;

OConfigSubsystem::OConfigSubsystem() {}

OConfigSubsystem::~OConfigSubsystem() {}

void
OConfigSubsystem::Initialize()
{
    if (s_Instance == nullptr) {
        s_Instance = (OConfigSubsystem*)malloc(sizeof(OConfigSubsystem));
        s_Instance->m_data = OCFG::Parse("./config/preferences.cfg");
        OE_INFO("Configuration Subsystem Initialized.");
    }
}

void
OConfigSubsystem::Shutdown()
{
    if (s_Instance != nullptr) {
        delete s_Instance;
        OE_INFO("Configuration Subsystem Shutted Down.");
    }
}

OSectionData
OConfigSubsystem::GetSection(const OString& _key)
{
    return s_Instance->m_data->Get(_key);
}
} // OnionEngine
#include "LogSubsystem.h"
#include <iomanip>
#include <sstream>

namespace OnionEngine {
OLogSubsystem* OLogSubsystem::s_Instance = nullptr;

OLogSubsystem::OLogSubsystem()
{
    /* Does nothing */
}

OLogSubsystem::~OLogSubsystem()
{
    /* Does nothing */
}

void
OLogSubsystem::Initialize(ELogLevel _level)
{
    if (s_Instance == nullptr) {
        s_Instance               = (OLogSubsystem*)malloc(sizeof(OLogSubsystem));
        s_Instance->m_logFile    = nullptr;
        s_Instance->mb_logToFile = false;
        s_Instance->m_logLevel = _level;

        time_t     t   = time(0);
        struct tm* now = localtime(&t);

        uint8 counter = 0;

        char buffer[32];
        strftime(buffer, 32, "./logs/%Y%m%d_oe-log_", now);
        OString filename;

        do {
            filename.clear();
            filename += buffer + std::to_string(counter) + std::string(".log");
        } while (OFileSystem::FileExists(filename.c_str()) && counter++ < 255);

        const char* path = filename.c_str();

        s_Instance->m_logFile    = new OFile(path, "w");
        s_Instance->mb_logToFile = s_Instance->m_logFile->Open();
    }
}

void
OLogSubsystem::Shutdown()
{
    if (s_Instance != nullptr) {
        delete s_Instance->m_logFile;
        free(s_Instance);
    }
}

inline const char*
OLogSubsystem::LogLevelToString(ELogLevel _LogLevel)
{
    switch (_LogLevel) {
        case ELogLevel::Fatal:
            return "FATAL";
        case ELogLevel::Error:
            return "ERROR";
        case ELogLevel::Warning:
            return "WARN ";
        case ELogLevel::Info:
            return "INFO ";
        case ELogLevel::Debug:
            return "DEBUG";
    }
    return "";
}

void
OLogSubsystem::LogInternal(const char* _Category,
                          ELogLevel   _LogLevel,
                          const char* _Format,
                          ...)
{
    if (m_logLevel <= _LogLevel) {
        //        PlatformLogMessage(_LogLevel);

        time_t     t   = time(0);
        struct tm* now = localtime(&t);

        char logtime[10];
        strftime(logtime, 10, "%OH:%OM:%OS", now);
        if (mb_logToFile) {
            va_list ap;
            fprintf(m_logFile->GetHandler(),
                    "[%s] [%s] [%s] ",
                    logtime,
                    _Category,
                    LogLevelToString(_LogLevel));
            va_start(ap, _Format);
            vfprintf(m_logFile->GetHandler(), _Format, ap);
            va_end(ap);
            fprintf(m_logFile->GetHandler(), "%c", '\n');

            m_logFile->Flush();
        }
        {
            va_list ap;
            printf("[%s] [%s] [%s] ",
                   logtime,
                   _Category,
                   LogLevelToString(_LogLevel));
            va_start(ap, _Format);
            vprintf(_Format, ap);
            va_end(ap);
            printf("%c", '\n');
        }

        //        PlatformClearLogMessage();
    }
}
} // OnionEngine

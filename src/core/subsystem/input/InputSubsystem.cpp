#include "InputSubsystem.h"
#include "LogSubsystem.h"

namespace OnionEngine {
OInputSubsystem* OInputSubsystem::s_Instance = nullptr;

OInputSubsystem::OInputSubsystem()
{
    /* Does nothing */
}

OInputSubsystem::~OInputSubsystem()
{
    /* Does nothins */
}

void
OInputSubsystem::Initialize()
{
    if (s_Instance == nullptr) {
        s_Instance = new OInputSubsystem();
        s_Instance->m_button_x = false;
        OE_INFO("Input Subsystem Initialized.");
    }
}

void
OInputSubsystem::Shutdown()
{
    if (s_Instance) {
        delete s_Instance;
        s_Instance = nullptr;
        OE_INFO("Input Subsystem Shutted Down.");
    }
}

void
OInputSubsystem::SetJoystick(uint8 _index, const OJoystick& _joystick)
{
    OE_ASSERT(_index < OE_MAX_LOCAL_PLAYERS);

    m_joysticks[_index] = _joystick;
}

} // OnionEngine

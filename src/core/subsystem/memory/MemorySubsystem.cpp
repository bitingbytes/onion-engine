#include "MemorySubsystem.h"
#include "LogSubsystem.h"

namespace OnionEngine {
OMemorySubsystem* OMemorySubsystem::s_Instance = nullptr;

OMemorySubsystem::OMemorySubsystem()
{
    /* Does nothing */
}

OMemorySubsystem::~OMemorySubsystem()
{
    /* Does nothing */
}

void
OMemorySubsystem::Initialize()
{
    if (s_Instance == nullptr) {
        s_Instance = (OMemorySubsystem*)malloc(sizeof(OMemorySubsystem));
        OE_INFO("Memory Subsystem Initialized.");
    }
}

void
OMemorySubsystem::Shutdown()
{
    if (s_Instance != nullptr) {
        free(s_Instance);
        OE_INFO("Memory Subsystem Shutted Down.");
    }
}

void*
OMemorySubsystem::Allocate(size_t _size)
{
    OE_ASSERT(_size > 0)

#ifdef OE_AUTHORED_MEMORY_ALLOCATION
    return AllocateAligned(_size);
#else
    return malloc(_size);
#endif
}

void
OMemorySubsystem::Free(void* _block)
{
#ifdef OE_AUTHORED_MEMORY_ALLOCATION
    if (_block) {
        uint8* AlignedMem = reinterpret_cast<uint8*>(_block);

        std::ptrdiff_t Shift = AlignedMem[-1];
        if (Shift == 0)
            Shift = 256;

        uint8* RawMem = AlignedMem - Shift;
        free(RawMem);
    }
#else
    free(_block);
#endif
}
} // OnionEngine

#include "StackAllocator.h"
#include "MemorySubsystem.h"

namespace OnionEngine {
OStackAllocator::OStackAllocator(size_t _size)
  : m_marker(0)
  , m_top(_size)
  , m_bottom(reinterpret_cast<uint8*>(OMemorySubsystem::Get().Allocate(_size)))
{}

uint8*
OStackAllocator::InternalAllocate(size_t _size)
{
    if (m_marker + _size > m_top) { // Use assertion
        // Stack Overflow
        return nullptr;
    }

    m_bottom += m_marker;
    m_marker += _size;

    return m_bottom;
}

void
OStackAllocator::Deallocate(size_t _size)
{
        m_marker -= _size;
}

void
OStackAllocator::Clear()
{
    m_marker = 0;
}
} // OnionEngine

#include "CRC32.h"
#include <string.h>

namespace OnionEngine {
uint32
OCRC32::Get(unsigned char* p, size_t n)
{
    unsigned long crc = CRC_32_R;
    size_t i;
    for (i = 0; i < n; i++)
        crc = CRC_Table[*p++ ^ (crc & 0xff)] ^ (crc >> 8);
    return (~crc);
}

uint32
OCRC32::Get(const char* _value)
{
    return Get((unsigned char*)_value, strlen(_value));
}
}

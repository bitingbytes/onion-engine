#include "Application.h"
#include "SDL2/SDL.h"

namespace OnionEngine {
MacOSApplication::MacOSApplication()
  : m_window(nullptr)
{}

MacOSApplication::~MacOSApplication() {}

IWindow*
MacOSApplication::MakeWindow()
{
    return MacOSWindow::MakeWindow();
}

void
MacOSApplication::Initialize()
{
    InitializeSubmodules();

    m_window = (MacOSWindow*)MakeWindow();
    InitializeWindow(*m_window, GetWindowFromConfig());
}

void
MacOSApplication::Shutdown()
{
    m_window->Close();
    ShutdownSubmodules();
}

void
MacOSApplication::InitializeWindow(IWindow&               _window,
                                   const SWindowSettings& _settings)
{
    MacOSWindow& Window = (MacOSWindow&)_window;

    Window.Initialize(_settings);
}

void
MacOSApplication::PumpMessage()
{
    SDLEventHandler();
}

IWindow*
MacOSApplication::GetCurrentWindow() const
{
    return m_window;
}

void
MacOSApplication::InitializeSubmodules()
{
    LogSubsystem::Initialize();
    MemorySubsystem::Initialize();
    ConfigSubsystem::Initialize();
    InputSubsystem::Initialize();
}

void
MacOSApplication::ShutdownSubmodules()
{
    InputSubsystem::Shutdown();
    ConfigSubsystem::Shutdown();
    MemorySubsystem::Shutdown();
    LogSubsystem::Shutdown();
}

void
MacOSApplication::SDLEventHandler()
{}
} // OnionEngine

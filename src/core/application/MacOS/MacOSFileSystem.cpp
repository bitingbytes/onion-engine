#include "FileSystem.h"
#include "LogSubsystem.h"
#include <libgen.h>
#include <sys/stat.h>
#include <unistd.h>

namespace OnionEngine {
bool
File::InternalOpen()
{
    if (!m_Opened) {
        m_Handler = fopen(m_Path, m_Mode);

        if (!IsValid()) {
            OE_ERROR("Could not open file: %s", m_Path);
            return false;
        }

        m_Opened = true;
        OE_VERB("The File %s was opened.", m_Path);
    } else {
        OE_WARN("The File %s is already opened.", m_Path);
    }

    return true;
}

size_t
File::InternalRead(void* _buffer, size_t _bytes)
{
    if (!IsValid()) {
        OE_ERROR("Could not read file %s. Be sure to use the Open() function "
                 "before trying to read the file.",
                 m_Path);
    }

    return fread(_buffer, sizeof(uint8), _bytes, m_Handler);
}

size_t
File::InternalWrite(void* _buffer, size_t _bytes)
{
    if (!IsValid()) {
        OE_ERROR("Could not read file %s. Be sure to use the Open() function "
                 "before trying to read the file.",
                 m_Path);
    }

    size_t wroten = fwrite(_buffer, _bytes, sizeof(uint8), m_Handler);

    return wroten;
}

bool
File::InternalFlush()
{
    if (!IsValid()) {
        OE_ERROR("Could not read file %s. Be sure to use the Open() function "
                 "before trying to read the file.",
                 m_Path);
        return false;
    }

    return fflush(m_Handler) == 0;
}

bool
File::InternalClose()
{
    m_Opened = false;
    if (IsValid())
        return fclose(m_Handler) == 0;
    return true;
}

bool
File::InternalSeek(size_t _offset, int32 _mode)
{
    return fseek(m_Handler, _offset, _mode);
}

size_t
File::InternalTell() const
{
    return ftell(m_Handler);
}

void
File::InternalGetStat()
{
    struct stat FileStat;

    stat(m_Path, &FileStat);
    m_Size += (size_t)FileStat.st_size;
    m_CreationTime   = (size_t)FileStat.st_ctime;
    m_LastAccessTime = (size_t)FileStat.st_atime;
    m_LastWriteTime  = (size_t)FileStat.st_mtime;
}

inline bool
File::IsDirty() const
{
    struct stat FileStat;

    stat(m_Path, &FileStat);

    return m_LastWriteTime != (size_t)FileStat.st_mtime;
}

void
FileSystem::InternalGetCurrentPath(tchar* _path)
{
    tchar temp[PATH_MAX];
    getcwd(temp, PATH_MAX);
    _path = temp;
}

void
FileSystem::FixPath(tchar* _path)
{}

bool
FileSystem::CreateDirectory(const tchar* _path, bool _recursively)
{
    if (mkdir(_path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == -1) {
        OE_ERROR("Could not create directory %s.", _path);
        return false;
    }

    return true;
}

bool
FileSystem::ChangeDirectory(const tchar* _path)
{
    if (chdir(_path) == -1) {
        OE_ERROR("Could not change to directory %s.", _path);
        return false;
    }

    return true;
}

bool
FileSystem::IsDirectory(const tchar* _path)
{
    struct stat st;
    stat(_path, &st);

    return S_ISDIR(st.st_mode);
}

String
FileSystem::GetFileName(const tchar* _path)
{
    tchar* path = (tchar*)_path;
    String temp(basename(path));
    return temp.substr(0, temp.rfind('.'));
}

bool
FileSystem::FileExists(const tchar* _path)
{
    struct stat buffer;
    return (stat(_path, &buffer) == 0);
}

std::vector<String>
FileSystem::GetFileList(const tchar* _path)
{
    std::vector<String> output;

    return output;
}
}
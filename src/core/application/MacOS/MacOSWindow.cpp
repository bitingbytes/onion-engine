#include "MacOSWindow.h"
#include <SDL2/SDL.h>

namespace OnionEngine {
MacOSWindow::MacOSWindow()
  : m_handle(nullptr)
{}

MacOSWindow::~MacOSWindow() {}

MacOSWindow*
MacOSWindow::MakeWindow()
{
    return new MacOSWindow();
}

void
MacOSWindow::Initialize(const SWindowSettings& _settings)
{
    m_settings = _settings;

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC);

    uint32 flags = SDL_WINDOW_OPENGL;

    if (m_settings.Mode == EWindowMode::WM_FULLSCREEN)
        flags |= SDL_WINDOW_FULLSCREEN;
    else if (m_settings.Mode == EWindowMode::WM_FULLSCREEN_BORDERLESS)
        flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;

    m_handle = SDL_CreateWindow(m_settings.Title.c_str(),
                                m_settings.PositionX,
                                m_settings.PositionY,
                                m_settings.Width,
                                m_settings.Height,
                                flags);

    if (m_handle == nullptr) {
        OE_ERROR(TEXT("Could not create window: %s"), SDL_GetError());
        return;
    }

    OE_VERB(TEXT("Window %s created with success."), m_settings.Title.c_str());

    //    SetupController();
}

void
MacOSWindow::Show()
{
    SDL_ShowWindow(m_handle);
}

void
MacOSWindow::Hide()
{
    SDL_HideWindow(m_handle);
}

void
MacOSWindow::Maximize()
{
    SDL_MaximizeWindow(m_handle);
}

void
MacOSWindow::Minimize()
{
    SDL_MinimizeWindow(m_handle);
}

bool
MacOSWindow::IsMaximized() const
{
    return true;
}

bool
MacOSWindow::IsMinimized() const
{
    return false;
}

void
MacOSWindow::Close()
{
    SDL_DestroyWindow(m_handle);
}

void
MacOSWindow::SetMode(EWindowMode _mode)
{
    m_settings.Mode = _mode;
}

void
MacOSWindow::Move(uint32 _x, uint32 _y)
{}

void*
MacOSWindow::GetHandle() const
{
    return m_handle;
}
}

#include "OTime.h"

#include <Windows.h>

namespace OnionEngine {

Time* Time::s_Instance = nullptr;

struct Members
{
    LARGE_INTEGER m_Start;
    double        m_Frequency;
};

void
Time::Initialize()
{
    if (s_Instance == nullptr) {
        s_Instance            = new Time();
        s_Instance->m_members = new (s_Instance->m_reserved) Members();
        LARGE_INTEGER frequency;
        QueryPerformanceFrequency(&frequency);
        s_Instance->m_members->m_Frequency = 1.0 / frequency.QuadPart;
        s_Instance->Reset();
    }
}

void
Time::Reset()
{
    QueryPerformanceCounter(&s_Instance->m_members->m_Start);
}

float
Time::Elapsed()
{
    LARGE_INTEGER current;
    QueryPerformanceCounter(&current);
    uint64 cycles = current.QuadPart - s_Instance->m_members->m_Start.QuadPart;
    return (float)(cycles * s_Instance->m_members->m_Frequency);
}

float
Time::ElapsedMillis()
{
    return s_Instance->Elapsed() * 1000.0f;
}

} // OnionEngine
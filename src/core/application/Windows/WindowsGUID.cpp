#include "GUID.h"
#include "Rpcdce.h"

namespace OnionEngine {
OGUID
OGUID::Generate()
{
    UUID wtemp;
    CoCreateGuid(&wtemp);
    OGUID temp;

    temp.DataInt[0] = wtemp.Data1;
    temp.DataInt[1] = wtemp.Data1 >> 8;
    temp.DataInt[2] = wtemp.Data2 << 8 | wtemp.Data3;
    temp.DataInt[3] = wtemp.Data4[3] << 24 | wtemp.Data4[2] << 16 |
                      wtemp.Data4[1] << 8 | wtemp.Data4[0];

    return temp;
}
} // OnionEngine

#include "LogSubsystem.h"
#include <Windows.h>

namespace OnionEngine {
void
OLogSubsystem::PlatformLogMessage(ELogLevel _Level)
{
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    switch (_Level) {
        case ELogLevel::Fatal:
            SetConsoleTextAttribute(hConsole,
                                    BACKGROUND_RED | BACKGROUND_INTENSITY |
                                      FOREGROUND_RED | FOREGROUND_BLUE |
                                      FOREGROUND_GREEN | FOREGROUND_INTENSITY);
            break;
        case ELogLevel::Error:
            SetConsoleTextAttribute(hConsole,
                                    FOREGROUND_RED | FOREGROUND_INTENSITY);
            break;
        case ELogLevel::Warning:
            SetConsoleTextAttribute(hConsole,
                                    FOREGROUND_RED | FOREGROUND_GREEN |
                                      FOREGROUND_INTENSITY);
            break;
        case ELogLevel::Info:
            SetConsoleTextAttribute(hConsole,
                                    FOREGROUND_GREEN | FOREGROUND_INTENSITY);
            break;
    }
}

void
OLogSubsystem::PlatformClearLogMessage()
{
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    SetConsoleTextAttribute(
      hConsole, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN);
}

std::string
OLogSubsystem::GetLastErrorAsString()
{
    // Get the error message, if any.
    DWORD errorMessageID = ::GetLastError();
    if (errorMessageID == 0)
        return std::string(); // No error message has been recorded

    LPSTR messageBuffer = nullptr;
    size_t size = FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                                  FORMAT_MESSAGE_FROM_SYSTEM |
                                  FORMAT_MESSAGE_IGNORE_INSERTS,
                                NULL,
                                errorMessageID,
                                MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                                (LPSTR)&messageBuffer,
                                0,
                                NULL);

    std::string message(messageBuffer, size);

    // Free the buffer.
    LocalFree(messageBuffer);

    return message;
}
}

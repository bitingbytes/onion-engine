#include "WindowsWindow.h"

namespace OnionEngine {
const tchar WND_CLASS_NAME[] = TEXT("OnionWindow");

WindowsWindow::WindowsWindow()
  : m_handle(NULL)
  , m_ownerApplication(nullptr)
  , m_windowMode(EWindowMode::WM_WINDOWNED)
{}

WindowsWindow::~WindowsWindow() {}

WindowsWindow*
WindowsWindow::MakeWindow()
{
    return new WindowsWindow();
}

void
WindowsWindow::Initialize(WindowsApplication* _ownerApplication,
                          const SWindowSettings& _settings,
                          HINSTANCE                              _hInstance)
{
    m_ownerApplication = _ownerApplication;
    m_settings         =  _settings;

    DWORD ExStyle = 0;
    DWORD ExFlags = WS_OVERLAPPEDWINDOW;

    m_handle = ::CreateWindowEx(ExStyle,
                                WND_CLASS_NAME,
                                _settings.Title.c_str(),
                                ExFlags,
                                CW_USEDEFAULT,
                                CW_USEDEFAULT,
                                CW_USEDEFAULT,
                                CW_USEDEFAULT,
                                NULL,
                                NULL,
                                _hInstance,
                                NULL);

    if (m_handle == nullptr) {
        OE_ERROR("Could not create window: %s", _settings.Title.c_str());
        return;
    }

    OE_VERB("Window %s created with success.", _settings.Title.c_str());
}
void
WindowsWindow::Show()
{
    ::ShowWindow(m_handle, SW_SHOW);
}

void
WindowsWindow::Hide()
{
    ::ShowWindow(m_handle, SW_HIDE);
}

void
WindowsWindow::Maximize()
{
    ::ShowWindow(m_handle, SW_MAXIMIZE);
}

void
WindowsWindow::Minimize()
{
    ::ShowWindow(m_handle, SW_MINIMIZE);
}

bool
WindowsWindow::IsMaximized() const
{
    return false;
}

bool
WindowsWindow::IsMinimized() const
{
    return false;
}

void
WindowsWindow::Close()
{
    ::CloseWindow(m_handle);
    ::DestroyWindow(m_handle);
}

void
WindowsWindow::SetMode(EWindowMode _mode)
{
    m_settings.Mode = _mode;
}

void
WindowsWindow::Move(uint32 _x, uint32 _y)
{
    m_settings.PositionX = _x;
    m_settings.PositionY = _y;
    ::MoveWindow(m_handle,
                 (int32)m_settings.PositionX,
                 (int32)m_settings.PositionY,
                 m_settings.Width,
                 m_settings.Height,
                 TRUE);
}

void*
WindowsWindow::GetHandle() const
{
    return m_handle;
}
}
#include "FileSystem.h"
#include "LogSubsystem.h"
#include <direct.h>
#include <shlwapi.h>

#pragma comment(lib, "shlwapi.lib")

namespace OnionEngine {
bool
OFile::InternalOpen()
{
    if (!m_Opened) {
        errno_t result = fopen_s(&m_Handler, m_Path, m_Mode);

        if (result != 0) {
            OE_ERROR("Could not open file: %s. Motive: %s",
                     m_Path,
                     OLogSubsystem::GetLastErrorAsString().c_str());
            return false;
        }

        m_Opened = true;
        OE_VERB("The File %s was opened.", m_Path);
    } else {
        OE_WARN("The File %s is already opened", m_Path);
    }

    return true;
}

size_t
OFile::InternalRead(void* _buffer, size_t _bytes)
{
    if (m_Handler == NULL && !m_Opened) {
        OE_ERROR("Could not read file %s. Be sure to use the Open() function "
                 "before trying to read the file.",
                 m_Path);
        return 0;
    }

    return fread_s(_buffer, _bytes, _bytes / 2, 2, m_Handler);
}

size_t
OFile::InternalWrite(void* _buffer, size_t _bytes)
{
    if (!IsValid()) {
        OE_ERROR("Could not read file %s. Be sure to use the Open() function "
                 "before trying to read the file.",
                 m_Path);
    }

    return fwrite(_buffer, _bytes, sizeof(uint8), m_Handler);
}

bool
OFile::InternalFlush()
{
    if (!IsValid()) {
        OE_ERROR("Could not read file %s. Be sure to use the Open() function "
                 "before trying to read the file.",
                 m_Path);
        return false;
    }

    return fflush(m_Handler) == 0;
}

bool
OFile::InternalClose()
{
    m_Opened = false;
    if (IsValid())
        return fclose(m_Handler) == 0;
    return true;
}

bool
OFile::InternalSeek(size_t _offset, int32 _mode)
{
    return fseek(m_Handler, (long)_offset, _mode);
}

size_t
OFile::InternalTell() const
{
    return ftell(m_Handler);
}

void
OFile::InternalGetStat()
{

    WIN32_FILE_ATTRIBUTE_DATA fileInfo;

    GetFileAttributesEx(m_Path, GetFileExInfoStandard, (void*)&fileInfo);

    m_Size = (size_t)fileInfo.nFileSizeLow;
    m_CreationTime = (size_t)fileInfo.ftCreationTime.dwLowDateTime;
    m_LastAccessTime = (size_t)fileInfo.ftLastAccessTime.dwLowDateTime;
    m_LastWriteTime = (size_t)fileInfo.ftLastWriteTime.dwLowDateTime;
}

inline bool
OFile::IsDirty() const
{
    WIN32_FILE_ATTRIBUTE_DATA fileInfo;

    GetFileAttributesEx(m_Path, GetFileExInfoStandard, (void*)&fileInfo);

    return m_LastWriteTime != (size_t)fileInfo.ftLastWriteTime.dwLowDateTime;
}

void
OFileSystem::InternalGetCurrentPath(tchar* _path)
{
    tchar buffer[MAX_PATH * sizeof(tchar)];

#if OE_UNICODE
    _wgetcwd(buffer, MAX_PATH * sizeof(tchar));
    FixPath(buffer);
    wmemcpy(_path, buffer, MAX_PATH * sizeof(tchar));
#else
    _getcwd(buffer, MAX_PATH * sizeof(tchar));
    FixPath(buffer);
    memcpy(_path, buffer, MAX_PATH * sizeof(tchar));
#endif
}

void
OFileSystem::FixPath(tchar* _path)
{
    for (int32 i = 0; _path[i] != '\0'; ++i) {
        if (_path[i] == '\\')
            _path[i] = '/';
    }
}

bool
OFileSystem::CreateDirectory(const tchar* _path, bool _recursively)
{
    if (::CreateDirectory(_path, NULL) == FALSE) {

        DWORD err = GetLastError();

        if (err != ERROR_ALREADY_EXISTS && _recursively) {
            OString path(_path);
            std::vector<OString> dirs;
            Split(path, dirs, "/");

            for (auto p : dirs) {
                ::CreateDirectory(p.c_str(), NULL);

                if (GetLastError() == ERROR_ALREADY_EXISTS)
                    break;
            }
        }

        OE_ERROR("Could not create directory %s. Motive: %s",
                 _path,
                 OLogSubsystem::GetLastErrorAsString().c_str());
        return false;
    }

    return true;
}

bool
OFileSystem::ChangeDirectory(const tchar* _path)
{
    if (::SetCurrentDirectory(_path) == FALSE) {
        OE_ERROR("Could not change to directory %s.", _path);
        return false;
    }

    return true;
}

void
OFileSystem::GetExecutableDirectory(tchar* path)
{
    GetModuleFileName(NULL, path, MAX_PATH * sizeof(tchar));
}

bool
OFileSystem::IsDirectory(const tchar* _path)
{
    DWORD ftyp = GetFileAttributes(_path);
    if (ftyp == INVALID_FILE_ATTRIBUTES)
        return false; // something is wrong with your path!

    if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
        return true; // this is a directory!

    return false; // this is not a directory!
}

OString
OFileSystem::GetFileName(const tchar* _path)
{
    OString temp(_path);
    return temp.substr(0, temp.rfind('.'));
}

bool
OFileSystem::FileExists(const tchar* _path)
{
    return ::PathFileExists(_path) == TRUE;
}

std::vector<OString>
OFileSystem::GetFileList(const tchar* _path)
{
    std::vector<OString> output;
    WIN32_FIND_DATA findfiledata;
    HANDLE hFind = INVALID_HANDLE_VALUE;

    char fullpath[MAX_PATH];
    GetFullPathName(_path, MAX_PATH, fullpath, 0);
    std::string fp(fullpath);

    hFind = FindFirstFile(fp.c_str(), &findfiledata);
    if (hFind != INVALID_HANDLE_VALUE) {
        do {
            output.push_back(findfiledata.cFileName);
        } while (FindNextFile(hFind, &findfiledata) != 0);
    }
    return output;
}
}

// TODO: O File pode ser um ponteiro que eu construo passando o Path e o Modo de
// leitura e entao chamo o file->Open()  para abri-lo?

// TODO: Pensar sobre o Unicode, o projeto deve ser SEMPRE Unicode? Caso não,
// deve-se corrigir o Logger
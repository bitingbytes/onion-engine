#include "WindowsApplication.h"

namespace OnionEngine {
WindowsApplication::WindowsApplication()
  : m_hInstance(NULL)
  , m_window(nullptr)
{}

WindowsApplication::~WindowsApplication() {}

IWindow*
WindowsApplication::MakeWindow()
{
    return WindowsWindow::MakeWindow();
}

void
WindowsApplication::Initialize()
{
    m_hInstance = ::GetModuleHandle(0);

    InitializeSubmodules();

    m_window = (WindowsWindow*)MakeWindow();
    InitializeWindow(*m_window, GetWindowFromConfig());
}

void
WindowsApplication::Shutdown()
{
    m_window->Close();
    ShutdownSubmodules();
}

void
WindowsApplication::InitializeWindow(IWindow&               _window,
                                     const SWindowSettings& _settings)
{
    WindowsWindow& OWindow = (WindowsWindow&)_window;

    OWindow.Initialize(this, _settings, m_hInstance);
}

void
WindowsApplication::PumpMessage()
{
    MSG Msg;

    while (PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE)) {
        TranslateMessage(&Msg);
        DispatchMessage(&Msg);
    }
}

IWindow*
WindowsApplication::GetCurrentWindow() const
{
    return m_window;
}

void
WindowsApplication::InitializeSubmodules()
{
    OLogSubsystem::Initialize();
    OMemorySubsystem::Initialize();
    OConfigSubsystem::Initialize();
    OInputSubsystem::Initialize();
}

void
WindowsApplication::ShutdownSubmodules()
{
    OInputSubsystem::Shutdown();
    OConfigSubsystem::Shutdown();
    OMemorySubsystem::Shutdown();
    OLogSubsystem::Shutdown();
}
} // OnionEngine

#include "Serializer.h"

namespace OnionEngine {
Serializer&
Serializer::operator<<(uint32 _value)
{
    Write(&_value, sizeof(uint32));
    return *this;
}

Serializer&
Serializer::operator>>(uint32& _value)
{
    Read(&_value, sizeof(uint32));
    return *this;
}

Serializer&
Serializer::operator<<(int32 _value)
{
    Write(&_value, sizeof(int32));
    return *this;
}

Serializer&
Serializer::operator>>(int32& _value)
{
    Read(&_value, sizeof(int32));
    return *this;
}

Serializer&
Serializer::operator<<(uint8* _value)
{
    Write(_value, sizeof(uint8));
    return *this;
}

Serializer&
Serializer::operator<<(float _value)
{
    Write(&_value, sizeof(float));
    return *this;
}

Serializer&
Serializer::operator>>(float& _value)
{
    Read(&_value, sizeof(float));
    return *this;
}

Serializer&
Serializer::operator<<(double _value)
{
    Write(&_value, sizeof(double));
    return *this;
}

Serializer&
Serializer::operator>>(double& _value)
{
    Read(&_value, sizeof(double));
    return *this;
}

/* Serializer&
Serializer::operator<<(const OString& _value)
{
  int32 Length = (int32)_value.length();
  *this << Length;
  Write((void*)_value.c_str(), sizeof(tchar) * Length);
  return *this;
}

Serializer&
Serializer::operator>>(OString& _value)
{
  int Length = -1;
  *this >> Length;
  std::vector<tchar> mem(Length + 1);
  tchar* pChars = &mem[0];
  Read(pChars, sizeof(tchar) * Length);
  mem[Length] = TEXT('\0');
  _value = pChars;
  return *this;
} */
} // namespace OnionEngine
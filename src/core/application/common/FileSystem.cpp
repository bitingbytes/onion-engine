#include "FileSystem.h"
#include "CRC32.h"

namespace OnionEngine {
OString
OFileSystem::AddExtension(const tchar* _path, const tchar* _extension)
{
    OString path(_path);
    OString ext(_extension);

    if (ext.find('.') == OString::npos)
        path += ".";

    path += ext;

    return path;
}

OString
OFileSystem::PathAppend(const tchar* _pathA, const tchar* _pathB)
{
    OString pathA(_pathA);
    OString pathB(_pathB);

    if (!IsDirectory(_pathA) || !IsDirectory(_pathB)) {
        return pathA;
    }

    tchar lastA = pathA[pathA.length() - 1];

    if (lastA != '/')
        pathA += "/";

    pathA += _pathB;

    return pathA;
}

OFile::OFile(const tchar* _path, const tchar* _mode)
  : m_Path(_path)
  , m_Mode(_mode)
  , m_Size(0)
  , m_Offset(0)
  , m_CreationTime(0)
  , m_LastAccessTime(0)
  , m_LastWriteTime(0)
  , m_Opened(false)
  , m_Handler(nullptr)
{
    InternalGetStat();
}

OFile::~OFile()
{
    Close();
}

bool
OFile::Open()
{
    return InternalOpen();
}

size_t
OFile::Read(void* _buffer, size_t _bytes)
{
    m_Offset += _bytes;
    return InternalRead(_buffer, _bytes);
}

size_t
OFile::Write(void* _buffer, size_t _bytes)
{
    m_Offset += _bytes;
    m_Size += _bytes;
    return InternalWrite(_buffer, _bytes);
}

bool
OFile::Flush()
{
    return InternalFlush();
}

bool
OFile::Close()
{
    return InternalClose();
}

OString
OFile::GetExtension() const
{
    OString temp(m_Path);
    return temp.substr(temp.find_last_of('.'));
}

bool
OFile::Seek(size_t _offset, int32 _mode)
{

    switch (_mode) {
        case 0:
            m_Offset = _offset;
            break;
        case 1:
            m_Offset += _offset;
            break;
        case 2:
            m_Offset -= _offset;
            break;
    }

    return InternalSeek(_offset, _mode);
}

OString
OFile::GetLine()
{
    tchar buffer[128];
    fgets(buffer, 128, m_Handler);
    m_Offset = Tell();
    return OString(buffer);
}

size_t
OFile::Tell()
{
    return InternalTell();
}

unsigned
OFile::GetCheckSum()
{
    if (!IsValid() || m_Mode[0] == 'w')
        return 0;

    size_t   oldOffset = m_Offset;
    unsigned checksum  = 0;

    Seek(0, SEEK_SET);

    while (!IsEoF()) {
        unsigned char block[1024];
        unsigned      readBytes = (unsigned) Read(block, 1024);
        for (unsigned i = 0; i < readBytes; ++i)
            checksum = block[i] + (checksum << 6) + (checksum << 16) - checksum;
    }

    Seek(oldOffset, SEEK_SET);
    return checksum;
}

} // namespace OnionEngine
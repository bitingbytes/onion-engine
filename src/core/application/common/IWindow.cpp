#include "IWindow.h"

namespace OnionEngine {
IWindow::IWindow()
{
    /* Does nothing */
}

IWindow::~IWindow()
{
    /* Does nothing */
}

void
IWindow::Show()
{
    /* Does nothing */
}

void
IWindow::Hide()
{
    /* Does nothing */
}

void
IWindow::Maximize()
{
    /* Does nothing */
}

void
IWindow::Minimize()
{
    /* Does nothing */
}

bool
IWindow::IsMaximized() const
{
    return true;
}

bool
IWindow::IsMinimized() const
{
    return false;
}

void
IWindow::Close()
{
    /* Does nothing */
}

void
IWindow::SetMode(EWindowMode _mode)
{
    /* Does nothing */
}

void
IWindow::Move(uint32 _x, uint32 _y)
{
    /* Does nothing */
}

void*
IWindow::GetHandle() const
{
    return nullptr;
}
} // OnionEngine

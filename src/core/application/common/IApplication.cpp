#include "IApplication.h"

namespace OnionEngine {
IApplication* IApplication::s_Instance = nullptr;

IApplication::~IApplication() {}

void
IApplication::Initialize()
{}

void
IApplication::Shutdown()
{}

void
IApplication::InitializeWindow(IWindow&               _window,
                               const SWindowSettings& _settings)
{}

void
IApplication::PumpMessage()
{}

void
IApplication::InitializeSubmodules()
{}

void
IApplication::ShutdownSubmodules()
{}

SWindowSettings
IApplication::GetWindowFromConfig()
{
    OSectionData     config = OConfigSubsystem::GetSection("Rendering");
    SWindowSettings temp   = SWindowSettings();
    temp.Width             = config.GetInt("iWidth");
    temp.Height            = config.GetInt("iHeight");
    temp.Mode              = (EWindowMode)config.GetInt("iMode");

    return temp;
}

IWindow*
IApplication::GetCurrentWindow() const
{
    return nullptr;
}

IApplication::IApplication() {}
} // OnionEngine
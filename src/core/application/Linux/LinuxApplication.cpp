#include "Application.h"
#include "SDL2/SDL.h"

namespace OnionEngine {
LinuxApplication::LinuxApplication()
  : m_window(nullptr)
{}

LinuxApplication::~LinuxApplication() {}

IWindow*
LinuxApplication::MakeWindow()
{
    return LinuxWindow::MakeWindow();
}

void
LinuxApplication::Initialize()
{
    InitializeSubmodules();

    m_window = (LinuxWindow*)MakeWindow();
    InitializeWindow(*m_window, GetWindowFromConfig());
}

void
LinuxApplication::Shutdown()
{
    m_window->Close();
    ShutdownSubmodules();
}

void
LinuxApplication::InitializeWindow(IWindow&               _window,
                                   const SWindowSettings& _settings)
{
    LinuxWindow& Window = (LinuxWindow&)_window;

    Window.Initialize(_settings);
}

void
LinuxApplication::PumpMessage()
{
    SDLEventHandler();
}

IWindow*
LinuxApplication::GetCurrentWindow() const
{
    return m_window;
}

void
LinuxApplication::InitializeSubmodules()
{
    LogSubsystem::Initialize();
    MemorySubsystem::Initialize();
    ConfigSubsystem::Initialize();
    InputSubsystem::Initialize();
}

void
LinuxApplication::ShutdownSubmodules()
{
    InputSubsystem::Shutdown();
    ConfigSubsystem::Shutdown();
    MemorySubsystem::Shutdown();
    LogSubsystem::Shutdown();
}

void
LinuxApplication::SDLEventHandler()
{}
} // OnionEngine

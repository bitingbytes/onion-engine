#include "OTime.h"

namespace OnionEngine {
typedef std::chrono::high_resolution_clock       HighResolutionClock;
typedef std::chrono::duration<float, std::milli> milliseconds_type;

Time* Time::s_Instance = nullptr;

struct Members
{
    std::chrono::time_point<HighResolutionClock> m_start;
};

void
Time::Initialize()
{
    if (s_Instance == nullptr) {
        s_Instance            = new Time();
        s_Instance->m_members = new (s_Instance->m_reserved) Members();
        s_Instance->Reset();
    }
}

void
Time::Reset()
{
    s_Instance->m_members->m_start = HighResolutionClock::now();
}

float
Time::Elapsed()
{
    return std::chrono::duration_cast<milliseconds_type>(
             HighResolutionClock::now() - s_Instance->m_members->m_start)
             .count() /
           1000.0f;
}

float
Time::ElapsedMillis()
{
    return s_Instance->Elapsed() * 1000.0f;
}
} // OnionEngine

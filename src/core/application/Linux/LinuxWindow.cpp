#include "LinuxWindow.h"
#include <SDL2/SDL.h>

namespace OnionEngine {
LinuxWindow::LinuxWindow()
  : m_handle(nullptr)
{}

LinuxWindow::~LinuxWindow() {}

LinuxWindow*
LinuxWindow::MakeWindow()
{
    return new LinuxWindow();
}

void
Initialize(const SWindowSettings& _settings)
{
    m_settings = _settings;

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC);

    uint32 flags = SDL_WINDOW_OPENGL;

    if (m_settings.Mode == EWindowMode::WM_FULLSCREEN)
        flags |= SDL_WINDOW_FULLSCREEN;
    else if (m_settings.Mode == EWindowMode::WM_FULLSCREEN_BORDERLESS)
        flags |= SDL_WINDOW_FULLSCREEN_DESKTOP;

    m_handle = SDL_CreateWindow(m_settings.Title.c_str(),
                                m_settings.PositionX,
                                m_settings.PositionY,
                                m_settings.Width,
                                m_settings.Height,
                                flags);

    if (m_handle == nullptr) {
        OE_ERROR(TEXT("Could not create window: %s"), SDL_GetError());
        return;
    }

    OE_VERB(TEXT("Window %s created with success."), m_settings.Title.c_str());

    SetupController();

    return;
}

void
LinuxWindow::Show()
{
    SDL_ShowWindow((SDL_Window*)m_handle);
}

void
LinuxWindow::Hide()
{
    SDL_HideWindow((SDL_Window*)m_handle);
}

void
LinuxWindow::Maximize()
{
    SDL_MaximizeWindow((SDL_Window*)m_handle);
}

void
LinuxWindow::Minimize()
{
    SDL_MinimizeWindow((SDL_Window*)m_handle);
}

bool
LinuxWindow::IsMaximized() const
{
    return true;
}

bool
LinuxWindow::IsMinimized() const
{
    return false;
}

void
LinuxWindow::Close()
{
    SDL_DestroyWindow((SDL_Window*)m_handle);
}

void
LinuxWindow::SetMode(EWindowMode _mode)
{
    m_settings.Mode = _mode;
}

void
LinuxWindow::Move(uint32 _x, uint32 _y)
{}

void*
LinuxWindow::GetHandle() const
{
    return m_handle;
}
}

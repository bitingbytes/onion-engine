#include "GUID.h"
#include "LogSubsystem.h"
#include "Random.h"

namespace OnionEngine {
OGUID
OGUID::Generate()
{
    OGUID temp;
    temp.DataInt[0] = Math::Random::RandUInt();
    temp.DataInt[1] = Math::Random::RandUInt();
    temp.DataInt[2] = Math::Random::RandUInt();
    temp.DataInt[3] = Math::Random::RandUInt();

    return temp;
}
} // OnionEngine

#include "LogSubsystem.h"

namespace OnionEngine {
void
LogSubsystem::PlatformLogMessage(ELogLevel _Level)
{
    switch (_Level) {
        case ELogLevel::Fatal:
            printf("%s", "\e[01;31m");
            break;
        case ELogLevel::Error:
            printf("%s", "\e[05;31m");
            break;
        case ELogLevel::Warning:
            printf("%s", "\e[05;33m");
            break;
        case ELogLevel::Info:
            printf("%s", "\e[05;32m");
            break;
        case ELogLevel::Debug:
            break;
    }
}

void
LogSubsystem::PlatformClearLogMessage()
{
    printf("%s", "\e[22;37m");
}
}
#include "Sprite.h"

namespace OnionEngine {
namespace Graphics { // Graphics

Sprite::Sprite()
  : m_material(nullptr)
  , m_rect()
{}

Sprite::Sprite(Texture2D* _texture)
{
    m_material = new Material("shaders/SpriteUnlit.glsl");
    m_material->SetTexture2D(_texture);

    m_rect.PosX   = 0;
    m_rect.PosY   = 0;
    m_rect.Width  = _texture->GetWidth();
    m_rect.Height = _texture->GetHeight();
}

Sprite::Sprite(Texture2D* _texture, const Math::Rect& _rect)
{
    m_material = new Material("shaders/SpriteUnlit.glsl");
    m_material->SetTexture2D(_texture);

    m_rect = _rect;
}

Sprite::~Sprite() {}
}
} // OnionEngine

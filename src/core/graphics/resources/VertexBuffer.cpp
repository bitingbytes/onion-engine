#include "VertexBuffer.h"

#if OE_SUPPORT_OPENGL
#include "opengl/OpenGLVertexBuffer.h"
#endif
#if ONION_SUPPORT_DIRECTX11 || ONION_SUPPORT_DIRECTX12
#endif
#if ONION_SUPPORT_DIRECTX12
#endif

namespace OnionEngine {
namespace Graphics {
VertexBuffer*
VertexBuffer::Create()
{
    switch (GraphicSubsystem::GetAPI()) {
        case EGraphicAPI::GA_NONE:
            return nullptr;
            break;

#if OE_SUPPORT_OPENGL
        case EGraphicAPI::GA_OPENGL:
            return new OpenGLVertexBuffer();
            break;
#endif

#if ONION_SUPPORT_DIRECTX11 || ONION_SUPPORT_DIRECTX12
        case EGraphicAPI::GA_DIRECTX11:
            break;
#endif

#if ONION_SUPPORT_DIRECTX12
        case EGraphicAPI::GA_DIRECTX12:
            break;
#endif
    }

    return nullptr;
}
} // Graphics
} // OnionEngine

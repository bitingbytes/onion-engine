#include "Material.h"

namespace OnionEngine {
namespace Graphics {
Material::Material(Shader* _shader)
  : m_shader(_shader)
  , m_color(Math::Color(1.0f, 1.0f, 1.0f, 1.0f))
{
    m_textures.reserve(1);
}

Material::Material(const char* _shaderPath)
  : m_shader(Shader::LoadFromFile(_shaderPath))
  , m_color(Math::Color(1.0f, 1.0f, 1.0f, 1.0f))
{
    m_textures.reserve(1);
}

Material::~Material()
{
    delete m_shader;
}

void
Material::Attach()
{
    m_shader->Attach();
    m_shader->SetColor("Color", m_color);
    for (auto t : m_textures) {
        t->Attach();
    }
}

void
Material::Render()
{
    // m_shader->Render();

    for (auto t : m_textures)
        t->Render();
}

void
Material::Detach()
{
    m_shader->Detach();

    for (auto t : m_textures)
        t->Detach();
}

void
Material::SetTexture2D(Texture2D* _texture)
{
    m_textures.push_back(_texture);
}

void
Material::SetMatrix(const char* _property, const Math::Matrix4x4& _matrix)
{
    m_shader->SetMatrix4x4(_property, _matrix);
}
} // OnionEngine
} // Graphics

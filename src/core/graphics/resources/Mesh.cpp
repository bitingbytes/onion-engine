#include "Mesh.h"

namespace OnionEngine {
namespace Graphics {
Mesh::Mesh(VertexArray* _array, IndexBuffer* _index, Material* _mat)
  : m_varray(_array)
  , m_ibuffer(_index)
  , m_material(_mat)
{}

Mesh::Mesh(VertexArray* _array, IndexBuffer* _index, Texture2D* _tex)
  : m_varray(_array)
  , m_ibuffer(_index)
{
    m_material = new Material("shaders/SpriteUnlit.glsl");
    m_material->SetTexture2D(_tex);
}

Mesh::~Mesh()
{
    delete m_ibuffer;
    delete m_material;
    delete m_varray;
}

void
Mesh::Draw(Math::Matrix4x4 _cam)
{
    m_material->Attach();
    m_material->SetMatrix("model", m_transform);
    m_material->SetMatrix("proj", _cam);

    m_varray->Bind();
    m_ibuffer->Attach();
    m_varray->Draw(m_ibuffer->GetCount());
    m_ibuffer->Detach();
    m_varray->Unbind();

    m_material->Detach();
}

} // Graphics

} // OnionEngine

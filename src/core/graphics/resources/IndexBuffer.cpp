#include "IndexBuffer.h"

#if OE_SUPPORT_OPENGL
#include "opengl/OpenGLIndexBuffer.h"
#endif
#if ONION_SUPPORT_DIRECTX11 || ONION_SUPPORT_DIRECTX12
#endif
#if ONION_SUPPORT_DIRECTX12
#endif

namespace OnionEngine {
namespace Graphics {
IndexBuffer*
IndexBuffer::Create(uint16* _data, uint32 _size)
{
    switch (GraphicSubsystem::GetAPI()) {
        case EGraphicAPI::GA_NONE:
            return nullptr;
            break;

#if OE_SUPPORT_OPENGL
        case EGraphicAPI::GA_OPENGL:
            return new OpenGLIndexBuffer(_data, _size);
            break;
#endif

#if ONION_SUPPORT_DIRECTX11 || ONION_SUPPORT_DIRECTX12
        case EGraphicAPI::GA_DIRECTX11:
            break;
#endif

#if ONION_SUPPORT_DIRECTX12
        case EGraphicAPI::GA_DIRECTX12:
            break;
#endif
    }
    return nullptr;
}

IndexBuffer*
IndexBuffer::Create(uint32* _data, uint32 _size)
{
    {
        switch (GraphicSubsystem::GetAPI()) {
            case EGraphicAPI::GA_NONE:
                return nullptr;
                break;

#if OE_SUPPORT_OPENGL
            case EGraphicAPI::GA_OPENGL:
                return new OpenGLIndexBuffer(_data, _size);
                break;
#endif

#if ONION_SUPPORT_DIRECTX11 || ONION_SUPPORT_DIRECTX12
            case EGraphicAPI::GA_DIRECTX11:
                break;
#endif

#if ONION_SUPPORT_DIRECTX12
            case EGraphicAPI::GA_DIRECTX12:
                break;
#endif
        }
        return nullptr;
    }
}
} // Graphics
} // OnionEngine

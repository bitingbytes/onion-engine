#include "Shader.h"
#include <fstream>

#if OE_SUPPORT_OPENGL
#include "opengl/OpenGLShader.h"
#endif
#if ONION_SUPPORT_DIRECTX11 || ONION_SUPPORT_DIRECTX12
#endif
#if ONION_SUPPORT_DIRECTX12
#endif

namespace OnionEngine {
namespace Graphics {
Shader*
Shader::LoadFromFile(const char* _path)
{
    switch (GraphicSubsystem::GetAPI()) {
        case EGraphicAPI::GA_NONE:
            return nullptr;
            break;

#if OE_SUPPORT_OPENGL
        case EGraphicAPI::GA_OPENGL:
            return new OpenGLShader(_path);
            break;
#endif

#if ONION_SUPPORT_DIRECTX11 || ONION_SUPPORT_DIRECTX12
        case EGraphicAPI::GA_DIRECTX11:
            break;
#endif

#if ONION_SUPPORT_DIRECTX12
        case EGraphicAPI::GA_DIRECTX12:
            break;
#endif
    }

    return nullptr;
}

bool
Shader::PreProcess(const char* _path, String** _shaders)
{
    std::ifstream sf;
    ShaderType    type = ShaderType::ST_UNKOWN;

    sf.open(_path, std::ios::in);

    if (!sf.is_open()) {
        OE_FATAL("Could not open shader file: %s", _path);
        return false;
    }

    while (!sf.eof()) {
        String line;
        std::getline(sf, line);

        if (strstr(line.c_str(), "@shader")) {
            if (strstr(line.c_str(), "vertex")) {
                type = ShaderType::ST_VERTEX;
            } else if (strstr(line.c_str(), "pixel")) {
                type = ShaderType::ST_PIXEL;
            }
        } else if (type != ShaderType::ST_UNKOWN) {
            _shaders[(uint32)type - 1]->append(line);
            _shaders[(uint32)type - 1]->append("\n");
        }
    }

    return true;
}

} // Graphics
} // OnionEngine

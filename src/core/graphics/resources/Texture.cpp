#include "Texture.h"

namespace OnionEngine {
namespace Graphics {
Texture::Texture()
  : m_width(0)
  , m_height(0)
  , m_wrapMode(ETextureWrapMode::TW_NONE)
  , m_format(ETextureColorFormat::TCF_NONE)
  , m_filter(ETextureFilter::TF_NONE)
  , m_hasMipMaps(false)
  , m_data(nullptr)
{}

Texture::Texture(const char* _path)
{
    File* file = new File(_path, "rb");

    file->Open();

    file->Read(&m_width, sizeof(m_width));
    file->Read(&m_height, sizeof(m_height));
    file->Read(&m_wrapMode, sizeof(ETextureWrapMode));
    file->Read(&m_format, sizeof(ETextureColorFormat));
    file->Read(&m_filter, sizeof(ETextureFilter));

    size_t newSize = file->GetSize() - file->Tell();

    m_data = new uint8[newSize];

    file->Read(m_data, file->GetSize());

    delete file;
}

Texture::~Texture()
{
    delete[] m_data;
}
} // Graphics

} // OnionEngine

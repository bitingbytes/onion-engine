#include "FlatRenderer.h"
#include "CameraComponent.h"

#define VERTEX_SIZE sizeof(Math::VertexP1T)
#define TRIANGLE_SIZE VERTEX_SIZE * 3
#define MAX_TRIANGLES 6000
#define MAX_VERTEX_BUFFER MAX_TRIANGLES * TRIANGLE_SIZE
#define MAX_INDEX_BUFFER MAX_VERTEX_BUFFER * 3

namespace OnionEngine {
namespace Graphics {
FlatRenderer::FlatRenderer(CameraComponent* _owner)
  : m_owner(_owner)
  , m_batcher(nullptr)
  , m_isBatching(false)
{
    if (m_isBatching)
        m_batcher = new BatchRenderer();
}

FlatRenderer::~FlatRenderer()
{
    delete m_batcher;
}

void
FlatRenderer::Begin()
{
    if (m_isBatching) {
        m_batcher->Clean();
    }
}

void
FlatRenderer::BeginScene()
{
    if (m_isBatching) {
        while (!m_commandQueue.empty()) {
            auto& cmd = m_commandQueue.front();

            switch (cmd.Type) {
                case ERendererCommand::RC_DRAW_MESH:
                    Mesh* mesh = (Mesh*)cmd.Data;
                    m_batcher->SubmitMesh(mesh);
                    break;
            }

            m_commandQueue.pop();
        }
    }
}

void
FlatRenderer::SubmitCommand(RendererCommand&& _cmd)
{
    m_commandQueue.push(_cmd);
}

void
FlatRenderer::EndScene()
{
    if (m_isBatching) {
        m_batcher->Clean();
    }
}

void
FlatRenderer::End()
{}

void
FlatRenderer::Present()
{
    if (m_isBatching)
        BatchRender();
    else
        CommandRender();
}

void
FlatRenderer::CommandRender()
{
    while (!m_commandQueue.empty()) {
        auto& cmd = m_commandQueue.front();

        switch (cmd.Type) {
            case ERendererCommand::RC_DRAW_MESH:
                Mesh* mesh = (Mesh*)cmd.Data;
                mesh->Draw(m_owner->GetCameraMatrix());
                break;
        }

        m_commandQueue.pop();
    }
}

void
FlatRenderer::BatchRender()
{
    m_batcher->Present();
}
} // Graphics
} // OnionEngine

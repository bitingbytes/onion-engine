#include "BatchRenderer.h"

#define VERTEX_SIZE sizeof(Math::VertexP1T)
#define TRIANGLE_SIZE VERTEX_SIZE * 3
#define MAX_TRIANGLES TRIANGLE_SIZE * 65000

namespace OnionEngine {
namespace Graphics {
BatchRenderer::BatchRenderer() {
    m_varray = VertexArray::Create();
    VertexBuffer* buffer = VertexBuffer::Create();
    buffer->Allocate(MAX_TRIANGLES);
}

BatchRenderer::~BatchRenderer() {
    delete m_varray;
    delete m_ibuffer;
}

void
BatchRenderer::SubmitMesh(Mesh* _mesh)
{}

void
BatchRenderer::Present()
{}

void
BatchRenderer::Clean()
{}
} // Graphics
} // OnionEngine

#include "GraphicSubsystem.h"
#include "LogSubsystem.h"

#if OE_SUPPORT_OPENGL
#include "opengl/OpenGLDevice.h"
#endif
#if ONION_SUPPORT_DIRECTX11 || ONION_SUPPORT_DIRECTX12
#endif
#if ONION_SUPPORT_DIRECTX12
#endif

namespace OnionEngine {
namespace Graphics {
GraphicSubsystem* GraphicSubsystem::s_Instance = nullptr;

GraphicSubsystem::GraphicSubsystem()
{
    /* Does nothing */
}

GraphicSubsystem::~GraphicSubsystem()
{
    /* Does nothing */
}

void
GraphicSubsystem::Initialize()
{
    if (s_Instance == nullptr) {
        s_Instance = (GraphicSubsystem*)malloc(sizeof(GraphicSubsystem));
        s_Instance->m_device     = nullptr;
        s_Instance->m_currentAPI = EGraphicAPI::GA_NONE;

        OE_INFO("Graphic Subsystem Initialized.");
    }
}

void
GraphicSubsystem::Shutdown()
{
    if (s_Instance != nullptr) {
        if (s_Instance->m_device != nullptr)
            delete s_Instance->m_device;

        free(s_Instance);
    }
}

void
GraphicSubsystem::SwitchAPI(const char* _apiString)
{
    Name api(_apiString);

#if OE_SUPPORT_OPENGL
    if (api == Name("OpenGL")) {
        SwitchAPI(EGraphicAPI::GA_OPENGL);
    }
#endif
#if ONION_SUPPORT_DIRECTX11
    if (api == Name("DirectX11")) {
        SwitchAPI(EGraphicAPI::GA_DIRECTX11);
    }
#endif
#if ONION_SUPPORT_DIRECTX12
    if (api == Name("DirectX12")) {
        SwitchAPI(EGraphicAPI::GA_DIRECTX12);
    }
#endif
}

void
GraphicSubsystem::SwitchAPI(EGraphicAPI _api)
{
    s_Instance->SwitchAPIInternal(_api);
}

void
GraphicSubsystem::Clear()
{
    s_Instance->ClearInternal();
}

void
GraphicSubsystem::Present()
{
    s_Instance->PresentInternal();
}

bool
GraphicSubsystem::CreateDevice(Window* _window)
{
    return s_Instance->CreateDeviceInternal(_window);
}

void
GraphicSubsystem::ClearInternal()
{
    m_device->Clear();
}

void
GraphicSubsystem::PresentInternal()
{
    m_device->Present();
}

bool
GraphicSubsystem::CreateDeviceInternal(Window* _window)
{
    return m_device->Create(_window);
}

EGraphicAPI
GraphicSubsystem::GetAPI()
{
    return s_Instance->m_currentAPI;
}

void
GraphicSubsystem::SwitchAPIInternal(EGraphicAPI _api)
{
    if (m_currentAPI == _api)
        return;

    m_currentAPI = _api;

    if (m_device == nullptr) {
        switch (m_currentAPI) {
            case EGraphicAPI::GA_NONE:
                m_device = nullptr;
                break;

#if OE_SUPPORT_OPENGL
            case EGraphicAPI::GA_OPENGL:
                m_device = new OpenGLDevice();
                break;
#endif

#if ONION_SUPPORT_DIRECTX11 || ONION_SUPPORT_DIRECTX12
            case EGraphicAPI::GA_DIRECTX11:
                break;
#endif

#if ONION_SUPPORT_DIRECTX12
            case EGraphicAPI::GA_DIRECTX12:
                break;
#endif
        }
    } /* else {} */

    OE_INFO("Graphic API setted to <GRAPHIC API>.");
}

} // Graphics
} // OnionEngine

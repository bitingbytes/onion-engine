#include "GraphicDevice.h"

namespace OnionEngine {
namespace Graphics {
GraphicDevice::GraphicDevice() {}

GraphicDevice::~GraphicDevice() {}

bool
GraphicDevice::Create(Window* _window)
{
    m_window = _window;
    return false;
}

void
GraphicDevice::Clear()
{}

void
GraphicDevice::Present()
{}

void
GraphicDevice::SetViewport(const Math::Rect&)
{}

} // Graphics
} // OnionEngine
#include "opengl/OpenGLVertexArray.h"

namespace OnionEngine {
namespace Graphics {
OpenGLVertexArray::OpenGLVertexArray()
{
    glGenVertexArrays(1, &m_VAO);
    glBindVertexArray(m_VAO);
}

OpenGLVertexArray::~OpenGLVertexArray() {
    glDeleteVertexArrays(1, &m_VAO);
}

void
OpenGLVertexArray::PushBuffer(VertexBuffer* buffer)
{
    m_buffers.push_back(buffer);
}

void
OpenGLVertexArray::Bind() const
{
    m_buffers.front()->Attach();
}

void
OpenGLVertexArray::Unbind() const
{
    m_buffers.front()->Detach();
}

void
OpenGLVertexArray::Draw(uint32 count) const
{
    glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_INT, NULL);
}
} // Graphics
} // OnionEngine
#include "opengl/OpenGLIndexBuffer.h"

namespace OnionEngine {
namespace Graphics {
OpenGLIndexBuffer::OpenGLIndexBuffer(uint16* _data, uint32 _size)
{
    m_count = _size;

    glGenBuffers(1, &m_ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    glBufferData(
      GL_ELEMENT_ARRAY_BUFFER, _size * sizeof(uint16), _data, GL_STATIC_DRAW);
}

OpenGLIndexBuffer::OpenGLIndexBuffer(uint32* _data, uint32 _size)
{
    m_count = _size;

    glGenBuffers(1, &m_ibo);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
    glBufferData(
      GL_ELEMENT_ARRAY_BUFFER, _size * sizeof(uint32), _data, GL_STATIC_DRAW);
}

OpenGLIndexBuffer::~OpenGLIndexBuffer()
{
    glDeleteBuffers(1, &m_ibo);
}

void
OpenGLIndexBuffer::Attach()
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo);
}

void
OpenGLIndexBuffer::Render()
{}

void
OpenGLIndexBuffer::Detach()
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
} // Graphics
} // OnionEngine

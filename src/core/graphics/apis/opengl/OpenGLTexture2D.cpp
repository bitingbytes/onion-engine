#include "opengl/OpenGLTexture2D.h"

namespace OnionEngine {
namespace Graphics {
OpenGLTexture2D::OpenGLTexture2D(const char* _path)
  : Texture2D(_path)
{
    glGenTextures(1, &m_tex);
    glBindTexture(GL_TEXTURE_2D, m_tex);

    GLint wrapMode   = 0;
    GLint filterMode = 0;
    GLint format     = 0;

    switch (m_wrapMode) {
        case ETextureWrapMode::TW_NONE:
            wrapMode = GL_REPEAT;
            break;
        case ETextureWrapMode::TW_WRAP:
            wrapMode = GL_REPEAT;
            break;
        case ETextureWrapMode::TW_MIRROR:
            wrapMode = GL_MIRRORED_REPEAT;
            break;
        case ETextureWrapMode::TW_CLAMP:
            wrapMode = GL_CLAMP_TO_EDGE;
            break;
        case ETextureWrapMode::TW_BORDER_COLOR:
            wrapMode = GL_CLAMP_TO_BORDER;
            break;
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);

    switch (m_filter) {
        case ETextureFilter::TF_NONE:
            filterMode = GL_NEAREST;
            break;
        case ETextureFilter::TF_NEAREST:
            filterMode = GL_NEAREST;
            break;
        case ETextureFilter::TF_BILINEAR:
            filterMode = GL_LINEAR;
            break;
        case ETextureFilter::TF_TRILINEAR:
            filterMode = GL_LINEAR;
            break;
        case ETextureFilter::TF_ANISOTROPIC:
            filterMode = GL_NEAREST_MIPMAP_LINEAR;
            break;
    }

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filterMode);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filterMode);

    switch (m_format) {
        case ETextureColorFormat::TCF_NONE:
            format = GL_INVALID_ENUM;
            break;
        case ETextureColorFormat::TCF_R8:
            format = GL_R;
            break;
        case ETextureColorFormat::TCF_R16:
            format = GL_R16;
            break;
        case ETextureColorFormat::TCF_R32:
            format = GL_R32F;
            break;
        case ETextureColorFormat::TCF_R8G8:
            format = GL_RG;
            break;
        case ETextureColorFormat::TCF_R16G16:
            format = GL_RG16;
            break;
        case ETextureColorFormat::TCF_R32G32:
            format = GL_RG32F;
            break;
        case ETextureColorFormat::TCF_R8G8B8:
            format = GL_RGB;
            break;
        case ETextureColorFormat::TCF_R16G16B16:
            format = GL_RGB16;
            break;
        case ETextureColorFormat::TCF_R32G32B32:
            format = GL_RGB32F;
            break;
        case ETextureColorFormat::TCF_R8G8B8A8:
            format = GL_RGBA;
            break;
        case ETextureColorFormat::TCF_R16G16B16A16:
            format = GL_RGBA16;
            break;
        case ETextureColorFormat::TCF_R32G32B32A32:
            format = GL_RGBA32F;
            break;
        case ETextureColorFormat::TCF_BC1:
            format = GL_COMPRESSED_RGBA_S3TC_DXT1_EXT;
            break;
        case ETextureColorFormat::TCF_BC2:
            break;
        case ETextureColorFormat::TCF_BC3:
            format = GL_COMPRESSED_RGBA_S3TC_DXT3_EXT;
            break;
        case ETextureColorFormat::TCF_BC4:
            break;
        case ETextureColorFormat::TCF_BC5:
            format = GL_COMPRESSED_RGBA_S3TC_DXT5_EXT;
            break;
        case ETextureColorFormat::TCF_BC6:
            break;
        case ETextureColorFormat::TCF_BC7:
            break;
    }

    glTexImage2D(GL_TEXTURE_2D,
                 0,
                 format,
                 m_width,
                 m_height,
                 0,
                 format,
                 GL_UNSIGNED_BYTE,
                 m_data);

    if (m_hasMipMaps)
        glGenerateMipmap(GL_TEXTURE_2D);

    glBindTexture(GL_TEXTURE_2D, 0);
}

void
OpenGLTexture2D::Attach()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, m_tex);
}

void
OpenGLTexture2D::Render()
{
    // glActiveTexture(GL_TEXTURE0);
    // glBindTexture(GL_TEXTURE_2D, m_tex);
}

void
OpenGLTexture2D::Detach()
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

} // Graphics
} // OnionEngine
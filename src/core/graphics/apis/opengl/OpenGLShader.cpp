#include "opengl/OpenGLShader.h"

namespace OnionEngine {
namespace Graphics {
OpenGLShader::OpenGLShader(const char* _path)
{
    String  vert, frag;
    String* shaders[2] = { &vert, &frag };

    OE_ASSERT(PreProcess(_path, shaders));

    const char* vertexSource   = shaders[0]->c_str();
    const char* fragmentSource = shaders[1]->c_str();

    m_shaderProgram  = glCreateProgram();
    m_vertexShader   = glCreateShader(GL_VERTEX_SHADER);
    m_fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

    glShaderSource(m_vertexShader, 1, &vertexSource, NULL);
    glCompileShader(m_vertexShader);

    GLint status;
    glGetShaderiv(m_vertexShader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE) {
        char buffer[512];
        glGetShaderInfoLog(m_vertexShader, 512, NULL, buffer);
        OE_ERROR("OpenGL Vertex Shader Error: %s", buffer);
    }

    glShaderSource(m_fragmentShader, 1, &fragmentSource, NULL);
    glCompileShader(m_fragmentShader);

    glGetShaderiv(m_fragmentShader, GL_COMPILE_STATUS, &status);

    if (status != GL_TRUE) {
        char buffer[512];
        glGetShaderInfoLog(m_fragmentShader, 512, NULL, buffer);
        OE_ERROR("OpenGL Fragment Error: %s", buffer);
    }

    glAttachShader(m_shaderProgram, m_vertexShader);
    glAttachShader(m_shaderProgram, m_fragmentShader);

    glLinkProgram(m_shaderProgram);
    glValidateProgram(m_shaderProgram);

    glDetachShader(m_shaderProgram, m_vertexShader);
    glDetachShader(m_shaderProgram, m_fragmentShader);

    glDeleteShader(m_vertexShader);
    glDeleteShader(m_fragmentShader);
}

OpenGLShader::~OpenGLShader()
{
    glDeleteProgram(m_shaderProgram);
    glDeleteShader(m_fragmentShader);
    glDeleteShader(m_vertexShader);
}

void
OpenGLShader::Attach()
{
    glUseProgram(m_shaderProgram);
}

void
OpenGLShader::Render()
{}

void
OpenGLShader::Detach()
{
    glUseProgram(0);
}

void
OpenGLShader::SetMatrix4x4(const char*            _property,
                           const Math::Matrix4x4& _matrix)
{
    GLint matrix = glGetUniformLocation(m_shaderProgram, _property);
    glUniformMatrix4fv(matrix, 1, GL_FALSE, _matrix.D);
}

void
OpenGLShader::SetColor(const char* _property, const Math::Color _color)
{
    GLint color = glGetUniformLocation(m_shaderProgram, _property);
    glUniform4f(color, _color.R, _color.G, _color.B, _color.A);
}

void
OpenGLShader::SetColor(const char* _property, const Math::Color8 _color)
{
    GLint color = glGetUniformLocation(m_shaderProgram, _property);
    glUniform4ui(color, _color.R, _color.G, _color.B, _color.A);
}

} // Graphics
} // OnionEngine

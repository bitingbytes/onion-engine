#include "opengl/OpenGLDevice.h"
#include "GraphicSubsystem.h"
#include "onionengine.h"

namespace OnionEngine {
namespace Graphics {
OpenGLDevice::OpenGLDevice() {}

OpenGLDevice::~OpenGLDevice() {}

bool
OpenGLDevice::Create(Window* _window)
{
    GraphicDevice::Create(_window);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, ONION_OPENGL_VERSION);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                        SDL_GL_CONTEXT_PROFILE_CORE);

    gContext = SDL_GL_CreateContext((SDL_Window*)_window->GetHandler());

    if (gContext == NULL) {
        OE_ERROR("Could not initialize OpenGL: %s", SDL_GetError());
        return false;
    }

    // Initialize GLEW
    glewExperimental = GL_TRUE;
    GLenum glewError = glewInit();
    if (glewError != GLEW_OK) {
        OE_ERROR("Error initializing GLEW! %s\n",
                 glewGetErrorString(glewError));
    }

    // Use Vsync
    if (SDL_GL_SetSwapInterval(1) < 0) {
        OE_ERROR("Warning: Unable to set VSync! SDL Error: %s\n",
                 SDL_GetError());
    }

    // Initialize OpenGL
    if (!InitGL()) {
        OE_ERROR("Unable to initialize OpenGL!\n");
        return false;
    }

    return true;
}

bool
OpenGLDevice::InitGL()
{
    gProgramID = glCreateProgram();

    // glEnable(GL_DEPTH_TEST);
    // glEnable(GL_ALPHA_TEST);
    // glEnable(GL_STENCIL_TEST);

    return gProgramID != 0;
}

void
OpenGLDevice::Clear()
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void
OpenGLDevice::Present()
{
    SDL_GL_SwapWindow((SDL_Window*)m_window->GetHandler());
}

GLuint
OpenGLDevice::GetProgramID()
{
    OpenGLDevice& ogl = (OpenGLDevice&)GraphicSubsystem::Get().GetDevice();
    return ogl.gProgramID;
}

void
OpenGLDevice::SetViewport(const Math::Rect& _rect)
{
    glViewport(_rect.PosX,
               _rect.PosY,
               m_window->GetSettings().Width * (_rect.Width / 100.0f),
               m_window->GetSettings().Height * (_rect.Height / 100.0f));
}

} // Graphics
} // OnionEngine

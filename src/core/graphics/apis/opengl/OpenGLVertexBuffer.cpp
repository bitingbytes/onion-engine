#include "opengl/OpenGLVertexBuffer.h"

namespace OnionEngine {
namespace Graphics {
OpenGLVertexBuffer::OpenGLVertexBuffer()
  : m_size(0)
{
    glGenBuffers(1, &m_VBO);
}

OpenGLVertexBuffer::~OpenGLVertexBuffer()
{
    glDeleteBuffers(1, &m_VBO);
}

void*
OpenGLVertexBuffer::GetPointerInternal()
{
    void* result = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    return result;
}

void
OpenGLVertexBuffer::ReleasePointer()
{
    glUnmapBuffer(GL_ARRAY_BUFFER);
}

void
OpenGLVertexBuffer::Allocate(size_t _size)
{
    m_size = _size;
    glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
    glBufferData(GL_ARRAY_BUFFER, _size, NULL, GL_STATIC_DRAW);
    SetLayout();
}

void
OpenGLVertexBuffer::SetData(const void* _data, size_t _size)
{
    glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
    glBufferData(GL_ARRAY_BUFFER, _size, _data, GL_STATIC_DRAW);
    SetLayout();
}

void
OpenGLVertexBuffer::SetLayout()
{

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);

    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1,
                          2,
                          GL_FLOAT,
                          GL_FALSE,
                          5 * sizeof(float),
                          (const void*)(3 * sizeof(float)));
}

void
OpenGLVertexBuffer::Attach()
{
    glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
    SetLayout();
}

void
OpenGLVertexBuffer::Render()
{}

void
OpenGLVertexBuffer::Detach()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
} // Graphics
} // OnionEngine

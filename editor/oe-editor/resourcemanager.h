#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <QWidget>

namespace Ui {
class ResourceManager;
}

class ResourceManager : public QWidget
{
    Q_OBJECT

public:
    explicit ResourceManager(QWidget *parent = nullptr);
    ~ResourceManager();

private:
    Ui::ResourceManager *ui;
};

#endif // RESOURCEMANAGER_H

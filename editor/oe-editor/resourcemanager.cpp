#include "resourcemanager.h"
#include "ui_resourcemanager.h"

ResourceManager::ResourceManager(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ResourceManager)
{
    ui->setupUi(this);
}

ResourceManager::~ResourceManager()
{
    delete ui;
}

#include "newprojectdialog.h"
#include "ui_newprojectdialog.h"
#include "QFileDialog"
#include <QDebug>
#include <QProcess>
#include "mainwindow.h"

NewProjectDialog::NewProjectDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewProjectDialog)
{
    ui->setupUi(this);
}

NewProjectDialog::~NewProjectDialog()
{
    delete ui;
}

void NewProjectDialog::on_buttonBox_accepted()
{
    QString projectName = ui->lineEdit_projectName->text();
    QString companyName = ui->lineEdit_companyName->text();
    m_projectPath = ui->lineEdit_path->text();

    QStringList arguments;

    arguments << "init"
              << "--root" << m_projectPath
              << "--name-project" << projectName
              << "--name-company" << companyName;

    QProcess* oebuilder = new QProcess(this);
    oebuilder->start("oe-builder.exe", arguments);
    QString output(oebuilder->readAllStandardOutput());

    qDebug() << output;

    oebuilder->waitForFinished();
      {
          Project* project = new Project();
          project->OName = projectName;
          project->Company = companyName;
          project->Root = m_projectPath;

        MainWindow* mp = static_cast<MainWindow*>(this->parent());
        if(mp)
        {
            mp->SetProject(project);
            qDebug() << "Project setted";
        }
      }
}

void NewProjectDialog::on_pushButton_clicked()
{
    QFileDialog fd(this);
    fd.setFileMode(QFileDialog::Directory);
    fd.setViewMode(QFileDialog::List);

    if(fd.exec() == QFileDialog::Accepted)
        ui->lineEdit_path->setText(fd.selectedFiles().at(0));
}

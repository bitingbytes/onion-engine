#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "project.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void SetProject(Project* _project);
private:
    Ui::MainWindow *ui;
    static Project* s_currentProject;
    void CallImporter();

private slots:
    void NewProject();
    void OpenProject();
    void Exit();

    void ConnectMenu();
    void on_actionImport_triggered();
};

#endif // MAINWINDOW_H

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QDebug"
#include "QProcess"
#include "newprojectdialog.h"
#include <QFileDialog>
#include <QMessageBox>
#include <QDirIterator>

Project* MainWindow::s_currentProject=nullptr;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ConnectMenu();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::SetProject(Project *_project)
{
    if(s_currentProject == nullptr)
    {
        s_currentProject = _project;
        CallImporter();
    }
}

void MainWindow::CallImporter()
{
    QDirIterator it(s_currentProject->Root, QStringList() << "*.png", QDir::Files, QDirIterator::Subdirectories);
    QStringList arguments;
    while (it.hasNext())
       arguments << it.next();

    QProcess* oeimporter= new QProcess(this);

    oeimporter->setProcessChannelMode(QProcess::ForwardedChannels);
    oeimporter->setWorkingDirectory(s_currentProject->Root);

    oeimporter->start("oe-importer.exe", arguments);
    if(!oeimporter->waitForStarted())
    {
        qWarning("Could not initialize oeimporter...");
        delete oeimporter;
        return;
    }

    qDebug() << QString(oeimporter->readAllStandardOutput());
    oeimporter->deleteLater();
}

void MainWindow::NewProject()
{
    NewProjectDialog newProjectDialog(this);
    newProjectDialog.setModal(true);
    newProjectDialog.exec();
}

void MainWindow::OpenProject()
{
    QFileDialog fd(this);
    fd.setFileMode(QFileDialog::Directory);
    fd.setViewMode(QFileDialog::List);

    if(fd.exec() == QFileDialog::Accepted)
    {
        QString projectRoot = fd.selectedFiles().at(0);
        QFile projectFile(projectRoot + "/project.yaml");

        if(!projectFile.exists())
        {
            QMessageBox::critical(this, tr("ERROR"), tr("Could not locate project.yaml under this directory."));
            return;
        }

        Project* project = new Project();
        project->Root = projectRoot;
        SetProject(project);
    }
}

void MainWindow::Exit()
{
    QApplication::quit();
}

void MainWindow::ConnectMenu()
{
    connect(ui->actionNew_Project, &QAction::triggered,this, &MainWindow::NewProject);
    connect(ui->actionExit, &QAction::triggered,this, &MainWindow::Exit);
    connect(ui->actionOpen_Project, &QAction::triggered, this, &MainWindow::OpenProject);
}

void MainWindow::on_actionImport_triggered()
{

}
